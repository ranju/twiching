/**
 * This code is for the newsletter
 * User: Swaroop Shankar
 * Date: 4/5/12
 * Time: 6:56 PM
 */
if (document.getElementById('newsletter_email').value == '') {
    jQuery("#newsletter_email").addClass('litetext');
    document.getElementById('newsletter_email').value = "Enter your email address"
} else {
    if (document.getElementById('newsletter_email').value != 'Enter your email address') {
        jQuery("#newsletter_email").removeClass('litetext');
    }
}

jQuery("#newsletter_email").blur(function () {
    if (document.getElementById('newsletter_email').value == '') {
        jQuery("#newsletter_email").addClass('litetext');
        document.getElementById('newsletter_email').value = "Enter your email address"
    }
});
jQuery("#newsletter_email").focus(function () {
    if (document.getElementById('newsletter_email').value == 'Enter your email address') {
        jQuery("#newsletter_email").removeClass('litetext');
        document.getElementById('newsletter_email').value = ""
    }
});

//function which will handle the subscription of a user
jQuery("#newsletter_subscribe").click(function () {
    var newsletter_email = jQuery('#newsletter_email').val();
    subscribe(newsletter_email)
});

function subscribe(email){
    var request = jQuery.post('/subscribe_newsletter/',
        {
            'newsletter_email':email
        }, function (data) {
            if (data.status == 'error') {
                switch (data.error) {
                    case 'email_missing':
                        alert('For us to send you Newsletters, we require a valid email address. Please provide us with a valid email address!');
                        break;
                    case 'network_error':
                        jQuery.post('/set_ajax_message/',
                            {
                                'message':'Oops! Error occurred while processing your request. Please try again later',
                                'status':'Error'
                            }, function (data) {
                                window.location.reload();
                            }
                        )
                        break;
                }
            } else if (data.status == 'success'){
                jQuery.post('/set_ajax_message/',
                    {
                        'message':'Congrats! You have successfully subscribed for our newsletter. We will now deliver some cool stuffs to your inbox frequently',
                        'status':'Success'
                    }, function (data) {
                        window.location.reload();
                    }
                )
            }
        }
    );
    request.fail(
        function (data) {
            if (data.status = 405) {
                jQuery.post('/set_ajax_message/',
                    {
                        'message':'You tried to perform an invalid operation, if you are not sure why this error occurred then please contact customer care',
                        'status':'Error'
                    }, function (data) {
                        window.location.reload();
                    }
                )
            } else if (data.status = 500) {
                jQuery.post('/set_ajax_message/',
                    {
                        'message':'Oops! Error occurred while processing your request. Please try again later. If the problem persist please contact customer care!',
                        'status':'Error'
                    }, function (data) {
                        window.location.reload();
                    }
                )
            }
        }
    );
}

function unsubscribe(email){
    var confirmation = confirm('Are you sure you want to unsubscribe from our newsletters?');
    if (confirmation){
        var request = jQuery.post('/unsubscribe_newsletter/',
            {
                'newsletter_email':email
            }, function (data) {
                if (data.status == 'error') {
                    switch (data.error) {
                        case 'network_error':
                            jQuery.post('/set_ajax_message/',
                                {
                                    'message':'Oops! Error occurred while processing your request. Please try again later',
                                    'status':'Error'
                                }, function (data) {
                                    window.location.reload();
                                }
                            )
                            break;
                        case 'email_missing':
                            alert('We wont be able to unsubscribed you without knowing your email address!');
                            break;
                    }
                } else if (data.status == 'success'){
                    jQuery.post('/set_ajax_message/',
                        {
                            'message':'Your email is now removed from our subscription list successfully!',
                            'status':'Success'
                        }, function (data) {
                            window.location.reload();
                        }
                    )
                }
            }
        );
        request.fail(
            function (data) {
                if (data.status = 405) {
                    jQuery.post('/set_ajax_message/',
                        {
                            'message':'You tried to perform an invalid operation, if you are not sure why this error occurred then please contact customer care',
                            'status':'Error'
                        }, function (data) {
                            window.location.reload();
                        }
                    )
                } else if (data.status = 500) {
                    jQuery.post('/set_ajax_message/',
                        {
                            'message':'Oops! Error occurred while processing your request. Please try again later. If the problem persist please contact customer care!',
                            'status':'Error'
                        }, function (data) {
                            window.location.reload();
                        }
                    )
                }
            }
        );
    }
}