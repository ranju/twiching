"""
No surprise here. This is the default django urls page

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""
from django.contrib import admin
from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import settings
from twiching.apps.accounts.forms import UserRegistrationForm
from registration.views import register
import registration.backends.default.urls as regUrls
from twiching.apps.feeds.models import LatestEntriesFeed
#from satchmo_store.urls import urlpatterns
#from satchmo_utils import urlhelper
import twiching.apps.accounts.regbackend
#from satchmo_utils.urlhelper import delete_named_urlpattern
from twiching.apps.myecommerce.app import application
#delete_named_urlpattern(urlpatterns, 'satchmo_search')


admin.autodiscover()
urlpatterns = patterns('',
    url(r'^$', 'twiching.apps.general.views.home', name='home'),
    url(r'^featured/$', 'twiching.apps.general.views.show_featured', name='featured'),
    url(r'^dashboard_order_status$','twiching.apps.myecommerce.catelogue.views.change_order_status', name='change_ordr_status'),
    url(r'^classifieds/', include('twiching.apps.classifieds.urls')),
    url(r'^products_add_comment$','twiching.apps.myecommerce.catelogue.views.add_comment', name='add_product_comment'),
    url(r'^admin/', include(admin.site.urls)),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT}),
    (r'^products/(\d+)/$', 'twiching.apps.myecommerce.catelogue.views.product_view'),
    (r'^feeds/$', LatestEntriesFeed()),
    (r'^products/', include(application.urls)),
    url(r'^product/remove/(\d+)/$','twiching.apps.myecommerce.basket.views.delete_basket_item', name='delete_basket_item'),
    url(r'^cart/(\d+)/(\d+)/update=(\d+)$','twiching.apps.myecommerce.basket.views.update_basket', name='update_basket'),
    url(r'^wishlist/$', 'twiching.apps.accounts.views.wishList', name='wish-list'),
    url(r'^wishlist/add/(?P<id>\d+)/$', 'twiching.apps.accounts.views.add_to_wishlist', name='add-wish-list'),
    url(r'^wishlist/remove/(?P<id>\d+)/$', 'twiching.apps.accounts.views.remove_from_wishlist', name='remove-wish-list'),
    
    url(r'', include('social_auth.urls')),
    (r'^ckeditor/', include('ckeditor.urls')),

    #Tagging Auto suggest
    (r'^tagging_autocomplete/', include('tagging_autocomplete.urls')),
    #Registration Application URL
    url(r'^accounts/register/$', register, {'backend': 'registration.backends.default.DefaultBackend', 'form_class': UserRegistrationForm}, name='registration_register'),
    url('^accounts/login/$', 'twiching.apps.accounts.views.loginUser'),
    url('^accounts/logout/$', 'twiching.apps.accounts.views.logoutUser'),
    url(r'^accounts/myads/$', 'twiching.apps.classifieds.views.accountsAds'),
    url(r'^accounts/myads/(?P<id>\d+)/$','twiching.apps.classifieds.views.edit_classified',name='edit_classified'),
    url(r'^password_reset/$', 'django.contrib.auth.views.password_reset', name='password_reset'),
    (r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done'),
    (r'^reset/(?P<uidb36>[-\w]+)/(?P<token>[-\w]+)/$', 'django.contrib.auth.views.password_reset_confirm'),
    (r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete'),
    (r'^accounts/', include(regUrls)),
    (r'^accounts/', include('captcha.backends.default.urls')),
    (r'^accounts/$', 'twiching.apps.accounts.views.accountDashboard'),
    (r'^accounts/dashboard/$', 'twiching.apps.accounts.views.accountDashboard'),
    (r'^accounts/edit/$', 'twiching.apps.accounts.views.editAccount'),
    (r'^accounts/activate/$', 'twiching.apps.accounts.views.changeActiveStatus'),
    (r'^accounts/delete/$', 'twiching.apps.accounts.views.deleteAccount'),
    (r'^accounts/picture/$', 'twiching.apps.accounts.views.profilePictureEdit'),
    (r'^accounts/profile_crop/$', 'twiching.apps.accounts.views.cropProfileImage'),
    (r'^accounts/photography/(\d{1,3})/$', 'twiching.apps.photography.views.accountsPhotoList'),
    (r'^accounts/photography/$', 'twiching.apps.photography.views.accountsPhotoList'),
    (r'^accounts/password/$', 'twiching.apps.accounts.views.changePassword'),
    (r'^accounts/edit_photo_info/(\d{1,10})/$', 'twiching.apps.photography.views.editPhotoInformation'),
    (r'^follow/$', 'twiching.apps.accounts.views.followUser'),


    (r'^twichers/(\d{1,10})/$', 'twiching.apps.accounts.views.twichers'),
    (r'^twichers/$', 'twiching.apps.accounts.views.twichers'),

    (r'^photographs/delete_comment/$', 'twiching.apps.photography.views.deleteComment'),
    (r'^photo/report_abuse/$', 'twiching.apps.photography.views.reportAbuse'),
    (r'^photographs/save_comments/$', 'twiching.apps.photography.views.saveComments'),
    (r'^photographs/save_rating/$', 'twiching.apps.photography.views.savePhotoRating'),
    (r'^photographs/feedback/$', 'twiching.apps.photography.views.feedback'),
    (r'^photograph/delete/(\d{1,10})/$', 'twiching.apps.photography.views.deletePhoto'),
    (r'^photographs/(\d{1,5})/(\w+)/(\d{1,5})/$', 'twiching.apps.photography.views.showAllPhotos'),
    (r'^photographs/(\d{1,5})/(\w+)/$', 'twiching.apps.photography.views.showAllPhotos'),
    (r'^photographs/$', 'twiching.apps.photography.views.showAllPhotos'),

    (r'^user/([\w\-+.@]+)/photo/(\d+)/$', 'twiching.apps.photography.views.showPhoto'),
    (r'^user/([\w\-+.@]+)/photo/$', 'twiching.apps.photography.views.showPhoto'),
    (r'^user/([\w\-+.@]+)/photographs/(\w+)/$', 'twiching.apps.photography.views.userPhotoLists'),
    (r'^user/([\w\-+.@]+)/photographs/$', 'twiching.apps.photography.views.userPhotoLists'),
    (r'^user/([\w\-+.@]+)/contact/$', 'twiching.apps.accounts.views.userContact'),
    (r'^user/([\w\-+.@]+)/$', 'twiching.apps.accounts.views.userProfile'),

    (r'^profiles/(\w+)/(\d{1,10})/$', 'twiching.apps.accounts.views.listAllProfiles'),
    (r'^profiles/(\w+)/$', 'twiching.apps.accounts.views.listAllProfiles'),
    (r'^profiles/$', 'twiching.apps.accounts.views.listAllProfiles'),

    (r'^event/(\d+)/$', 'twiching.apps.events.views.event'),
    (r'^events/(\d+)/$', 'twiching.apps.events.views.eventsList'),
    (r'^events/$', 'twiching.apps.events.views.eventsList'),

    (r'^wall/ajax/(\w+)/$', 'twiching.apps.wall.views.wallAjax'),
    (r'^wall/$', 'twiching.apps.wall.views.Index'),

    #Shopping Cart
    #url(r'^products/(?P<slug>[-\w\d]+)/$', 'twiching.apps.ecommerce.store.views.get_product'),
    
#    (r'^products/(\d+)/$', 'twiching.apps.ecommerce.store.views.productDetail'),
#    (r'^products/$', 'twiching.apps.ecommerce.store.views.productList'),
    #(r'^cart/$', 'twiching.apps.ecommerce.store.views.showCart'),
    #(r'^cart/add/$', 'twiching.apps.ecommerce.store.views.addToCart'),
    #(r'^cart/qty/$', 'twiching.apps.ecommerce.store.views.set_quantity'),
    #(r'^cart/remove/$', 'twiching.apps.ecommerce.store.views.remove'),
    #(r'^shipping/$', 'payment.views.contact.contact_info'),


    #(r'^wishlist/move_to_cart/$', 'twiching.apps.ecommerce.wishlist.views.move_to_cart'),
    #(r'^wishlist/add/$', 'twiching.apps.ecommerce.wishlist.views.wishlist_add'),
    #(r'^wishlist/remove/$', 'twiching.apps.ecommerce.wishlist.views.wishlist_remove'),

    (r'^learningcenter/(\w+)/(\d+)/$', 'twiching.apps.atoz.views.page'),
    (r'^learningcenter/(\d+)/$', 'twiching.apps.atoz.views.index'),


    (r'^assistance/album/(\d+)$', 'twiching.apps.assistance.views.AlbumPhotos'),
    (r'^assistance/(\d+)/$', 'twiching.apps.assistance.views.Index'),
    (r'^assistance/$', 'twiching.apps.assistance.views.Index'),

    (r'^set_ajax_message/$', 'twiching.apps.general.views.setMessageForAjax'),
    (r'^subscribe_newsletter/$', 'twiching.apps.general.views.subscribeNewsletter'),
    (r'^unsubscribe_newsletter/$', 'twiching.apps.general.views.unsubscribeNewsletter'),
    (r'^resize-image/$', 'twiching.apps.general.views.resizeImage'),

    (r'^contact-us/$', 'twiching.apps.cms.views.ContactUs'),

    (r'^calculate-popularity/$', 'twiching.apps.photography.views.photoPopularity'), # calculates photo popularity
    (r'^calculate-user-popularity/$', 'twiching.apps.accounts.views.UserPopularity'), # calculates user popularity
    (r'^notification-status/$', 'twiching.apps.general.views.emailNotification'),

    (r'^search/(\w+)/([\w\-+._%)(!~\'*]+)/(\d+)/(\w+)/$', 'twiching.apps.search.views.searchSite'),
    (r'^search/(\w+)/([\w\-+._%)(!~\'*]+)/$', 'twiching.apps.search.views.searchSite'),

    (r'^adzone/', include('adzone.urls')),

    #CMS Application URL
    url(r'^(?P<slug>[-\w]+)/$', 'twiching.apps.cms.views.page'),
    url(r'^set_photo_mail$', 'twiching.apps.general.views.set_photos_mails'),
)

urlpatterns += staticfiles_urlpatterns()

#from django.conf import settings

#if settings.DEBUG:
    #urlpatterns += patterns('',
        #(r'^static/(.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    #)


#urlhelper.replace_urlpatterns(
#    urlpatterns,
#    [
#        #add override urls here (match the name from satchmo's url files)
#        url(r'^$', 'twiching.apps.general.views.home', {}, name='satchmo_shop_home'),
#        url(r'^$', 'twiching.apps.accounts.views.loginUser', {}, name='auth_login'),
#        url(r'^$', 'twiching.apps.accounts.views.logoutUser', {}, name='auth_logout'),
#        url(r'^$', 'twiching.apps.ecommerce.store.views.showCart', {}, name='satchmo_cart'),
#        url(r'^$', 'twiching.apps.ecommerce.store.views.addToCart', {}, name='satchmo_cart_add'),
#        url(r'^$', 'twiching.apps.ecommerce.store.views.set_quantity', {}, name='satchmo_cart_set_qty'),
#        url(r'^$', 'twiching.apps.ecommerce.store.views.remove', {}, name='satchmo_cart_remove'),
#        url(r'^$', 'twiching.apps.ecommerce.wishlist.views.wishlist_view', {}, name='satchmo_wishlist_view'),
#        url(r'^$', 'twiching.apps.ecommerce.wishlist.views.wishlist_remove', {}, name='satchmo_wishlist_remove'),
#        url(r'^$', 'twiching.apps.ecommerce.wishlist.views.wishlist_add', {}, name='satchmo_wishlist_add'),
#        url(r'^(?P<parent_slugs>([-\w]+/)*)?(?P<slug>[-\w]+)/$', 'twiching.apps.ecommerce.category.views.category_view', {}, name='satchmo_category'),
#        url(r'^accounts/register/$', register, {'backend': 'registration.backends.default.DefaultBackend', 'form_class': UserRegistrationForm}, name='registration_register'),
#    ]
#)
