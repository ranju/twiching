from django.db import models

from django.contrib.syndication.views import Feed
from django.core.urlresolvers import reverse
from twiching.apps.classifieds.models import Ad
from oscar.apps.catalogue.models import Product

class LatestEntriesFeed(Feed):
    title = "Latest products in Twiching"
    link = "/feeds"
    description = "Latest addition"
    description_template = 'feeds/description.html'
    def items(self):
        return Product.objects.order_by('-date_updated')[:5]
        #return Ad.objects.order_by('-added_date')[:5]

#    def item_title(self, item):
#        return item.title
#
#    def item_description(self, item):
#        return item.description

    

    # item_link is only needed if NewsItem has no get_absolute_url method.
#    def item_link(self, item):
#        return reverse('news-item', args=[item.pk])
