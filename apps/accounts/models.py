"""
Here we have the database related code for the accounts application

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

__author__ = 'Swaroop Shankar V <swaroop@opentechnics.com>'

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from twiching.thirdparty.countries.models import Country
from twiching.oscar.apps.catalogue.models import Product


class UserProfile(models.Model):
    """
    This class would define the extra fields that is required for a user who will be registring to the site. This model will
    be used for the Django Profile Application
    """
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    #Links to the user model and will have one to one relationship
    user = models.OneToOneField(User)

    #Other fields thats required for the registration
    gender = models.CharField(_('Gender'), max_length=1, choices=GENDER_CHOICES, null=True, blank=True)
    dob = models.DateField(_('Date of Birth'), null=True, blank=True)
    country = models.ForeignKey(Country, null=True, blank=True)
    address1 = models.CharField(_('Street Address Line 1'), max_length=250, null=True, blank=True)
    address2 = models.CharField(_('Street Address Line 2'), max_length=250, null=True, blank=True)
    city = models.CharField(_('City'), max_length=100, null=True)
    state = models.CharField(_('State/Province'), max_length=250, null=True, blank=True)
    pincode = models.CharField(_('Pincode'), max_length=15, null=True, blank=True)
    avatar = models.CharField(_('Avatar'), null=True, max_length=250, blank=True)
    twichers = models.BooleanField(_('Add to Twichers'), default=False)
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.user.first_name + ' ' + self.user.last_name

    class Meta:
        permissions = (
            ("view_account_dashboard", "Access Own Dashboard"),
            ("edit_account_information", "Edit account information"),
            ("edit_profile_picture", "Allow user to change his/her profile image"),
            ("view_profiles_listing", "Allow user to see all profiles as a list"),
        )


class ExtraInformation(models.Model):
    """
    This model will contain all the extra fields of a user
    """
    user = models.OneToOneField(User)
    about_me = models.TextField(_('About Me'), null=True, blank=True)
    contact_number = models.CharField(_('Contact Number'), max_length=25, null=True, blank=True)
    facebook_url = models.URLField(_('Facebook Url'), null=True, blank=True)
    google_plus_url = models.URLField(_('Google Plus Url'), null=True, blank=True)
    twitter_url = models.URLField(_('Twitter Url'), null=True, blank=True)
    flickr_url = models.URLField(_('Flickr Url'), null=True, blank=True)
    website_url = models.URLField(_('Website'), null=True, blank=True)
    awards = models.TextField(_('Awards'), null=True, blank=True)
    published_works = models.TextField(_('Published Works'), null=True, blank=True)
    agency = models.TextField(_('Agency Name'), max_length=150, null=True, blank=True)
    experience_year = models.IntegerField(_('Experience Years'), default=0, null=True, blank=True)
    experience_month = models.IntegerField(_('Experience Months'), default=0, null=True, blank=True)
    total_likes = models.IntegerField(_('Total Likes'), null=True, blank=True, default=0)
    total_dislikes = models.IntegerField(_('Total Dislikes'), null=True, blank=True, default=0)
    popularity = models.FloatField(_('Popularity'), null=True, blank=True, default=0.0)
    total_views = models.IntegerField(_('Views'), null=True, blank=True, default=0)
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.user.first_name


class UserMessage(models.Model):
    """
    This model will store all the messages send by users to other users through the site
    """
    send_user = models.ForeignKey(User, related_name="send_to_user")
    received_user = models.ForeignKey(User, related_name="received_by_user")
    subject = models.CharField(_('Subject'), max_length=250)
    content = models.TextField(_('Message'))
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        permissions = (
            ("send_user_message", 'Send message to another user'),
        )


class UserFollow(models.Model):
    """
    This model stores the list of other users a user follows
    """
    follower = models.ForeignKey(User, related_name='twiching_follower')  # The user who is following other users
    following = models.ForeignKey(User, related_name='twiching_following')  # The user who is followed by the follower
    created_on = models.DateTimeField(auto_now_add=True, null=True)


class UserWishList(models.Model):
    """
    This model stores the list of products in a user's wish list
    """
    user = models.ForeignKey(User, related_name='user_wishlist')  
    product = models.ForeignKey(Product, related_name='user_wishlist_product')
    created_on = models.DateTimeField(auto_now_add=True, null=True)
