"""
Forms for the registration and related features. Part of accounts package

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

__author__ = 'Swaroop Shankar V <swaroop@opentechnics.com>'

from django import forms
from django.utils.translation import ugettext_lazy as _
from registration.forms import RegistrationFormUniqueEmail
from twiching.thirdparty.countries.models import Country
from django.contrib.auth.models import Group
from datetime import datetime
from django.forms.widgets import RadioSelect
from captcha.fields import ReCaptchaField
from twiching.library.dateDropDownWidget import SelectDateWidget
from twiching.settings import REGISTRATION_MIN_AGE, REGISTRATION_MAX_AGE


class UserRegistrationForm(RegistrationFormUniqueEmail):
    """
    A custom build registration form which extends Registration Form Application and adds a few more fields to it
    """
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    #generate the years. We don't want anyone to register based on the limit set in settings file
    currentYear = datetime.now().year
    min_year = int(currentYear) - (REGISTRATION_MIN_AGE - 1)
    max_year = int(currentYear) - REGISTRATION_MAX_AGE
    year_range = range(max_year, min_year)

    #create the form fields
    retype_email = forms.EmailField(label=_('Re-type Email'), max_length=75, required=True)
    first_name = forms.CharField(label=_('First Name'), max_length=30, required=True)
    last_name = forms.CharField(label=_('Last Name'), max_length=30)
    captcha = ReCaptchaField()
    gender = forms.ChoiceField(widget=RadioSelect(), choices=GENDER_CHOICES, label=_('Gender'))
    dob = forms.DateField(widget=SelectDateWidget(attrs={'style': 'width: 60px;'}, years=year_range), label=_('Date of Birth'))
    user_type = forms.ModelChoiceField(queryset=Group.objects.all().order_by('id').exclude(name='Fashion Designer'), label=_('You are a'), required=True)
    terms = forms.BooleanField(widget=forms.CheckboxInput, label="Terms and Conditions", required=True)

    def clean_retype_email(self):
        re_email    = self.cleaned_data.get('retype_email')
        email       = self.cleaned_data.get('email')
        if email != re_email:
            raise forms.ValidationError('Email and Re-type email must be same')
        return re_email


class UserEditForm(forms.Form):
    """
    A custom build registration form which extends Registration Form Application and adds a few more fields to it
    """

    #we need to get the values to generate the default form values from the database table
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    first_name = forms.CharField(label=_('First Name'), max_length=30, required=True)
    last_name = forms.CharField(label=_('Last Name'), max_length=30)
    email = forms.EmailField(label=_('Email'), max_length=75, required=True)
    contact_number = forms.CharField(label=_('Contact Number'), max_length=25, required=False)
    gender = forms.ChoiceField(choices=GENDER_CHOICES, label=_('Gender'))
    dob = forms.DateField(
                          ('%d/%m/%Y',),
                          label=_('Date of Birth'),
                          )
    address1 = forms.CharField(max_length=250, label=('Address Line 1'))
    address2 = forms.CharField(max_length=250, label=_('Address Line 2'), required=False)
    city = forms.CharField(max_length=100, label=_('City'))
    state = forms.CharField(max_length=250, label=_('State/Province'))
    pincode = forms.CharField(max_length=15, label=_('Pincode'), required=False)
    country = forms.ModelChoiceField(queryset=Country.objects.all(), label='Country')
    profile_id = forms.IntegerField(widget=forms.HiddenInput, required=False)
    user_type = forms.ModelChoiceField(queryset=Group.objects.all().order_by('id'), label=_('You are a'), required=True)

    #additional information page
    about_me = forms.CharField(label=_('About Me'), widget=forms.Textarea(attrs={'cols': '19', 'rows': '4'}),  required=False)
    facebook_url = forms.URLField(label=_('Facebook Profile URL'), required=False)
    twitter_url = forms.URLField(label=_('Twitter Profile URL'), required=False)
    google_plus_url = forms.URLField(label=_('Google Plus Profile URL'), required=False)
    flickr_url = forms.URLField(label=_('Flickr Profile URL'), required=False)
    website_url = forms.URLField(label=_('Website URL'), required=False)
    awards = forms.CharField(label=_('Awards'), widget=forms.Textarea(attrs={'cols': '19', 'rows': '4'}),  required=False)
    published_works = forms.CharField(label=_('Works'), widget=forms.Textarea(attrs={'cols': '19', 'rows': '4'}),  required=False)
    agency = forms.CharField(max_length=150, label=_('Agency'), required=False)
    experience_year = forms.IntegerField(label=('Year(s)'), max_value=99, min_value=0, required=False, widget=forms.TextInput(attrs={'style': 'width:20px'}))
    experience_month = forms.IntegerField(label=_('Month(s)'), max_value=12, min_value=0, widget=forms.TextInput(attrs={'style': 'width:20px'}), required=False)


class ProfilePictureForm(forms.Form):
    avatar = forms.ImageField(label=_('Profile Picture'))
    old_image = forms.CharField(widget=forms.HiddenInput, required=False)


class LoginForm(forms.Form):
    """
    The login form fields
    """
    username = forms.CharField(widget=forms.TextInput(), label=_('Username'), max_length=30, required=True)
    password = forms.CharField(label=_('Password'), widget=forms.PasswordInput(), required=True)


class ContactUserForm(forms.Form):
    """
    This form will enable users to contact other users
    """
    subject = forms.CharField(max_length=150, label=_('Subject'), required=True)
    content = forms.CharField(label=_('Message'), widget=forms.Textarea())


class ChangePassword(forms.Form):
    """
    This form will enable users to contact other users
    """
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')  # cache the user object you pass in
        super(ChangePassword, self).__init__(*args, **kwargs)  # and carry on to init the form

    old_password = forms.CharField(label=('Old Password'), widget=forms.PasswordInput(attrs={'size': 10, 'style': 'width: 175px !important;'}), required=True)
    latest_password = forms.CharField(label=('New Password'), widget=forms.PasswordInput(attrs={'size': 10, 'style': 'width: 175px !important;'}), required=True)
    confirm_password = forms.CharField(label=('Confirm Password'), widget=forms.PasswordInput(attrs={'size': 10, 'style': 'width: 175px !important;'}), required=True)

    def clean_confirm_password(self):
        password1 = self.cleaned_data.get('latest_password')
        password2 = self.cleaned_data.get('confirm_password')

        if not password2:
            raise forms.ValidationError("You must confirm your password")
        if password1 != password2:
            raise forms.ValidationError("New and Confirm passwords should match")
        return self.cleaned_data

    def clean_new_password(self):
        new_password = self.cleaned_data.get('latest_password')
        if len(new_password) < 6:
            raise forms.ValidationError("Password length should be of at least 6 characters")
        return self.cleaned_data

    def clean_old_password(self):
        from django.contrib.auth import authenticate
        old_password = self.cleaned_data.get('old_password')
        user = authenticate(username=self.user.username, password=old_password)
        if not old_password:
            raise forms.ValidationError("Old password field cannot be left blank")
        if user is None:
            raise forms.ValidationError("Password do not match with the one in our records")
        return self.cleaned_data
