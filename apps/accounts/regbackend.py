"""
This is the regbackend which will define the signal function for saving the additional profile fields 

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

__author__ = 'Swaroop Shankar V <swaroop@opentechnics.com>'

from django.contrib.auth.models import User, Group
from twiching.apps.accounts.forms import UserRegistrationForm
from twiching.apps.accounts.models import UserProfile, ExtraInformation
from satchmo_store.contact.models import Contact, ContactRole
from registration.signals import user_registered
from social_auth.signals import socialauth_registered
from twiching.settings import SOCIAL_AUTH_DEFAULT_GROUP
from registration.models import RegistrationProfile

import datetime, time

def user_created(sender, user, request, **kwargs):
    form = UserRegistrationForm(request.POST)
    userObject = User.objects.get(username = form.data['username'])

    #Before saving dob we need to convert the date to a format acceptable to the database
    day = form.data['dob_day']
    month = form.data['dob_month']
    year = form.data['dob_year']
    dateTime = datetime.datetime(int(year), int(month), int(day))
    dateTuple = dateTime.timetuple()
    
    #let us save the user to a group selected by the user in usertype
    groupObject = Group.objects.get(pk = form.data['user_type'])
    userObject.groups.add(groupObject)
    
    userObject.first_name = form.data['first_name']
    userObject.last_name = form.data['last_name']
    userObject.save()
    
    data = UserProfile(user=user)
    data.gender = form.data['gender']
    data.dob = time.strftime("%Y-%m-%d",dateTuple)
    data.created_on = datetime.datetime.now()
    data.save()

#    contact = Contact()
#    contact.first_name = form.data['first_name']
#    contact.last_name = form.data['last_name']
#    contact.dob = time.strftime("%Y-%m-%d",dateTuple)
#    contact.email = form.data['email']
#    contact.user = user
#    contact.role = ContactRole.objects.get(name='Customer')
#    contact.save()

    extraInformation = ExtraInformation(user=user)
    extraInformation.popularity = 0
    extraInformation.total_dislikes = 0
    extraInformation.total_likes = 0
    extraInformation.save()
    return True

def social_auth_register(sender, user, response, details, **kwargs):

    dob = response.get('birthday', None)
    if dob != None:
        dateList = dob.split('/')
        dateTime = datetime.datetime(int(dateList[2]), int(dateList[0]), int(dateList[1]))
        dateTuple = dateTime.timetuple()

    gender = response.get('gender', None)
    if gender != None:
        if gender == 'male':
            gender = 'M'
        else:
            gender = 'F'

    user.first_name = details['first_name']
    user.last_name = details['last_name']
    user.email = details['email']
    user.save()

    groupObject = Group.objects.get(pk = SOCIAL_AUTH_DEFAULT_GROUP)
    user.groups.add(groupObject)

    data = UserProfile(user=user)
    data.gender = gender
    if dob != None:
        data.dob = time.strftime("%Y-%m-%d",dateTuple)
    data.created_on = datetime.datetime.now()
    data.save()

#    contact = Contact()
#    contact.first_name = details['first_name']
#    contact.last_name = details['last_name']
#    if dob != None:
#        contact.dob = time.strftime("%Y-%m-%d",dateTuple)
#    contact.email = details['email']
#    contact.user = user
#    contact.role = ContactRole.objects.get(name='Customer')
#    contact.save()

    extraInformation = ExtraInformation()
    extraInformation.user = user
    extraInformation.about_me = details.get('about_me', None)
    extraInformation.facebook_url = details.get('link', None)
    extraInformation.popularity = 0
    extraInformation.total_dislikes = 0
    extraInformation.total_likes = 0
    extraInformation.save()

    try:
        registrationProfileObject = RegistrationProfile.objects.get(user = user)
    except RegistrationProfile.DoesNotExist:
        #create a active status
        registrationProfileObject = RegistrationProfile()
        registrationProfileObject.user = user
        registrationProfileObject.activation_key = 'ALREADY_ACTIVATED'
        registrationProfileObject.save()
    else:
        registrationProfileObject.activation_key = 'ALREADY_ACTIVATED'
        registrationProfileObject.save()
    return True


user_registered.connect(user_created)
socialauth_registered.connect(social_auth_register, sender=None)