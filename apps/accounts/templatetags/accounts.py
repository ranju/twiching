"""
This script would define template tags which would display blocks which are related to users
"""
__author__ = 'Swaroop Shankar V <swaroop@opentechnics.com>'

from django import template
from django.contrib.auth.models import User, Group
from twiching.settings import MEDIA_URL, MEDIA_ROOT, PROFILE_IMAGE_UPLOAD_PATH, GROUP_ID_MAPPING, STATIC_URL
from django.utils.translation import ugettext_lazy as _
import os

register = template.Library()

@register.inclusion_tag('accounts/custom_tag_popular_users.html', takes_context=True)
def popular_users(context):
    userObject = User.objects.select_related('extrainformation', 'userprofile').\
    values('first_name','last_name','username','userprofile__avatar', 'userprofile__gender').\
    filter(is_active=1,is_staff=0,is_superuser=0).order_by('-extrainformation__popularity', '-last_login')[:10]
    userData = list()
    for user in userObject:
        profileImage = user['userprofile__avatar']
        if profileImage == '' or profileImage is None:
            if user['userprofile__gender'] == 'F':
                profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female-small.jpg"
            elif user['userprofile__gender'] == 'M':
                profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male-small.jpg"
        else:
            profileImagePath = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/"+profileImage
            if not os.path.exists(profileImagePath):
                #If the file does not exist then load the file from other sources
                if user['userprofile__gender'] == 'F':
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female-small.jpg"
                elif user['userprofile__gender'] == 'M':
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male-small.jpg"
            else:
                profileImage =  PROFILE_IMAGE_UPLOAD_PATH + "/"+profileImage

        values = {
            'username': user['username'],
            'first_name': user['first_name'],
            'last_name': user['last_name'],
            'avatar': profileImage,
            'group_id': None,
        }
        userData.append(values)
    return { 'userList':userData, 'STATIC_URL': STATIC_URL, 'title':_('Popular Users')}

@register.inclusion_tag('accounts/custom_tag_popular_users.html', takes_context=True)
def popular_photographers(context):
    userObject = User.objects.select_related('extrainformation', 'userprofile').\
                 values('first_name','last_name','username','userprofile__avatar', 'userprofile__gender').\
                 filter(is_active=1,is_staff=0,is_superuser=0, groups = Group(pk = int(GROUP_ID_MAPPING['PHOTOGRAPHER']))).order_by('-extrainformation__popularity', '-last_login')[:10]
    userData = list()
    for user in userObject:
        profileImage = user['userprofile__avatar']
        if profileImage == '' or profileImage is None:
            if user['userprofile__gender'] == 'F':
                profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female-small.jpg"
            elif user['userprofile__gender'] == 'M':
                profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male-small.jpg"
        else:
            profileImagePath = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/"+profileImage
            if not os.path.exists(profileImagePath):
                #If the file does not exist then load the file from other sources
                if user['userprofile__gender'] == 'F':
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female-small.jpg"
                elif user['userprofile__gender'] == 'M':
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male-small.jpg"
            else:
                profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/"+profileImage

        values = {
            'username': user['username'],
            'first_name': user['first_name'],
            'last_name': user['last_name'],
            'avatar': profileImage,
            'group_id': str(GROUP_ID_MAPPING['PHOTOGRAPHER']),
            }
        userData.append(values)
    return { 'userList':userData, 'STATIC_URL': STATIC_URL, 'title':_('Popular Photographers')}

@register.inclusion_tag('accounts/custom_tag_popular_users.html', takes_context=True)
def popular_models(context):
    userObject = User.objects.select_related('extrainformation', 'userprofile').\
                 values('first_name','last_name','username','userprofile__avatar', 'userprofile__gender').\
                 filter(is_active=1,is_staff=0,is_superuser=0, groups = Group(pk = int(GROUP_ID_MAPPING['MODEL']))).order_by('-extrainformation__popularity', '-last_login')[:10]
    userData = list()
    for user in userObject:
        profileImage = user['userprofile__avatar']
        if profileImage == '' or profileImage is None:
            if user['userprofile__gender'] == 'F':
                profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female-small.jpg"
            elif user['userprofile__gender'] == 'M':
                profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male-small.jpg"
        else:
            profileImagePath = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/"+profileImage
            if not os.path.exists(profileImagePath):
                #If the file does not exist then load the file from other sources
                if user['userprofile__gender'] == 'F':
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female-small.jpg"
                elif user['userprofile__gender'] == 'M':
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male-small.jpg"
            else:
                profileImage =  PROFILE_IMAGE_UPLOAD_PATH + "/"+profileImage

        values = {
            'username': user['username'],
            'first_name': user['first_name'],
            'last_name': user['last_name'],
            'avatar': profileImage,
            'group_id': str(GROUP_ID_MAPPING['MODEL']),
            }
        userData.append(values)
    return { 'userList':userData, 'MEDIA_URL': MEDIA_URL, 'STATIC_URL': STATIC_URL, 'PROFILE_IMAGE_UPLOAD_PATH': PROFILE_IMAGE_UPLOAD_PATH, 'title':_('Popular Models')}


