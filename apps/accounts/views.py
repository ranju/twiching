"""
View page for the accounts application.

@author: Swaroop Shankar V, Praveen P Nair
@organization: Opentechnics
@contact: swaroop@opentechnics.com, praveen@opentechnics.com
@copyright: (c) 2011, Twiching
"""

__author__ = 'Swaroop Shankar V <swaroop@opentechnics.com>'

import os
import hashlib
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.db.utils import DatabaseError
from django.db.models import Sum, Avg
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.http import Http404, HttpResponse
from django.contrib.contenttypes.models import ContentType
from djangoratings.models import Score
from endless_pagination.decorators import page_template
from sorl.thumbnail.shortcuts import delete
from mailsnake import MailSnake
from ajaxutils.decorators import ajax
from twiching.apps.accounts.models import UserProfile, ExtraInformation, UserMessage, UserFollow, UserWishList
from twiching.apps.photography.models import Photos, PhotoComments, PhotoRating
from registration.models import RegistrationProfile
from twiching.settings import PROFILE_IMAGE_UPLOAD_PATH, MEDIA_ROOT, DATE_FORMAT, MAILCHIMP_API_KEY, GROUP_ID_MAPPING
from twiching.apps.accounts.forms import LoginForm, UserEditForm, ProfilePictureForm
from twiching.config.zodiac_list import ZODIAC_SIGN
from twiching.apps.general.views import _sendNotification
from oscar.apps.catalogue.models import Product

def loginUser(request):
    """
    This form `validates the username and password and creates a login session
    """
    #if the user is already logged in the we dont need to display the login screen. Redirect the user to the home
    #page
    if request.user.is_authenticated():
        return redirect('/')
        #Following code validates and then log's a user into the system
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = str(request.POST.get('username'))
            password = str(request.POST.get('password'))

            if username.lower() == 'username':
                messages.error(request, _("Sorry you have provided an invalid username!"))
                return redirect('/accounts/login')

            if password.lower() == 'password':
                messages.error(request, _("Sorry you have provided an invalid password!"))
                return redirect('/accounts/login')

            redirectTo = request.GET.get('next', '')
            if redirectTo == '':
                redirectTo = request.POST.get('next', '')
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    if redirectTo != '':
                        return redirect(redirectTo)
                    else:
                        return redirect('/')
                else:
                    #check if the is_active status was set by the user or admin
                    regProfile = RegistrationProfile.objects.get(user=user)
                    if regProfile.activation_key == 'ALREADY_ACTIVATED':
                        login(request, user)
                        messages.error(request,
                            _('You need to active your account before using any personal features of this site'))
                        return redirect('/accounts/activate/')
                    else:
                        messages.error(request, _('Login Failed! Your account is not active.'))
            else:
                messages.error(request, _("Login Failed! Username and/or password where incorrect"))
    else:
        form = LoginForm()
    return render_to_response('accounts/login_form.html', {'form': form}, context_instance=RequestContext(request))


def logoutUser(request):
    """
    This method would destroy a user session and logs out that user from the system
    """
    logout(request)
    messages.success(request, _('You are successfully logged out from your account!'))
    return redirect('/accounts/login')


@login_required(login_url='/accounts/login/')
def accountDashboard(request):
    """
    This method would display various options to update user's profile and other related details
    """
    #let us check if the current logged in user got the permission to access this page
    if not request.user.has_perm('accounts.view_account_dashboard'):
        messages.error(request, _('You do not have permission to access that page!'))
        return redirect('/')

    if not request.user.is_superuser or not request.user.is_staff:
        try:
            regProfile = RegistrationProfile.objects.get(user=request.user)
        except RegistrationProfile.DoesNotExist:
            if not request.user.is_active:
                logout(request)
                messages.error(request,
                    _('Your account is not active. Please contact customer care to activate your account'))
                return redirect('/accounts/login')
        else:
            if regProfile.activation_key == 'ALREADY_ACTIVATED' and not request.user.is_active:
                messages.error(request,
                    _('You need to active your account before using any personal features of this site'))
                return redirect('/accounts/activate/')

    #check if the user is subscribed to the newsletter
    ms = MailSnake(MAILCHIMP_API_KEY)
    lists = ms.lists()
    status = ms.listMemberInfo(id=lists['data'][0]['id'], email_address=request.user.email)
    if status['success']:
        if status['data'][0]['status'] == 'unsubscribed':
            subscription_status = False
        else:
            subscription_status = True
    else:
        subscription_status = False

    #Continue rest of the processing
    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Account Dashboard', 'url': '/accounts/dashboard'}
    ]

    templateVariables = {
        'subscription_status': subscription_status,
        'breadcrumb': breadcrumb,
    }

    return render_to_response('accounts/dashboard.html', templateVariables, context_instance=RequestContext(request))


@login_required(login_url='/accounts/login/')
def changeActiveStatus(request):
    """
    This functionality will enable user to change their account status to either active or inactive
    """
    user = request.user
    if request.method == 'POST':
        if user.is_active:
            user.is_active = False
            user.save()
            messages.success(request, _('You have successfully deactivated your account!'))
            return redirect('/accounts/dashboard/')
        else:
            user.is_active = True
            user.save()
            messages.success(request, _('You have successfully Activated your account!'))
            return redirect('/accounts/dashboard/')
    else:
        if user.is_active:
            breadcrumb = [
                    {'name': 'Home', 'url': '/'},
                    {'name': 'Account Dashboard', 'url': '/accounts/dashboard'},
                    {'name': 'Deactivate Account', 'url': '/accounts/activate'}
            ]
            return render_to_response('accounts/deactivate_account.html', {'breadcrumb': breadcrumb},
                context_instance=RequestContext(request))
        else:
            breadcrumb = [
                    {'name': 'Home', 'url': '/'},
                    {'name': 'Account Dashboard', 'url': '/accounts/dashboard'},
                    {'name': 'Activate Account', 'url': '/accounts/activate'}
            ]
            return render_to_response('accounts/activate_account.html', {'breadcrumb': breadcrumb},
                context_instance=RequestContext(request))


@login_required(login_url='/accounts/login/')
def deleteAccount(request):
    """
    This action would delete the user account along with any other data assosiated with that account
    """
    user = request.user
    if request.method == 'POST':
        #get all the photographs uploaded by this user
        PhotoObject = Photos.objects.filter(user=user)
        for photo in PhotoObject:
            delete(photo.image)
#            try:
#                os.path.exists(MEDIA_ROOT + str(photo.image.name))
#            except IOError:
#                pass
            PhotoComments.objects.all().filter(photo=photo).delete()
            photo.delete()
        userProfileObject = UserProfile.objects.get(user=user)

        userProfileObject.delete()

        userExtraInformationObject = ExtraInformation.objects.get(user=user)
        userExtraInformationObject.delete()
        user.delete()

        logout(request)
        messages.success(request, _('Your account is removed from our system successfully!'))
        return redirect('/accounts/login')
    else:
        #load the delete warning page
        breadcrumb = [
                {'name': 'Home', 'url': '/'},
                {'name': 'Account Dashboard', 'url': '/accounts/dashboard'},
                {'name': 'Delete Account', 'url': '/accounts/delete'}
        ]
        return render_to_response('accounts/delete_account.html', {'breadcrumb': breadcrumb},
            context_instance=RequestContext(request))


@login_required(login_url='/accounts/login/')
def editAccount(request):
    """
    This method will allow the user to edit the user profile
    """
    import time
    import datetime
    from twiching.thirdparty.countries.models import Country

    try:
        regProfile = RegistrationProfile.objects.get(user=request.user)
    except RegistrationProfile.DoesNotExist:
        if not request.user.is_active:
            logout(request)
            messages.error(request,
                _('Your account is not active. Please contact customer care to activate your account'))
            return redirect('/accounts/login')
    else:
        if regProfile.activation_key == 'ALREADY_ACTIVATED' and not request.user.is_active:
            messages.error(request,
                _('You need to active your account before using any personal features of this site'))
            return redirect('/accounts/activate/')

    #let us check if the current logged in user got the permission to access this page
    if not request.user.has_perm('accounts.edit_account_information'):
        messages.error(request, _('You do not have permission to access that page!'))
        return redirect('/accounts/dashboard/')

    userId = request.user.id
    if not request.user.is_superuser or not request.user.is_staff:
        group = Group.objects.get(user=User(pk=userId))
        group_id = group.id
    else:
        messages.error(request, _(
            'You cannot edit your profile. If you think that this should not happen and is an error please contact our customer support!'))
        return redirect('/accounts/dashboard/')
    if request.method == 'POST':
        form = UserEditForm(request.POST)
        if form.is_valid():
            userObject = User.objects.get(pk=userId)

            newGroupId = request.POST.get('user_type')

            if newGroupId != group_id:
                #let us remove the previous group assigned to the user
                groupObject = Group.objects.get(pk=int(group_id))
                userObject.groups.remove(groupObject)
                #now assign the new group to the user
                groupObject = Group.objects.get(pk=int(newGroupId))
                userObject.groups.add(groupObject)

            userObject.first_name = request.POST.get('first_name')
            userObject.last_name = request.POST.get('last_name')
            userObject.email = request.POST.get('email')
            userObject.save()

            if form.data['profile_id'] == '':
                userProfileObject = UserProfile(user=userObject)
            else:
                userProfileObject = UserProfile.objects.get(pk=form.data['profile_id'])

            userProfileObject.gender = form.data['gender']
            #Before saving dob we need to convert the date to a format acceptable to the database
            dobFromForm = form.data['dob']
            dateList = dobFromForm.split('/')
            dateTime = datetime.datetime(int(dateList[2]), int(dateList[1]), int(dateList[0]))
            dateTuple = dateTime.timetuple()
            userProfileObject.dob = time.strftime("%Y-%m-%d", dateTuple)
            userProfileObject.address1 = form.data['address1']
            userProfileObject.address2 = form.data['address2']
            userProfileObject.state = form.data['state']
            userProfileObject.city = form.data['city']
            userProfileObject.pincode = form.data['pincode']
            userProfileObject.country = Country(iso=form.data['country'])
            userProfileObject.updated_on = datetime.datetime.now()
            userProfileObject.save()

            try:
                extraInfoObject = ExtraInformation.objects.get(user=User(pk=userId))
            except ExtraInformation.DoesNotExist:
                extraInfoObject = ExtraInformation()
                extraInfoObject.user = User(pk=userId)
            extraInfoObject.about_me = request.POST.get('about_me', None)
            extraInfoObject.contact_number = request.POST.get('contact_number', None)
            extraInfoObject.agency = request.POST.get('agency', None)
            extraInfoObject.awards = request.POST.get('awards', None)
            extraInfoObject.experience_month = request.POST.get('experience_month', 0)
            extraInfoObject.experience_year = request.POST.get('experience_year', 0)
            extraInfoObject.published_works = request.POST.get('published_works', None)
            facebookUrl = request.POST.get('facebook_url', None)
            if facebookUrl is not None and facebookUrl != '':
                if 'http://' not in facebookUrl and 'https://' not in facebookUrl:
                    facebookUrl = 'http://'+str(facebookUrl)
            extraInfoObject.facebook_url = facebookUrl

            twitterUrl = request.POST.get('twitter_url', None)
            if twitterUrl is not None and twitterUrl != '' :
                if 'http://' not in twitterUrl and 'https://' not in twitterUrl:
                    twitterUrl = 'http://'+str(twitterUrl)
            extraInfoObject.twitter_url = twitterUrl

            googlePlusUrl = request.POST.get('google_plus_url', None)
            if googlePlusUrl is not None and googlePlusUrl != '':
                if 'http://' not in googlePlusUrl and 'https://' not in googlePlusUrl:
                    googlePlusUrl = 'http://'+str(googlePlusUrl)
            extraInfoObject.google_plus_url = googlePlusUrl

            flickrUrl = request.POST.get('flickr_url', None)
            if flickrUrl is not None and flickrUrl != '':
                if 'http://' not in flickrUrl and 'https://' not in flickrUrl:
                    flickrUrl = 'http://'+str(flickrUrl)
            extraInfoObject.flickr_url = flickrUrl

            webUrl = request.POST.get('website_url', None)
            if webUrl is not None and webUrl != '':
                if 'http://' not in webUrl and 'https://' not in webUrl:
                    webUrl = 'http://'+str(webUrl)
            extraInfoObject.website_url = webUrl

            extraInfoObject.save()
            messages.success(request, _('Your profile information updated successfully!'))
    else:
        try:
            userProfile = request.user.get_profile()
            dob = userProfile.dob
            dateTuple = dob.timetuple()
            dob = time.strftime("%d/%m/%Y", dateTuple)

            #let us generate the default values that needs to be showed up on the edit form
            initial = {
                'first_name': request.user.first_name,
                'last_name': request.user.last_name,
                'email': request.user.email,
                'gender': userProfile.gender,
                'dob': dob,
                'address1': userProfile.address1,
                'address2': userProfile.address2,
                'city': userProfile.city,
                'pincode': userProfile.pincode,
                'state': userProfile.state,
                'country': userProfile.country,
                'profile_id': userProfile.id,
                'user_type': group_id,
                }
        except UserProfile.DoesNotExist:
            initial = {
                'first_name': request.user.first_name,
                'last_name': request.user.last_name,
                'email': request.user.email,
                'user_type': group_id,
                }

        try:
            extraInfoObject = ExtraInformation.objects.get(user=User(pk=userId))
        except ExtraInformation.DoesNotExist:
            pass
        else:
            initial['about_me'] = extraInfoObject.about_me
            initial['facebook_url'] = extraInfoObject.facebook_url
            initial['twitter_url'] = extraInfoObject.twitter_url
            initial['google_plus_url'] = extraInfoObject.google_plus_url
            initial['flickr_url'] = extraInfoObject.flickr_url
            initial['website_url'] = extraInfoObject.website_url
            initial['awards'] = extraInfoObject.awards
            initial['published_works'] = extraInfoObject.published_works
            initial['agency'] = extraInfoObject.agency
            initial['experience_year'] = extraInfoObject.experience_year
            initial['experience_month'] = extraInfoObject.experience_month

        form = UserEditForm(initial=initial)

    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Account Dashboard', 'url': '/accounts/dashboard'},
            {'name': 'Edit Account', 'url': '/accounts/edit'}
    ]

    templateVariables = {
        'GROUP_ID_MAPPING': GROUP_ID_MAPPING,
        'form': form,
        'breadcrumb': breadcrumb,
        'group_id': int(group_id),
    }
    return render_to_response('accounts/edit_account.html', templateVariables, context_instance=RequestContext(request))


@login_required(login_url='/accounts/login/')
def changePassword(request):
    try:
        regProfile = RegistrationProfile.objects.get(user=request.user)
    except RegistrationProfile.DoesNotExist:
        if not request.user.is_active:
            logout(request)
            messages.error(request,
                _('Your account is not active. Please contact customer care to activate your account'))
            return redirect('/accounts/login')
    else:
        if regProfile.activation_key == 'ALREADY_ACTIVATED' and not request.user.is_active:
            messages.error(request,
                _('You need to active your account before using any personal features of this site'))
            return redirect('/accounts/activate/')

    from twiching.apps.accounts.forms import ChangePassword

    if request.method == 'POST':
        form = ChangePassword(request.POST, user=request.user)
        if form.is_valid():
            #check if the password is correct
            new_password = request.POST.get('latest_password')
            user = request.user
            user.set_password(new_password)
            try:
                user.save()
            except DatabaseError:
                messages.error(request, _('Failed to update your password. Please try again!'))
            else:
                messages.success(request, _('Successfully changed your password'))
    else:
        form = ChangePassword(user=request.user)

    templateVariables = {
        'form': form
    }
    return render_to_response('accounts/change_password.html', templateVariables,
        context_instance=RequestContext(request))


@login_required(login_url='/accounts/login/')
def profilePictureEdit(request):
    """
    This function will provide an option for the user to upload their profile picture
    """
    from twiching.settings import MEDIA_ROOT, MEDIA_URL, PROFILE_IMAGE_UPLOAD_PATH
    from twiching.library.imageManipulation import resize
    from PIL import Image
    import os

    try:
        regProfile = RegistrationProfile.objects.get(user=request.user)
    except RegistrationProfile.DoesNotExist:
        if not request.user.is_active:
            logout(request)
            messages.error(request,
                _('Your account is not active. Please contact customer care to activate your account'))
            return redirect('/accounts/login')
    else:
        if regProfile.activation_key == 'ALREADY_ACTIVATED' and not request.user.is_active:
            messages.error(request,
                _('You need to active your account before using any personal features of this site'))
            return redirect('/accounts/activate/')

    #let us check if the current logged in user got the permission to access this page
    if not request.user.has_perm('accounts.edit_profile_picture'):
        messages.error(request, _('You do not have permission to access that page!'))
        return redirect('/accounts/dashboard/')

    #We require the existing image name so that we can show it on the edit page.
    try:
        userProfile = request.user.get_profile()
    except UserProfile.DoesNotExist:
        messages.error(request, _('You need to complete your profile information before uploading a profile picture!'))
        return redirect('/accounts/edit/')
    else:
        profileImage = userProfile.avatar
        profileId = userProfile.id
        noProfileImage = 0
        #Check if the image exist in crop folder else use the main image
        if profileImage != '' and profileImage is not None:
            profileImagePath = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/crop/" + profileImage
            if not os.path.exists(profileImagePath):
                #If the file does not exist then load the file from other sources
                profileImagePath = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/" + profileImage
                if not os.path.exists(profileImagePath):
                    if userProfile.gender == 'M':
                        profileImage = MEDIA_URL + PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male.jpg"
                    elif userProfile.gender == 'F':
                        profileImage = MEDIA_URL + PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female.jpg"
                    else:
                        profileImage = MEDIA_URL + PROFILE_IMAGE_UPLOAD_PATH + "/no-profile-medium.jpg"
                    noProfileImage = 1
                else:
                    profileImage = MEDIA_URL + PROFILE_IMAGE_UPLOAD_PATH + "/" + profileImage
            else:
                profileImage = MEDIA_URL + PROFILE_IMAGE_UPLOAD_PATH + "/crop/" + profileImage
        else:
            if userProfile.gender == 'M':
                profileImage = MEDIA_URL + PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male.jpg"
            elif userProfile.gender == 'F':
                profileImage = MEDIA_URL + PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female.jpg"
            else:
                profileImage = MEDIA_URL + PROFILE_IMAGE_UPLOAD_PATH + "/no-profile-medium.jpg"
            noProfileImage = 1

    #Create the form
    showOverlay = 0
    if request.method == 'POST':
        form = ProfilePictureForm(request.POST, request.FILES)
        if form.is_valid():
            fileName = _saveImage(request.FILES['avatar'])
            userProfileObject = UserProfile.objects.get(pk=profileId)
            userProfileObject.avatar = fileName
            userProfileObject.save()
            profileImage = MEDIA_URL + PROFILE_IMAGE_UPLOAD_PATH + "/" + fileName

            file = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + '/' + str(fileName)
            img = Image.open(file)
            size = (700, 550)
            resize(img, size, True, file)


            #delete old image
            oldProfileCropImagePath = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/crop/" + request.POST.get('old_image')
            oldProfileOriginalImagePath = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/" + request.POST.get('old_image')
            try:
                os.remove(oldProfileCropImagePath)
            except OSError:
                pass
            try:
                os.remove(oldProfileOriginalImagePath)
            except OSError:
                pass

            #Here we set whether overlay with the image needs to be displayed. In this overlay user will be able
            #to crop the image
            showOverlay = 1
    else:
        initial = {
            'old_image': userProfile.avatar,
            }
        form = ProfilePictureForm(initial=initial)
        showOverlay = 0

    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Account Dashboard', 'url': '/accounts/dashboard/'},
            {'name': 'Change Profile Picture', 'url': '/accounts/picture/'}
    ]
    return render_to_response('accounts/edit_profile_picture.html', {
        'form': form,
        'profileImage': profileImage,
        'showOverlay': showOverlay,
        'breadcrumb': breadcrumb,
        'noProfileImage': noProfileImage,
        'PROFILE_IMAGE_UPLOAD_PATH': PROFILE_IMAGE_UPLOAD_PATH,
        }, context_instance=RequestContext(request))


def cropProfileImage(request):
    """
    Action which will enable the user to crop their profile image
    """
    from twiching.settings import MEDIA_ROOT, PROFILE_IMAGE_UPLOAD_PATH
    from twiching.library.imageManipulation import imageCrop, resize
    from PIL import Image

    userProfile = request.user.get_profile()
    profileImage = userProfile.avatar

    file = MEDIA_ROOT + str(PROFILE_IMAGE_UPLOAD_PATH + '/') + str(profileImage)
    img = Image.open(file)
    width, height = img.size

    if request.method == 'GET':
        x = request.GET.get('x')
        y = request.GET.get('y')
        x1 = request.GET.get('x1')
        y1 = request.GET.get('y1')
        if x is not None and y is not None and x1 is not None and y1 is not None:
            imageInstance = Image.open(MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/" + profileImage)
            output = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/crop/" + profileImage
            if not imageCrop(imageInstance, x, y, x1, y1, output):
                output = "Failed"
            else:
                imageInstance = Image.open(output)
                size = (48, 48)
                outPut = MEDIA_ROOT + str(PROFILE_IMAGE_UPLOAD_PATH + '/thumbnail/') + "Thumb_" + profileImage
                resize(imageInstance, size, True, outPut, True)

                size = (116, 86)
                outPut = MEDIA_ROOT + str(PROFILE_IMAGE_UPLOAD_PATH + '/thumbnail/') + "Small_" + profileImage
                resize(imageInstance, size, True, outPut, True)

                output = "Completed"
            return render_to_response('common/ajax.html', {'output': output})
        else:
            templateVariables = {
                'profileImage': profileImage,
                'PROFILE_IMAGE_UPLOAD_PATH': PROFILE_IMAGE_UPLOAD_PATH,
                'width': width,
                'height': int(height),
                }
            return render_to_response('accounts/crop_profile_image.html', templateVariables,
                context_instance=RequestContext(request))


def userProfile(request, user_name):
    """
    This action would display the profile of a user
    """
    try:
        userObject = User.objects.get(username=user_name)
    except User.DoesNotExist:
        raise Http404

    #check if the user is active or not
    if not userObject.is_active:
        raise Http404

    is_superuser = userObject.is_superuser

    #check if the current user is authenticated
    if request.user.is_authenticated():
        current_user_id = request.user.id
    else:
        current_user_id = 0

    #check if the user got access to contact form
    if not request.user.has_perm('accounts.send_user_message'):
        show_contact = 0
    else:
        show_contact = 1

    #If the user is not a superuser then display the profile else never display the profile
    if not is_superuser:
        try:
            userProfile = userObject.get_profile()
        except UserProfile.DoesNotExist:
            profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/no-profile-medium.jpg"
            data = {
                'user_id': userObject.id,
                'first_name': userObject.first_name,
                'last_name': userObject.last_name,
                'email': userObject.email,
                'username': userObject.username,
                'gender': '',
                'dob': '',
                'country': '',
                'address1': '',
                'address2': '',
                'city': '',
                'state': '',
                'pincode': '',
                'avatar': profileImage,
                }
        else:
            try:
                userExtraInformation = ExtraInformation.objects.get(user=userObject.id)
            except ExtraInformation.DoesNotExist:
                userExtraInformation = None
            if userProfile.gender == 'M':
                profileNoImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male.jpg"
            elif userProfile.gender == 'F':
                profileNoImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female.jpg"
            else:
                profileNoImage = PROFILE_IMAGE_UPLOAD_PATH + "/no-profile-medium.jpg"
            profileImage = userProfile.avatar
            if profileImage == '' or profileImage is None:
                profileImage = profileNoImage
            else:
                profileImagePath = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/crop/" + profileImage
                if not os.path.exists(profileImagePath):
                    #If the file does not exist then load the file from other sources
                    profileImagePath = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/" + profileImage
                    if not os.path.exists(profileImagePath):
                        profileImage = profileNoImage
                    else:
                        profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/" + profileImage
                else:
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/crop/" + profileImage

            #let us populate the data to be displayed on the frontend
            try:
                country_iso = str(userProfile.country.iso).lower()
            except AttributeError:
                country_iso = None

            groupList = userObject.groups.values()
            groupName = list()
            for group in groupList:
                groupName.append(group['name'])

            groupName = ', '.join(groupName)

            #get the zodiac name and image
            try:
                zodiacSign = None
                zodiacDate = int(userProfile.dob.strftime('%m%d'))
                ZODIACSIGN = sorted(ZODIAC_SIGN)
                for zodiac in ZODIACSIGN:
                    if zodiacDate < zodiac:
                        zodiacSign = ZODIAC_SIGN[zodiac]
                        break
            except AttributeError:
                zodiacSign = None

            #get the total photo count
            photoCount = Photos.objects.filter(private = False, user = userObject).count()

            #get the user follower count
            followerCount = UserFollow.objects.filter(following = userObject).count()

            data = {
                'user_id': userObject.id,
                'first_name': userObject.first_name,
                'last_name': userObject.last_name,
                'email': userObject.email,
                'username': userObject.username,
                'gender': userProfile.gender,
                'dob': userProfile.dob,
                'country': userProfile.country,
                'address1': userProfile.address1,
                'address2': userProfile.address2,
                'city': userProfile.city,
                'state': userProfile.state,
                'pincode': userProfile.pincode,
                'avatar': profileImage,
                'country_iso': country_iso,
                'groupNames': groupName,
                'joined': userObject.date_joined,
                'zodiacSign': zodiacSign,
                'photoCount': photoCount,
                'twicher': int(userProfile.twichers),
                'followerCount': followerCount,
            }
            if userExtraInformation is not None:
                data['about_me'] = userExtraInformation.about_me
                data['agency'] = userExtraInformation.agency
                data['awards'] = userExtraInformation.awards
                data['contact_number'] = userExtraInformation.contact_number
                data['facebook_url'] = userExtraInformation.facebook_url
                data['flickr_url'] = userExtraInformation.flickr_url
                data['twitter_url'] = userExtraInformation.twitter_url
                data['google_plus_url'] = userExtraInformation.google_plus_url
                data['website_url'] = userExtraInformation.website_url
                data['published_works'] = userExtraInformation.published_works
                data['experience_year'] = userExtraInformation.experience_year
                data['experience_month'] = userExtraInformation.experience_month
            else:
                data['about_me'] = None
                data['agency'] = None
                data['awards'] = None
                data['contact_number'] = None
                data['facebook_url'] = None
                data['twitter_url'] = None
                data['flickr_url'] = None
                data['google_plus_url'] = None
                data['website_url'] = None
                data['published_works'] = None
                data['experience_year'] = None
                data['experience_month'] = None
                data['total_views'] = 0


        #let us check if the current logged in user is following this user or not
        if request.user.is_authenticated():
            if request.user.id != userObject.id:
                enable_follow = 1
                try:
                    UserFollow.objects.get(follower=request.user, following=userObject)
                except UserFollow.DoesNotExist:
                    show_follow_button = 1
                else:
                    show_follow_button = 0
            else:
                enable_follow = 0
                show_follow_button = 1
        else:
            enable_follow = 1
            show_follow_button = 1
        breadcrumb = [
                {'name': 'Home', 'url': '/'},
                {'name': userObject.first_name + ' ' + userObject.last_name,
                 'url': '/user/' + userObject.username + '/'},
        ]
    else:
        messages.error(request, _('Sorry! You don\'t have permission to access that profile'))
        return redirect('/')

    #get the profile views
    try:
        userExtraInformation = ExtraInformation.objects.get(user = userObject)
    except ExtraInformation.DoesNotExist:
        userExtraInformation = ExtraInformation()
        userExtraInformation.user = userObject
        userExtraInformation.total_views = 1
    else:
        if request.user.is_authenticated():
            if request.user.id != userObject.id:
                userExtraInformation.total_views += 1
        else:
            userExtraInformation.total_views += 1
    userExtraInformation.save()



    templateVariables = {
        'data': data,
        'breadcrumb': breadcrumb,
        'current_user_id': current_user_id,
        'show_contact': show_contact,
        'enable_follow': enable_follow,
        'show_follow_button': show_follow_button,
        'ZODIAC_SIGN': sorted(ZODIAC_SIGN),
        'profile_view': int(userExtraInformation.total_views),
        }

    return render_to_response('accounts/profile.html', templateVariables, context_instance=RequestContext(request))


@login_required(login_url='/accounts/login/')
def userContact(request, user_name=None):
    """
    This function would enable logged in user to send an email to another user
    """
    from django.template.loader import get_template
    from django.core.mail import EmailMultiAlternatives
    from django.template import Context

    from twiching.apps.accounts.forms import ContactUserForm

    if user_name is None:
        raise Http404

    try:
        userObject = User.objects.get(username=str(user_name))
    except User.DoesNotExist:
        raise Http404

        #check if the user is active or not
    if not userObject.is_active:
        raise Http404

    #check if the user got access to contact form
    if not request.user.has_perm('accounts.send_user_message'):
        messages.error(request, _('You don\'t have permission to send messages!'))
        return redirect('/user/' + str(user_name))

    #check if the current user is trying to send an email to himself
    if request.user.id == userObject.id:
        messages.error(request, _('We guess you really don\'t wish to send an email to yourself!'))
        return redirect('/user/' + str(user_name))

    if request.method == 'POST':
        form = ContactUserForm(request.POST)
        if form.is_valid():
            subject = request.POST.get('subject')
            content = request.POST.get('content')
            to_email = '"' + userObject.first_name + ' ' + userObject.last_name + '" <' + userObject.email + '>'
            from_email = '"' + request.user.first_name + ' ' + request.user.last_name + '" <do_not_reply@twiching.com>'

            textEmail = get_template('emails/user_contact_email.txt')
            htmlEmail = get_template('emails/user_contact_email.html')

            variables = {
                'name': userObject.first_name,
                'full_name': userObject.first_name + ' ' + userObject.last_name,
                'sender_name': request.user.first_name,
                'sender_fullname': request.user.first_name + ' ' + request.user.last_name,
                'subject': subject,
                'message': content,
                'sender_profile': 'http://' + request.META['HTTP_HOST'] + '/user/' + request.user.username + '/',
                'sender_contact_url': 'http://' + request.META[
                                                  'HTTP_HOST'] + '/user/' + request.user.username + '/contact/',
                }
            variables = Context(variables)
            text_content = textEmail.render(variables)
            html_content = htmlEmail.render(variables)
            message = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
            message.attach_alternative(html_content, "text/html")
            try:
                message.send()
            except Exception:
                messages.error(request,
                    _('Oops! Some error occurred while sending your message. Please try again later'))
                return redirect('/user/' + str(user_name) + '/contact/')
            else:
                #we need to save these details into the db
                userMessageObject = UserMessage()
                userMessageObject.received_user = User(pk=userObject.id)
                userMessageObject.send_user = User(pk=request.user.id)
                userMessageObject.subject = subject
                userMessageObject.content = content
                userMessageObject.save()
                messages.success(request, _('Your message send successfully!'))
                return redirect('/user/' + str(user_name) + '/contact/')
    else:
        form = ContactUserForm()

    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': userObject.first_name + ' ' + userObject.last_name, 'url': '/user/' + userObject.username + '/'},
            {'name': 'Contact', 'url': '/user/' + userObject.username + '/contact/'}

    ]

    templateVariables = {
        'userData': userObject,
        'breadcrumb': breadcrumb,
        'form': form,
        }

    return render_to_response('accounts/user_contact.html', templateVariables, context_instance=RequestContext(request))

@page_template("accounts/list_all_profiles_lazyloading.html")
def listAllProfiles(request, filter='popular', group_id=None,template='accounts/list_all_profiles.html', extra_context=None):
    """
    This functionality would display all the profiles
    """
    pList = []
    if filter == 'popular':
        if group_id is not None:
            try:
                userObject = User.objects.select_related('extrainformation').filter(is_active=1, is_staff=0,
                    is_superuser=0, groups=Group(pk=int(group_id))).order_by('-extrainformation__popularity', '-last_login')
            except User.DoesNotExist:
                raise Http404
        else:
            group_id = 0
            try:
                userObject = User.objects.select_related('extrainformation').filter(is_active=1, is_staff=0,
                    is_superuser=0).order_by('-extrainformation__popularity', '-last_login')
            except User.DoesNotExist:
                raise Http404
    elif filter == 'latest':
        if group_id is not None:
            try:
                userObject = User.objects.filter(is_active=1, is_staff=0, is_superuser=0, groups=Group(pk=group_id)).order_by('-last_login')
            except User.DoesNotExist:
                raise Http404
        else:
            group_id = 0
            try:
                userObject = User.objects.filter(is_active=1, is_staff=0, is_superuser=0).order_by('-last_login')
            except User.DoesNotExist:
                raise Http404
    else:
        raise Http404

    for userData in userObject:
        try:
            userProfile = userData.get_profile()
        except UserProfile.DoesNotExist:
            profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/no-profile-medium.jpg"
            countryIso = None
        else:
            profileImage = userProfile.avatar
            if profileImage == '' or profileImage is None:
                if userProfile.gender == 'F':
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female-small.jpg"
                elif userProfile.gender == 'M':
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male-small.jpg"
            else:
                profileImagePath = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/" + profileImage
                if not os.path.exists(profileImagePath):
                    #If the file does not exist then load the file from other sources
                    if userProfile.gender == 'F':
                        profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female-small.jpg"
                    elif userProfile.gender == 'M':
                        profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male-small.jpg"
                else:
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/" + profileImage
            try:
                countryIso = str(userProfile.country.iso).lower()
            except AttributeError:
                countryIso = None
            #get group name
        try:
            groupName = Group.objects.get(user=User(userData.id)).name
        except Group.DoesNotExist:
            groupName = ''

        data = {
            'username': userData.username,
            'first_name': userData.first_name,
            'last_name': userData.last_name,
            'full_name': userData.first_name + ' ' + userData.last_name,
            'avatar': profileImage,
            'group_name': groupName,
            'last_login': userData.last_login,
            'country_iso': countryIso,
            'twichers': userProfile.twichers
        }
        pList.append(data)

    #let us get the group names
    groupList = Group.objects.all()

    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Profiles', 'url': '/profiles'},

    ]

    templateVariables = {
        'data': pList,
        'breadcrumb': breadcrumb,
        'DATE_FORMAT': DATE_FORMAT,
        'groupList': groupList,
        'filter': filter,
        'group_id': int(group_id),
        }
    if extra_context is not None:
        templateVariables.update(extra_context)
    if request.is_ajax():
        return render_to_response(template, templateVariables,
        context_instance=RequestContext(request))
    return render_to_response(template, templateVariables,
        context_instance=RequestContext(request))

@page_template("accounts/twichers.html")
def twichers(request, groupId=0, filter='popular', categoryId=0,template='accounts/twichers.html', extra_context=None):
    """
    This function would display all the users based on group who are assigned as twichers
    """
    groupName = None

    if not groupId:
        userData = UserProfile.objects.filter(twichers=True, user__is_active=1, user__is_staff=0,
            user__is_superuser=0).order_by('-user__last_login')
    else:
        userData = UserProfile.objects.filter(user__groups__id=int(groupId), twichers=True, user__is_active=1,
            user__is_staff=0, user__is_superuser=0).order_by('-user__last_login')

    pList = []
    if len(userData) > 0:
        for uData in userData:
            userD = User.objects.get(pk=uData.user_id)
            profileImage = uData.avatar
            if profileImage == '' or profileImage is None:
                if uData.gender == 'F':
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female-small.jpg"
                elif uData.gender == 'M':
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male-small.jpg"
            else:
                profileImagePath = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/crop/" + profileImage
                if not os.path.exists(profileImagePath):
                    #check if the image exist in the main photography path
                    profileImagePath = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/" +  profileImage
                    if not os.path.exists(profileImagePath):
                        #If the file does not exist then load the file from other sources
                        if uData.gender == 'F':
                            profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female-small.jpg"
                        elif uData.gender == 'M':
                            profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male-small.jpg"
                    else:
                        profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/" + profileImage
                else:
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/crop/" + profileImage
            groupName = userD.groups.values_list('name', flat=True)
            groupName = str(groupName[0])
            try:
                country_iso = str(uData.country.iso).lower()
            except AttributeError:
                country_iso = ''
            data = {
                'username': uData.user.username,
                'first_name': uData.user.first_name,
                'last_name': uData.user.last_name,
                'full_name': uData.user.first_name + ' ' + uData.user.last_name,
                'avatar': profileImage,
                'last_login': uData.user.last_login,
                'country_iso': country_iso,
                'groupName': groupName.capitalize(),
                }
            pList.append(data)

    if groupName is None:
        raise Http404

    if groupId:
        breadcrumb = [
                {'name': 'Home', 'url': '/'},
                {'name': 'Twichers', 'url': '/twichers'},
                {'name': str(groupName) + 's', 'url': '/twichers/' + str(groupId)}

        ]
    else:
        breadcrumb = [
                {'name': 'Home', 'url': '/'},
                {'name': 'Twichers', 'url': '/twichers'},

        ]
    templateVariables = {
        'data': pList,
        'userData': userData,
        'DATE_FORMAT': DATE_FORMAT,
        'breadcrumb': breadcrumb,
        'groupName': groupName,
        'groupId': int(groupId),
        }

    if extra_context is not None:
        templateVariables.update(extra_context)
    return render_to_response(template, templateVariables, context_instance=RequestContext(request))


@ajax(login_required=True, require_POST=True)
def followUser(request):
    '''
    This action would be an ajax request and would enable one user to follow another user or un-follow a user based on the status
    '''

    #assert False
    following_user = request.POST.get('following', None)
    if following_user is None:
        return {
            'status': 'error',
            'error': 'field_value_missing'
        }

    if int(following_user) == request.user.id:
        return {
            'status': 'error',
            'error': 'same_user'
        }

    #check if the current user is already following the following user
    try:
        followObject = UserFollow.objects.get(follower=request.user, following=User(pk=int(following_user)))
    except UserFollow.DoesNotExist:
        #means the current logged in user is not a follower of the following user
        followObject = UserFollow()
        followObject.follower = request.user
        followObject.following = User(pk=int(following_user))
        try:
            followObject.save()
        except DatabaseError:
            return {
                'status': 'error',
                'error': 'database_error',
                'type': 'follow'
            }
        else:
            followingUser = User.objects.get(pk=int(following_user))
            follower_name = request.user.first_name + ' ' + request.user.last_name
            follower_profile_link = 'http://' + request.META['HTTP_HOST'] + '/user/' + request.user.username
            subject = follower_name + ' is now following you in Twiching.com'

            to_email = followingUser.email
            http_host = request.META['HTTP_HOST']
            following_name = followingUser.first_name + ' ' + followingUser.last_name
            email_template = 'user_following'
            email_sent_status = _sendNotification(
                subject=subject,
                to_email=to_email,
                http_host=http_host,
                commenter_name=follower_name,
                commenter_profile_link=follower_profile_link,
                photo_owner_name = following_name,
                email_template = email_template
            )
            return {
                'status': 'success',
                'type': 'follow'
            }
    else:
        try:
            followObject.delete()
        except DatabaseError:
            return {
                'status': 'error',
                'error': 'database_error',
                'type': 'unfollow'
            }
        else:
            return {
                'status': 'success',
                'type': 'unfollow'
            }

def UserPopularity(request):
    """
    This action would calculate how popular a user is among all the other users.
    Calculation is based on various criteria like total photos uploaded,
    photo popularity of photos uploaded, how many times they login into the system
    etc
    """
    #let us get all active users and users who are not superusers
    userObject = User.objects.filter(is_superuser = False, is_active = True).only('id')
    userTotalCount = userObject.count()

    totalPhotos = Photos.objects.filter(private = 0).count()
    totalFeaturedPhotos = Photos.objects.filter(private = 0, featured = 1).count()

    content_type = ContentType.objects.get_for_model(PhotoRating)
    #get the whole star rating
    avg_num_star_votes = Score.objects.filter(content_type=content_type).aggregate(Avg('votes'))
    avg_num_star_votes = avg_num_star_votes['votes__avg']
    avg_star_rating = Score.objects.filter(content_type=content_type).aggregate(Avg('score'))
    avg_star_rating = avg_star_rating['score__avg']

    #get whole like score
    total_all_likes = PhotoRating.objects.aggregate(Sum('likes'))
    total_all_dislikes = PhotoRating.objects.aggregate(Sum('dislikes'))
    avg_num_like_votes = PhotoRating.objects.exclude(total_votes=0).aggregate(Avg('total_votes'))
    avg_num_like_votes = avg_num_like_votes['total_votes__avg']
    avg_like_rating = total_all_likes['likes__sum'] / (total_all_likes['likes__sum'] + total_all_dislikes['dislikes__sum'])

    userList = list()
    popularity_list = list()
    for users in userObject:
        #get the count of photos uploaded by this user
        photoCount = Photos.objects.filter(user = users, private = 0).count()
        if photoCount:
            photoCountPercentage = round((photoCount/totalPhotos)*100,2)
        else:
            photoCountPercentage = 0

        #get percentage of photos uploaded made to featured from this user
        photoFeaturedCount = Photos.objects.filter(user = users, private = 0, featured = 1).count()
        if photoFeaturedCount:
            featuredPercentage = round((photoFeaturedCount/totalFeaturedPhotos)*100, 2)
        else:
            featuredPercentage = 0

        #calculate star popularity based on bayesian average method
        this_num_star_votes = PhotoRating.objects.filter(photo__user = users, photo__private = 0).aggregate(Sum('star_rating_votes'))
        this_num_star_votes = this_num_star_votes['star_rating_votes__sum']
        this_star_rating = PhotoRating.objects.filter(photo__user = users, photo__private = 0).aggregate(Sum('star_rating_score'))
        this_star_rating = this_star_rating['star_rating_score__sum']
        try:
            starPopularityRating = ( (avg_num_star_votes * avg_star_rating) + (this_num_star_votes * this_star_rating) ) / (avg_num_star_votes + this_num_star_votes)
        except Exception:
            starPopularityRating = 0

        #calculate the like popularity
        userPhotoLike = PhotoRating.objects.filter(photo__user = users, photo__private = 0).aggregate(Sum('likes'))
        userPhotoLike = userPhotoLike['likes__sum']
        userPhotoDislike = PhotoRating.objects.filter(photo__user = users, photo__private = 0).aggregate(Sum('dislikes'))
        userPhotoDislike = userPhotoDislike['dislikes__sum']
        try:
            this_num_like_votes = userPhotoLike + userPhotoDislike
        except Exception:
            this_num_like_votes = 0

        try:
            this_like_rating = userPhotoLike/this_num_star_votes
        except Exception:
            this_like_rating = 0

        try:
            likePopularityRating = ( (avg_num_like_votes * avg_like_rating) + (this_num_like_votes * this_like_rating) ) / (avg_num_like_votes + this_num_like_votes)
        except Exception:
            likePopularityRating = 0

        #get the total followers for the current user
        followerCount = UserFollow.objects.filter(following = users).count()
        try:
            followerPercentage = (followerCount/userTotalCount)*100
        except Exception:
            followerPercentage = 0

        #let us now calculate the popularity
        userPopularity = round((photoCountPercentage + featuredPercentage + starPopularityRating + likePopularityRating + followerPercentage)/5, 2)*100
        try:
            extraInformation = ExtraInformation.objects.get(user = users)
        except ExtraInformation.DoesNotExist:
            extraInformation = ExtraInformation()
            extraInformation.user = users
            extraInformation.popularity = userPopularity
        else:
            extraInformation.popularity = userPopularity
        extraInformation.save()
        userList.append(users)

    extraInformation = ExtraInformation.objects.exclude(user__in = userList)
    extraInformation.update(popularity = 0)

    return HttpResponse('Done')

def _saveImage(image):
    """
    This function is to handle uploaded image file
    """
    from twiching.settings import MEDIA_ROOT, PROFILE_IMAGE_UPLOAD_PATH
    from twiching.library.imageManipulation import resize
    from PIL import Image
    import time
    from os import remove

    fileName = image._get_name()
    fileName = hashlib.md5(fileName).hexdigest() + str(int(time.time()))
    fileNameWithOld = fileName + fileName
    fd = open('%s/%s' % (MEDIA_ROOT, str(PROFILE_IMAGE_UPLOAD_PATH + '/') + str(fileNameWithOld)), 'wb')
    for chunk in image.chunks():
        fd.write(chunk)
    fd.close()

    imagePath = MEDIA_ROOT + str(PROFILE_IMAGE_UPLOAD_PATH + '/') + str(fileNameWithOld)
    imageInstance = Image.open(imagePath)
    size = (800, 1024)
    fileName = str(fileName) + '.jpg'
    outPut = MEDIA_ROOT + str(PROFILE_IMAGE_UPLOAD_PATH + '/') + fileName

    #Let us now resize the image
    resize(imageInstance, size, True, outPut)

    size = (48, 48)
    outPut = MEDIA_ROOT + str(PROFILE_IMAGE_UPLOAD_PATH + '/thumbnail/') + "Thumb_" + fileName
    resize(imageInstance, size, True, outPut, True)

    size = (116, 86)
    outPut = MEDIA_ROOT + str(PROFILE_IMAGE_UPLOAD_PATH + '/thumbnail/') + "Small_" + fileName
    resize(imageInstance, size, True, outPut, True)

    try:
        remove(imagePath)
    except OSError:
        pass

    return fileName

@login_required(login_url='/accounts/login/')
def wishList(request):
    wishlist = UserWishList.objects.filter(user=request.user)
    return render_to_response('accounts/wishlist.html',{'wishlist':wishlist}, context_instance=RequestContext(request))

@login_required(login_url='/accounts/login/')
def add_to_wishlist(request, id):
    try:
        product = Product.objects.get(id=id)
    except:
        messages.error(request,'Item not found.')
        return redirect('/products/')

    product_exist = UserWishList.objects.filter(user=request.user,product=product )
    if product_exist:
        messages.error(request,'Product already exist in your wishlist')
        return redirect('/wishlist/')
    wishlist = UserWishList()
    wishlist.user = request.user
    wishlist.product = product
    wishlist.save()
    return redirect('/wishlist/')


@login_required(login_url='/accounts/login/')
def remove_from_wishlist(request, id):
    try:
        list = UserWishList.objects.get(id=id)
    except:
        messages.error(request,'Item not found.')
        return redirect('/wishlist/')

    list.delete()
    return redirect('/wishlist/')





















