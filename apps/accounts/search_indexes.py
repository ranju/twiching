"""
Search index implementation for user

@author: Aswathy Rajesh
@organization: Opentechnics
@contact: aswathi.r@opentechnics.com
@copyright: (c) 2012, Twiching
"""
import datetime
#from haystack import indexes
#from haystack import site
from django.contrib.auth.models import User


#class UserIndex(indexes.SearchIndex, indexes.Indexable):
    #text = indexes.CharField(document=True, use_template=True)
    #first_name = indexes.CharField(model_attr='first_name')
    #last_name = indexes.CharField(model_attr='last_name')
    #username = indexes.CharField(model_attr='username')
    #join_date = indexes.DateTimeField(model_attr='date_joined')

    #def get_model(self):
        #return User
    
    #def index_queryset(self):
     #   """Used when the entire index for model is updated."""
        #return User.objects.filter(date_joined__lte=datetime.datetime.now())

#site.register(User, UserIndex)
