"""
Admin related code for accounts application goes here

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

__author__ = 'Swaroop Shankar V <swaroop@opentechnics.com>'

from django.contrib import admin
from django.contrib.auth.models import User
from twiching.apps.accounts.models import  UserProfile, ExtraInformation
from django.contrib.auth.admin import UserAdmin

admin.site.unregister(User)

class UserProfileInline(admin.StackedInline):
    model = UserProfile

class ExtraInformationInline(admin.StackedInline):
    model = ExtraInformation
    
class UserProfileAdmin(UserAdmin):
    inlines = [UserProfileInline, ExtraInformationInline,]

admin.site.register(User, UserProfileAdmin)