"""
This view will have functions and action which do not require an application as such for its implementation

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""
import os
import random
from PIL import Image
from django.shortcuts import render_to_response
from django.template import RequestContext,Context
from django.contrib import messages
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponse, Http404
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
from ajaxutils.decorators import ajax
from mailsnake import MailSnake
from django.shortcuts import render_to_response, redirect
from endless_pagination.decorators import page_template
from twiching.apps.accounts.forms import LoginForm
from twiching.apps.general.models import Banner, EmailNotification, StoreFeatures
from twiching.settings import MAILCHIMP_API_KEY, MEDIA_ROOT, PRODUCT_UPLOAD_PATH, MEDIA_URL, ENABLED_GROUP_ID_MAPPING
#from twiching.library.imageManipulation import resize
from twiching.apps.classifieds.models import Ad
from twiching.oscar.apps.catalogue.models import Category

def home(request):
    form = LoginForm()
    bannerList = Banner.objects.all().filter(area=1)
    categories = Category.objects.all()
    if request.user.is_authenticated():
        group_id = request.user.groups.values_list('id',flat=True)
        if not len(group_id):
            group_id = int(ENABLED_GROUP_ID_MAPPING[random.choice(ENABLED_GROUP_ID_MAPPING.keys())])
        else:
            group_id = int(random.choice(group_id))
    else:
        #group_id = int(ENABLED_GROUP_ID_MAPPING[random.choice(ENABLED_GROUP_ID_MAPPING.keys())])
	group_id = 1
    ads = Ad.objects.filter(approve=True).order_by('-added_date')[:3]
    templateVariables = {
        'form': form,
        'bannerList': bannerList,
        'group_id': group_id,
        'categories': categories,
        'ads': ads,
    }
    return render_to_response('general/homepage.html', templateVariables,
        context_instance=RequestContext(request))


def get_page_numbers(current_page, num_pages):
    return ("previous", 1, None, current_page, "next")

@login_required(login_url='/accounts/login/')
def emailNotification(request):
    """
    Un-subscribe an email from site notifications
    """
    subscriptionStatus = True
    if request.method == 'POST':
        #check if the email is already there in the un subscribe list
        try:
            notifyObject = EmailNotification.objects.get(email = request.user.email)
        except EmailNotification.DoesNotExist:
            notifyObject = EmailNotification()
            notifyObject.email = request.user.email

            try:
                notifyObject.save()
            except Exception:
                messages.error(request, _('Sorry error occurred while removing you from the notification list. Please try again later !'))
                subscriptionStatus = True
            else:
                messages.success(request, _('Your email is successfully removed from the notification list!'))
                subscriptionStatus = False
        else:
            try:
                notifyObject.delete()
            except Exception:
                messages.error(request, _('Sorry error occurred while adding you to the notification list. Please try again later !'))
                subscriptionStatus = False
            else:
                messages.success(request, _('Your email is successfully subscribed to the notification list!'))
                subscriptionStatus = True
    else:
        try:
            notifyObject = EmailNotification.objects.get(email = request.user.email)
        except EmailNotification.DoesNotExist:
            subscriptionStatus = True
        else:
            subscriptionStatus = False


    #Continue rest of the processing
    if subscriptionStatus:
        breadcrumb = [
                {'name': 'Home', 'url': '/'},
                {'name': 'Un-Subscribe from notification', 'url': '/notification-status/'},
        ]
    else:
        breadcrumb = [
                {'name': 'Home', 'url': '/'},
                {'name': 'Subscribe to notification', 'url': '/notification-status/'},
        ]

    templateVariables = {
        'breadcrumb': breadcrumb,
        'subscriptionStatus': subscriptionStatus,
    }

    return render_to_response('general/unsubscribe_from_notification.html', templateVariables,
        context_instance=RequestContext(request))


@ajax(require_POST=True)
def setMessageForAjax(request):
    '''
    This action is to set the django message from an ajax request.
    '''
    message = request.POST.get('message', None)
    message_status = request.POST.get('status', 'Error')
    if message_status == 'Error':
        messages.error(request, _(message))
    elif message_status == 'Info':
        messages.info(request, _(message))
    elif message_status == 'Success':
        messages.success(request, _(message))
    elif message_status == 'Warning':
        messages.warning(request, _(message))
    return HttpResponse('completed')


@ajax(require_POST=True)
def subscribeNewsletter(request):
    '''
    Subscribe to mailchimp newsletter
    '''
    email = request.POST.get('newsletter_email', None)
    if email is None:
        return {
            'status': 'error',
            'error': 'email_missing'
        }

    ms = MailSnake(MAILCHIMP_API_KEY)
    lists = ms.lists()
    try:
        ms.listSubscribe(
            id=lists['data'][0]['id'],
            email_address=email,
            update_existing=True,
            double_optin=False,
        )
    except Exception:
        return {
            'status': 'error',
            'error': 'network_error'
        }
    else:
        return {
            'status': 'success'
        }


@ajax(require_POST=True)
def unsubscribeNewsletter(request):
    '''
    Un-Subscribe from mailchimp newsletter
    '''
    email = request.POST.get('newsletter_email', None)
    if email is None:
        return {
            'status': 'error',
            'error': 'email_missing'
        }

    ms = MailSnake(MAILCHIMP_API_KEY)
    lists = ms.lists()
    try:
        ms.listUnsubscribe(
            id=lists['data'][0]['id'],
            email_address=email,
        )
    except:
        return {
            'status': 'error',
            'error': 'network_error'
        }
    else:
        return {
            'status': 'success'
        }


@ajax(require_POST=True)
def resizeImage(request):
    """
    This action is to resize images using ajax call. Depends on solr-thumbnails
    """
    image = request.POST.get('image', '')
    width = request.POST.get('width', 0)
    height = request.POST.get('height', 0)
    crop = request.POST.get('crop', 0)

    widthheight = str(width) + 'x' + str(height)

    #get file name
    fileName = os.path.basename(image)

    #now let us create the special file name for this resize
    resizeFileName = widthheight + '_' + fileName

    #now check if the file already exist. If yes then return the file name else resize it
    resizeImagePath = MEDIA_ROOT + PRODUCT_UPLOAD_PATH + '/' + resizeFileName
    try:
        Image.open(resizeImagePath)
    except IOError:
        #let us resize the image
        imagePath = MEDIA_ROOT + image
        try:
            imageInstance = Image.open(imagePath)
        except IOError:
            return {
                'status': 'error',
                'error': 'IOError'
            }
        else:
            size = (int(width), int(height))
            if crop == 1:
                resize(imageInstance, size, True, resizeImagePath)
            else:
                resize(imageInstance, size, False, resizeImagePath)

    urlImagePath = MEDIA_URL + PRODUCT_UPLOAD_PATH + '/' + resizeFileName
    return {
        'status': 'success',
        'im': urlImagePath,
        }

def _sendNotification(**kwargs):
    """
    Sends notification to users
    """
    to_email = kwargs.get('to_email')
    #check if the user have unsubscribed from the notification
    try:
        EmailNotification.objects.get(email = to_email)
    except EmailNotification.DoesNotExist:
        pass
    else:
        return False
    photo_owner_name = kwargs.get('photo_owner_name')
    #to_email = photo_owner_name + "<" + to_email + ">"
    subject = kwargs.get('subject')
    content = kwargs.get('content')
    http_host = kwargs.get('http_host')
    commenter_name = kwargs.get('commenter_name')
    commenter_profile_link = kwargs.get('commenter_profile_link')
    photo_name = kwargs.get('photo_name')
    photo_url=kwargs.get('photo_url')
    from_email = 'Twiching.com <do_not_reply@twiching.com>'

    if kwargs.get('email_template'):
        emailTemplateFile = kwargs.get('email_template')
    else:
        emailTemplateFile = 'event_notification'

    txtEmailTemplate = 'emails/'+emailTemplateFile+'.txt'
    htmlEmailTemplate = 'emails/'+emailTemplateFile+'.html'

    textEmail = get_template('emails/' + emailTemplateFile + '.txt')
    htmlEmail = get_template('emails/' + emailTemplateFile + '.html')

    variables = {
            'subject': subject,
            'message': content,
            'unsubscribe_link': 'http://' + http_host + '/notification-status/',
            'http_host': http_host,
            'commenter_name': commenter_name,
            'commenter_profile_link': commenter_profile_link,
            'photo_name': photo_name,
            'photo_url': photo_url,
            'photo_owner_name': photo_owner_name,
        }
    variables = Context(variables)
    text_content = textEmail.render(variables)
    html_content = htmlEmail.render(variables)
    tmp = to_email
    try:
        tmp.append("")
        message = EmailMultiAlternatives(subject, text_content, from_email, to_email)
    except:
        to_email = photo_owner_name + "<" + to_email + ">"
        message = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
    message.attach_alternative(html_content, "text/html")
    try:
        message.send()
        return True
    except Exception:
        return False

def page_not_found(request):
    """
    Calling this function would simply raise 404 error
    """
    raise Http404

def show_featured(request):
    """
    Shows the featured page for product details.
    """
    features = StoreFeatures.objects.all()
    if features:
        return render_to_response('general/featured.html',{'features':features},context_instance=RequestContext(request))

    return render_to_response('404.html',context_instance=RequestContext(request))

def set_photos_mails(request):
    from twiching.apps.photography.models import Photos
    photos = Photos.objects.all()
    for photo in photos:
        if photo.approved:
            photo.approved_mail = True
            photo.save()
            #print 'approved',photo.id
        if photo.featured:
            photo.featured_mail = True
            photo.save()
            #print 'featured',photo.id

    return redirect('/')
