"""
Forms for all the items that falls into general category

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from apps.general.models import EmailNotification
