"""
Well any general models that will be accessed by other models are provided here

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

from django.db import models
from django.utils.translation import ugettext_lazy as _
from twiching.settings import MEDIA_ROOT, BANNER_IMAGE_UPLOAD_PATH
from twiching.library.imageManipulation import resize
from PIL import Image

class Banner(models.Model):
    PAGE_CHOICE = (
        ('1', 'Homepage'),
        ('2', 'Products'),
        ('3', 'A to Z Photography'),
	('4', 'Homepage Ad Banner'),
    )

    name = models.CharField(_('Name'), max_length=250)
    link = models.URLField(_('Link'))
    area = models.CharField(_('Banner Area'), max_length=1, choices=PAGE_CHOICE, null=False, blank=True, default=1)
    new_window = models.BooleanField(_('Display in new Window'), default=False)
    banner = models.ImageField(upload_to=BANNER_IMAGE_UPLOAD_PATH+'/')

    created_on = models.DateTimeField(auto_now_add = True)
    updated_on = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        super(Banner, self).save()
        size = None
        if int(self.area) == 2:
            size = (700, 280)
        elif int(self.area) == 3:
            size = (641, 265)
	elif int(self.area) == 4:
            size = (255, 255)

        if int(self.area) != 1 and int(self.area) != 4:
            imageFilePath = self.banner.name
            filePath = MEDIA_ROOT+ str(imageFilePath)
            img = Image.open(filePath)
            output = MEDIA_ROOT+ str(imageFilePath)
            resize(img, size, True, output, True)

class EmailNotification(models.Model):
    """
    This model would store all the emails that have un-subscribed from the site notification
    """
    email = models.EmailField(_('Email'))
    unsubscribed_on = models.DateTimeField(auto_now_add = True)




class StoreFeatures(models.Model):

    CONTENT_CHOICE = (
        ('secured', 'Secured'),
        ('free_shipping', 'Free Shipping'),
        ('pod', 'Pay On Delivery'),
        ('warranty', '1 Year Warranty'),
    )
    title = models.CharField(_('Object Name'),choices=CONTENT_CHOICE,max_length=250)
    content_title = models.CharField(_('Content Title'), max_length=250)
    content = models.TextField()

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Store Features')
        verbose_name_plural = _('Store Features')


class StoreAdminEmails(models.Model):
    email = models.EmailField(_('Email'),max_length=250)
    
    def __unicode__(self):
        return self.email

    class Meta:
        verbose_name = _('Store Admin Email')
        verbose_name_plural = _('Store Admin Emails')



