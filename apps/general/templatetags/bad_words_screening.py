from twiching.config.profanities_list import PROFANITIES_LIST
from django.template import Library

register = Library()

@register.filter("replace_bad_words")
def replace_bad_words(value):
    """ Replaces profanities in strings with safe words
    For instance, "shit" becomes "s--t"
    """
    words_seen = [w for w in PROFANITIES_LIST if w in value]
    if words_seen:
        for word in words_seen:
            value = value.replace(word, "%s" % ('*'*(len(word))))
    return value