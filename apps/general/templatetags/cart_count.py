__author__ = 'swaroop'

from satchmo_store.shop.models import Cart
from math import ceil
from django.template import Library

register = Library()

@register.inclusion_tag('general/show_cart_count.html', takes_context=True)
def show_cart_count(context):
    request = context['request']
    cart = Cart.objects.from_request(request)
    cartCount = int(cart.numItems)
    return {'count': cartCount}