from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import Library
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import stringfilter
from twiching.apps.general.models import Banner
import os
import random

register = Library()

@register.inclusion_tag('general/homepage_ad.html', takes_context=True)
def get_home_ad_banner(context):
    banner = Banner.objects.filter(area = '4')
    id_list = []
    if banner:
        total = banner.values_list('id')
        for tot in total:
            id_list.append(int(tot[0]))
        random.shuffle(id_list)
	random.shuffle(id_list)
        banner = banner.get(pk=id_list[0])
        return { 'ad_banner':banner}

    return { 'ad_banner':None }
