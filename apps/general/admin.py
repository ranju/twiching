"""
Admin related code for general section

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""
__author__ = 'swaroop'

from django.contrib import admin
from twiching.apps.general.models import Banner, StoreFeatures, StoreAdminEmails

class BannerAdmin(admin.ModelAdmin):
    list_filter = ('area',)

class StoreFeaturesAdmin(admin.ModelAdmin):
   list_display = ('title', 'content_title', 'content',)
   list_editable = ('content_title','content',)

class StoreAdminEmailsAdmin(admin.ModelAdmin):
   list_display = ('id','email', )
   list_editable = ('email',)


admin.site.register(Banner, BannerAdmin)
admin.site.register(StoreFeatures, StoreFeaturesAdmin)
admin.site.register(StoreAdminEmails, StoreAdminEmailsAdmin)

