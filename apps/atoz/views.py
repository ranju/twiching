"""
Actions for the Learning Center (formerly known as AtoZ) Application.

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.db.models import Q
from django.http import Http404
from endless_pagination.decorators import page_template
from twiching.settings import DATE_FORMAT, LEARNING_CENTER_ID
from twiching.apps.atoz.models import Post, Category #, ourAssistance
from twiching.apps.general.models import Banner

@page_template("atoz/learning_center_pagination.html")
def index(request, category = None,template=None , extra_context=None):
    """
    This action would display summary of all the post. this is the page that is loaded as the main page for
    learning center
    """
    if category is None:
        raise Http404

    #check if the category exist
    try:
        category = Category.objects.get(pk = category)
    except Category.DoesNotExist:
        raise Http404
    else:
        cartegoryName = category.name

        if category.id == 1 or category.id == 2:
            featuredPosts = Post.objects.filter(featured = True, category = category, publish = True).order_by('-created_on')[:4]
            latestPosts = Post.objects.filter(publish = True, category = category).order_by('-created_on')
            bannerList = list()
        else:
            featuredPosts = Post.objects.filter(featured = True, category = category, publish = True).order_by('-created_on')[:4]
            latestPosts = Post.objects.filter(publish = True, category = category).order_by('-created_on')
            bannerList = Banner.objects.all().filter(area = 3)
        breadcrumb = [
                {'name': 'Home', 'url': '/'},
                {'name': 'Learning Center', 'url': '#'},
                {'name': cartegoryName, 'url': '/learningcenter/'+str(category)}
        ]

        templateVariables = {
            'cartegoryName': cartegoryName,
            'featuredPosts': featuredPosts,
            'latestPosts': latestPosts,
            'DATE_FORMAT': DATE_FORMAT,
            'bannerList': bannerList,
            'breadcrumb': breadcrumb,
            'category_id': int(category.id),
            'LEARNING_CENTER_ID': LEARNING_CENTER_ID,
        }

	if template is None:
            if category.id == 1 or category.id == 2:
	        template = 'atoz/home_without_banner.html'
                #return render_to_response('atoz/home_without_banner.html', templateVariables, context_instance=RequestContext(request))
            else:
	        template = 'atoz/home_with_banner.html'
                #return render_to_response('atoz/home_with_banner.html', templateVariables, context_instance=RequestContext(request))

	if extra_context is not None:
            templateVariables.update(extra_context)
        return render_to_response(template, templateVariables, context_instance=RequestContext(request))


def page(request, category = None, id = None):
    """
    This action would display the complete post
    """
    if category is None:
        raise Http404

    if id is None:
        raise Http404

    try:
        if request.user.is_superuser:
            postObject = Post.objects.get(pk = int(id))
        else:
            postObject = Post.objects.get(pk = int(id), publish = True)
    except Post.DoesNotExist:
        raise Http404

    latestPosts = Post.objects.filter(~Q(pk = int(id)), publish = True, category = postObject.category).order_by('-created_on')[:10]

    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Learning Center', 'url': '#'},
            {'name': str(category).title(), 'url': '/learningcenter/'+str(postObject.category.id)},
            {'name': str(postObject.title).title(), 'url': '/learningcenter/'+str(postObject.category.name).lower()+'/'+str(postObject.id)}
    ]

    templateVariables = {
        'DATE_FORMAT': DATE_FORMAT,
        'breadcrumb': breadcrumb,
        'post': postObject,
        'latestPosts': latestPosts,
        'meta_tag': postObject.meta_tags,
        'meta_description': postObject.meta_content,
        'category_id': int(postObject.category.id),
        'LEARNING_CENTER_ID': LEARNING_CENTER_ID,
    }
    return render_to_response('atoz/page.html', templateVariables, context_instance=RequestContext(request))
