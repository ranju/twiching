"""
Search index implementation for learning center

@author: Aswathy Rajesh
@organization: Opentechnics
@contact: aswathi.r@opentechnics.com
@copyright: (c) 2012, Twiching
"""
import datetime
from haystack import indexes
#from haystack import site
from twiching.apps.atoz.models import Post


class PostIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    summary = indexes.CharField(model_attr='summary')
    content = indexes.CharField(model_attr='content')
    createDate = indexes.DateTimeField(model_attr='created_on')
    categId = indexes.IntegerField(model_attr='category_id')

    def get_model(self):
        return Post

    def index_queryset(self):
        """Used when the entire index for model is updated."""
        return Post.objects.filter(created_on__lte=datetime.datetime.now())


#site.register(Post, PostIndex)
