"""
Admin related code for flatpage application goes here

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

from django.contrib import admin
from twiching.apps.atoz.models import Post, Category

class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'featured')
    search_fields = ('title',)
    list_filter = ('category','featured')
    fieldsets = (
        ('Post', {
            'fields': ('title', 'summary', 'content')
        }),
        ('Post Settings', {
            'fields': ('category', 'image', 'featured', 'publish')
        }),
        ('SEO', {
            'classes': ('collapse',),
            'fields': ('meta_tags','meta_content')
        }),
        )

admin.site.register(Category)
admin.site.register(Post, PostAdmin)
