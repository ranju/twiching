from django.db import models
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField

def get_image_path(instance, filename):
    """
    This will rename the uploaded file based on a random generated string based on the uploaded time and filename etc
    """
    import time
    import os
    import hashlib
    from twiching.settings import AtoZ_IMAGE_UPLOAD_PATH
    randomString = hashlib.md5(filename).hexdigest()+str(int(time.time()))
    filename =  str(randomString) + filename
    return os.path.join(AtoZ_IMAGE_UPLOAD_PATH,  filename)

class Category(models.Model):
    name = models.CharField(_('Name'), max_length=250)
    description = models.TextField(_('Description'), null = True, blank = True)
    created_on = models.DateTimeField(auto_now_add = True)
    updated_on = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        return self.name

class Post(models.Model):
    """
    These are the fields that will be used to distinguish a normal flatpage to aToZ Photography
    """
    title = models.CharField(_('Title'), max_length=250)
    summary = models.TextField(_('Summary'), null = False, blank = False)
    content = RichTextField(null = False, blank = False)
    meta_tags = models.CharField(_('Meta Tags'), max_length=250, null = True, blank = True)
    meta_content = models.TextField(_('Meta Description'), null = True, blank = True)
    category = models.ForeignKey(Category, null = False, blank = False)
    featured = models.BooleanField(_('Featured'), default=0)
    image = models.ImageField(_('Image'), upload_to=get_image_path, null = True, blank = True)
    publish = models.BooleanField(_('Publish'), default=1)
    created_on = models.DateTimeField(auto_now_add = True, null=True)
    updated_on = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return self.title

