"""
Template tags for various flatpage related blocks

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""

#__author__ = 'swaroop'
#
from django.template import Library
from twiching.apps.atoz.models import Post
from twiching.settings import LEARNING_CENTER_ID, STATIC_URL

register = Library()

@register.inclusion_tag('atoz/custom_tag_latest_articles.html', takes_context=True)
def latest_a_to_z_articles(context):
    latestPosts = Post.objects.filter(publish = True).order_by('-created_on')[:5]
    return { 'latestPosts':latestPosts, 'LEARNING_CENTER_ID': LEARNING_CENTER_ID, 'STATIC_URL': STATIC_URL }

#class FlatPageBreadCrumbNode(Node):
#    def __init__(self, flatpage_id):
#        self.flatpage_id = flatpage_id
#    def render(self, context):
#        #check if the flatpage is part of a to z or not
#        try:
#            atozObject = Contents.objects.get(flatpage = FlatPage(pk = int(self.flatpage_id)))
#        except Contents.DoesNotExist:
#            pass
#        return ''
#
#def flatpage_bread_crumb(paser, token):
#    bits = token.contents.split()
#    if len(bits) != 2:
#        raise TemplateSyntaxError, _("flatpage_bread_crumb tag takes exactly one argument")
#    return FlatPageBreadCrumbNode(bits[1])
#
#flatpage_bread_crumbs = register.tag(flatpage_bread_crumb)


