"""
Implementation of satchmo wishlist

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

from satchmo_store.contact.models import Contact
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, redirect
from django.http import Http404, HttpResponseRedirect
from satchmo_store.shop.views.cart import product_from_post
from product.models import Product
from satchmo_ext.wishlist.models import ProductWish
from django.core import urlresolvers
from django.utils.datastructures import MultiValueDictKeyError
from django.template import RequestContext
from twiching.settings import L10N_SETTINGS, DATE_FORMAT
from satchmo_store.shop.signals import satchmo_cart_changed
from satchmo_store.shop.models import Cart
from satchmo_ext.wishlist.views import _wish_from_post, _wishlist_remove
from satchmo_store.shop.exceptions import CartAddProhibited
from django.contrib import messages

import logging

log = logging.getLogger('wishlist.view')

@login_required(login_url='/accounts/login/')
def wishlist_view(request, message=""):
    try:
        contact = Contact.objects.from_request(request)
    except Contact.DoesNotExist:
        messages.error(request, _('Sorry, there is a mismatch in your profile details. Please contact customer support with details of what you where trying to do!'))
        return redirect('/accounts/dashboard/')

    wishes = ProductWish.objects.filter(contact=contact)

    currency = L10N_SETTINGS['currency_formats'][L10N_SETTINGS['default_currency']]['symbol']

    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Account Dashboard', 'url': '/accounts/dashboard'},
            {'name': 'Wishlist', 'url': '/accounts/edit'}
    ]

    templateVariables = {
        'wishlist' : wishes,
        'wishlist_message' : message,
        'breadcrumb': breadcrumb,
        'currency': currency,
        'DATE_FORMAT': DATE_FORMAT,
    }

    return render_to_response('wishlist/index.html',templateVariables, context_instance=RequestContext(request))

@login_required(login_url='/accounts/login/')
def wishlist_add(request):
    """Add an item to the wishlist."""
    try:
        contact = Contact.objects.from_request(request)
    except Contact.DoesNotExist:
        #we need to add an entry into contact and shipping
        pass


    log.debug('FORM: %s', request.POST)
    formdata = request.POST.copy()
    productslug = None
    if formdata.has_key('productname'):
        productslug = formdata['productname']
    try:
        product, details = product_from_post(productslug, formdata)

    except (Product.DoesNotExist, MultiValueDictKeyError):
        log.debug("Could not find product: %s", productslug)
        raise Http404

    wish = ProductWish.objects.create_if_new(product, contact, details)
    url = urlresolvers.reverse('satchmo_wishlist_view')
    return HttpResponseRedirect(url)

def move_to_cart(request):
    wish, msg = _wish_from_post(request)
    if wish:
        cart = Cart.objects.from_request(request, create=True)
        try:
            cart.add_item(wish.product, number_added=1, details=wish.details)
        except CartAddProhibited, cap:
            msg = _("Wishlist product '%(product)s' could't be added to the cart. %(details)s") % {
                'product' : wish.product.translated_name(),
                'details' : cap.message
            }
            return wishlist_view(request, message=msg)

        #url = urlresolvers.reverse('satchmo_cart')
        satchmo_cart_changed.send(cart, cart=cart, request=request)
        return HttpResponseRedirect('/cart/')
    else:
        return wishlist_view(request)

@login_required(login_url='/accounts/login/')
def wishlist_remove(request):
    contact = Contact.objects.from_request(request)

    success, msg = _wishlist_remove(request)
    if success:
        messages.success(request, _("Product successfully removed from your wishlist"))
    else:
        messages.error(request, _("Failed to remove the product from your wishlist"))
    return redirect('/wishlist/')