"""
Implementation of satchmo store

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
from django.core.xheaders import populate_xheaders
from product.views import display_featured
from product.queries import bestsellers
from twiching.settings import L10N_SETTINGS
from product.models import ProductImage, Product, ProductAttribute
from twiching.apps.general.models import Banner
from django.http import Http404, HttpResponse
from decimal import Decimal
from l10n.utils import moneyfmt
from product.utils import find_best_auto_discount
from product.views import find_product_template
from satchmo_store.shop.models import Cart
from satchmo_store.shop.signals import satchmo_cart_view, satchmo_cart_details_query, satchmo_cart_add_complete, satchmo_cart_changed
from livesettings import config_value
from satchmo_store.shop.views.cart import product_from_post, decimal_too_big, _product_error, _json_response, _set_quantity
from satchmo_utils.views import bad_or_missing
from django.utils.datastructures import MultiValueDictKeyError
from satchmo_utils.numbers import RoundedDecimalError, round_decimal
from satchmo_store.shop.exceptions import CartAddProhibited
from django.http import HttpResponseRedirect

import logging

log = logging.getLogger('shop.views.cart')

def productList(request):
    # Display the category, its child categories, and its products.

    featured = display_featured(500)
    products = list()
    for product in featured:
        attributes = ProductAttribute.objects.filter(product = product)[:3]
        newProduct = {
            'id': product.id,
            'main_image': product.main_image,
            'name': product.name,
            'slug': product.slug,
            'attributes': attributes,
            'stock': int(product.items_in_stock),
        }
        products.append(newProduct)

    best_sellers = bestsellers(4)
    bestProducts = list()
    for product in best_sellers:
        newProduct = {
            'id': product.id,
            'main_image': product.main_image,
            'name': product.name,
            'slug': product.slug,
            }
        bestProducts.append(newProduct)

    best_sellers_count = len(bestProducts)

    bannerList = Banner.objects.all().filter(area = 2)

    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Store', 'url': '/products'},
    ]

    templateVariables = {
        'best_sellers': bestProducts,
        'products': products,
        'best_sellers_count': best_sellers_count,
        'bannerList': bannerList,
        'breadcrumb': breadcrumb,
    }

    return render_to_response('shop/product_list.html', templateVariables, context_instance=RequestContext(request))

def productDetail(request, product_id = None):
    if product_id is None:
        raise Http404

    try:
        product = Product.objects.get(pk = product_id)
    except Product.DoesNotExist:
        raise Http404

    category_list = product.category.all()

    #lets get the price
    quantity = Decimal('1')
    price = moneyfmt(product.get_qty_price(quantity))

    best_discount = find_best_auto_discount(product)

    #lets get the custom attributes for this product
    attributes = ProductAttribute.objects.filter(product = product)

    productImages = product.productimage_set.values()

    #related Products
    related_products = product.related_items.all()


    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Shopping', 'url': '/products'},
            {'name': product.name, 'url': '/products/'+product_id},

    ]

    currency = L10N_SETTINGS['currency_formats'][L10N_SETTINGS['default_currency']]['symbol']

    templateVariables = {
        'product': product,
        'category': category_list,
        'price': price,
        'best_discount': best_discount,
        'attributes': attributes,
        'related_products': related_products,
        'breadcrumb': breadcrumb,
        'productImages': productImages,
        'currency': currency,
        'stock': int(product.items_in_stock),
    }

    return render_to_response('shop/product_details.html', templateVariables, context_instance=RequestContext(request))

def showCart(request, cart=None, error_message='', default_view_tax=None):
    """Display the items in the cart."""
    if default_view_tax is None:
        default_view_tax = config_value('TAX', 'DEFAULT_VIEW_TAX')

    if not cart:
        cart = Cart.objects.from_request(request)

    if cart.numItems > 0:
        products = [item.product for item in cart.cartitem_set.all()]
        sale = find_best_auto_discount(products)
    else:
        sale = None

    imageList = list()
    for c in cart:
        images = ProductImage.objects.filter(product = c.product).order_by('sort')[:1]
        for img in images:
            imageList.append({'cart_id': c.id, 'picture': img.picture})

    satchmo_cart_view.send(cart,
        cart=cart,
        request=request)

    context = RequestContext(request, {
        'cart': cart,
        'error_message': error_message,
        'default_view_tax' : default_view_tax,
        'sale' : sale,
        'imageList': imageList,
        })

    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Shopping', 'url': '/products'},
            {'name': 'My Cart', 'url': '/cart/'},

    ]

    return render_to_response('shop/cart.html', {'breadcrumb': breadcrumb}, context_instance=context)

def addToCart(request, id=0, redirect_to='satchmo_cart'):
    """Add an item to the cart."""
    log.debug('FORM: %s', request.POST)
    formdata = request.POST.copy()
    productslug = None

    cartplaces = config_value('SHOP', 'CART_PRECISION')
    roundfactor = config_value('SHOP', 'CART_ROUNDING')


    if formdata.has_key('productname'):
        productslug = formdata['productname']
    try:
        product, details = product_from_post(productslug, formdata)

        if not (product and product.active):
            log.debug("product %s is not active" % productslug)
            return bad_or_missing(request, _("That product is not available at the moment."))
        else:
            log.debug("product %s is active" % productslug)

    except (Product.DoesNotExist, MultiValueDictKeyError):
        log.debug("Could not find product: %s", productslug)
        return bad_or_missing(request, _('The product you have requested does not exist.'))

    # First we validate that the number isn't too big.
    if decimal_too_big(formdata['quantity']):
        return _product_error(request, product, _("Please enter a smaller number."))

    # Then we validate that we can round it appropriately.
    try:
        quantity = round_decimal(formdata['quantity'], places=cartplaces, roundfactor=roundfactor)
    except RoundedDecimalError:
        return _product_error(request, product,
            _("Invalid quantity."))

    if quantity <= Decimal('0'):
        return _product_error(request, product,
            _("Please enter a positive number."))

    cart = Cart.objects.from_request(request, create=True)
    # send a signal so that listeners can update product details before we add it to the cart.
    satchmo_cart_details_query.send(
        cart,
        product=product,
        quantity=quantity,
        details=details,
        request=request,
        form=formdata
    )
    try:
        added_item = cart.add_item(product, number_added=quantity, details=details)

    except CartAddProhibited, cap:
        return _product_error(request, product, cap.message)

    # got to here with no error, now send a signal so that listeners can also operate on this form.
    satchmo_cart_add_complete.send(cart, cart=cart, cartitem=added_item, product=product, request=request, form=formdata)
    satchmo_cart_changed.send(cart, cart=cart, request=request)

    if request.is_ajax():
        data = {
            'id': product.id,
            'name': product.translated_name(),
            'item_id': added_item.id,
            'item_qty': str(round_decimal(quantity, 2)),
            'item_price': unicode(moneyfmt(added_item.line_total)) or "0.00",
            'cart_count': str(round_decimal(cart.numItems, 2)),
            'cart_total': unicode(moneyfmt(cart.total)),
            # Legacy result, for now
            'results': _("Success"),
            }
        log.debug('CART AJAX: %s', data)

        return _json_response(data)
    else:

        return HttpResponseRedirect('/cart')

def set_quantity(request):
    """Set the quantity for a cart item.

    Intended to be called via the cart itself, returning to the cart after done.
    """
    cart_url = '/cart/'

    if not request.POST:
        return HttpResponseRedirect(cart_url)

    success, cart, cartitem, errors = _set_quantity(request)

    if request.is_ajax():
        if errors:
            return _json_response({'errors': errors, 'results': _("Error")}, True)
        else:
            return _json_response({
                'item_id': cartitem.id,
                'item_qty': str(cartitem.quantity) or "0",
                'item_price': unicode(moneyfmt(cartitem.line_total)) or "0.00",
                'cart_total': unicode(moneyfmt(cart.total)) or "0.00",
                'cart_count': str(cart.numItems) or '0',
                })

    else:
        if success:
            return HttpResponseRedirect(cart_url)
        else:
            return showCart(request, cart = cart, error_message = errors)

def remove(request):
    """Remove an item from the cart."""
    if not request.POST:
        # Should be a POST request
        return bad_or_missing(request, "Please use a POST request")

    success, cart, cartitem, errors = _set_quantity(request, force_delete=True)

    if request.is_ajax():
        if errors:
            return _json_response({'errors': errors, 'results': _("Error")}, True)
        else:
            return _json_response({
                'cart_total': unicode(moneyfmt(cart.total)),
                'cart_count': str(cart.numItems),
                'item_id': cartitem.id,
                'results': success, # Legacy
            })
    else:
        if errors:
            return showCart(request, cart=cart, error_message=errors)
        else:
            return HttpResponseRedirect('/cart/')