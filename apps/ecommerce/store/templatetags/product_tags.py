from django import template
from product.views import display_featured
from twiching.settings import MEDIA_URL
from product.queries import bestsellers
from twiching.settings import L10N_SETTINGS


register = template.Library()

@register.inclusion_tag('shop/custom_tag_display_featured.html', takes_context=True)
def featured_products(context):
    featured = display_featured(8)
    return { 'products':featured, 'MEDIA_URL': MEDIA_URL}

@register.inclusion_tag('shop/custom_tag_display_best_sellers_vertical.html', takes_context=True)
def featured_best_sellers_vertical(context):
    best_sellers = bestsellers(8)
    currency = L10N_SETTINGS['currency_formats'][L10N_SETTINGS['default_currency']]['symbol']
    return { 'products':best_sellers, 'MEDIA_URL': MEDIA_URL, 'currency': currency,}

