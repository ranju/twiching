from product.models import Category, Product
from product.utils import find_best_auto_discount
from product.signals import index_prerender
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
from django.http import Http404
from decimal import Decimal
from l10n.utils import moneyfmt

def category_view(request, slug, parent_slugs=''):
    """Display the category, its child categories, and its products.

    Parameters:
     - slug: slug of category
     - parent_slugs: ignored
    """
    try:
        category =  Category.objects.get_by_site(slug=slug)
        products = list(category.active_products())
        sale = find_best_auto_discount(products)
    except Category.DoesNotExist:
        raise Http404
    newProductList = list()
    for product in products:
        #lets get the price
        quantity = Decimal('1')
        price = moneyfmt(product.get_qty_price(quantity))
        data = {
            'id': product.id,
            'slug': product.slug,
            'name': product.name,
            'price': price,
            'main_image': product.main_image,
        }
        newProductList.append(data)

    products = newProductList

    #child_categories = category.get_all_children()

    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': category.translated_name, 'url': '/profiles'},
    ]

    templateVariables = {
        'category': category,
        'products' : products,
        'sale' : sale,
        'breadcrumb': breadcrumb,
    }

    index_prerender.send(Product, request=request, context=templateVariables, category=category, object_list=products)
    return render_to_response('product/category.html', templateVariables, context_instance=RequestContext(request))