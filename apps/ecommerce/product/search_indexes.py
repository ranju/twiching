"""
Search index implementation for store

@author: Aswathy Rajesh, Swaroop Shankar V
@organization: Opentechnics
@contact: aswathi.r@opentechnics.com, swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""
import datetime
from haystack import indexes
#from haystack import site
from product.models import Product


class ProductIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='name')
    desc = indexes.CharField(model_attr='description')
    add_date = indexes.DateTimeField(model_attr='date_added')

    def get_model(self):
        return Product

    def index_queryset(self):
        return Product.objects.filter(date_added__lte=datetime.datetime.now())


#site.register(Product, ProductIndex)
