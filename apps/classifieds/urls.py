from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('twiching.apps.classifieds.views',
    url(r'^add/$','add_classified', name='add_classified'),
    url(r'^add/add_brand$','add_brand', name='add_brand'),
    url(r'^add/add_comment/(?P<id>\d+)$','add_comment', name='add_comment'),
    url(r'^$','list_items', name='list_items'),
    url(r'^(?P<id>\d+)/$','detail_classified', name='detail_classified'),
    url(r'^send-mail/$','send_mail_to', name='send_mail_to'),
    url(r'^search/$','search',name='search'),
    url(r'^delete_ad/$','delete_ad', name='delete_ad'),
    )

