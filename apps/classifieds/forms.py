from django import forms
from django.forms import ModelForm
from twiching.apps.classifieds.models import *
from django.forms import widgets
from django.forms.extras.widgets import SelectDateWidget
from django.forms.formsets import formset_factory
from django.contrib.auth.models import User
#from captcha.fields import ReCaptchaField
#from locpick.widget import LocationPickerWidget

class AdForm(forms.ModelForm):
    class Meta:
        model = Ad
        fields = ('title','category','brand','location','landmark','price','condition','email', 'phone','description','tag','latitude','longitude','usage_years','usage_months','usage_days','features','color','material','extra_feature','custom_category','custom_brand')

        widgets = {
            'condition': forms.RadioSelect,
            #'location': LocationPickerWidget
        }
    #captcha = ReCaptchaField(attrs={'theme' : 'red'})
#    dob = forms.DateField( widget=SelectDateWidget(years=range(1980, 2012)))

    def __init__(self,*args,**kwargs):
        super(AdForm, self).__init__(*args, **kwargs)
#        self.fields['dob'].widget.attrs['style'] = "width:60px;"
	self.fields['usage_years'].widget.attrs['style'] = "width:45px;"
        self.fields['usage_months'].widget.attrs['style'] = "width:45px;"
        self.fields['usage_days'].widget.attrs['style'] = "width:45px;"
	#self.fields['category'].widget.attrs['onchange'] = "change_brand()"


    def save(self,**kwargs):
        user = kwargs.pop('user')
        print "user is",user
        ad = super(AdForm, self).save(commit=False)
        ad.user = user
#        user = User.objects.get(username='akh')
#        ad.user=user
        ad.save()
        if ad.custom_category:
            category = Category()
            category.name = ad.custom_category
            category.save()
            ad.category = category
        if ad.custom_brand:
            brand = Brand()
            brand.name = ad.custom_brand
            brand.save()
            ad.brand = brand
            cat = ad.category
            cat.brands.add(brand)
        if ad.custom_category and ad.brand:
            cat = ad.category
            cat.brands.add(ad.brand)
        return ad

    def clean(self):
        cleaned_data = self.cleaned_data
        condition = cleaned_data.get("condition")
        usage_years = cleaned_data.get("usage_years")
        usage_months = cleaned_data.get("usage_months")
        usage_days = cleaned_data.get("usage_days")
        category = cleaned_data.get("category")
        custom_category = cleaned_data.get("custom_category")
        brand = cleaned_data.get("brand")
        custom_brand = cleaned_data.get("custom_brand")
        if condition == False :
            if not usage_years and not usage_months and not usage_days:
                msg = "Please enter years,months and days of usage in digits"
                self._errors["usage_years"] = self.error_class([msg])
                self._errors["usage_months"] = self.error_class([msg])
                self._errors["usage_days"] = self.error_class([msg])
                if usage_years:
                    del cleaned_data["usage_years"]
                if usage_months:
                    del cleaned_data["usage_months"]
                if usage_days:
                    del cleaned_data["usage_days"]

        if not category and not custom_category:
           msg1 = "Choose a category or add a custom category"
           self._errors["category"] = self.error_class([msg1])
           if category:
                    del cleaned_data["category"]

        if category and custom_category:
            msg2 = "either choose  a category or add a custom category,not both"
            self._errors["category"] = self.error_class([msg2])
            if category:
                    del cleaned_data["category"]

        if not brand and not custom_brand:
           msg3 = "Choose a brand or add a custom brand"
           self._errors["brand"] = self.error_class([msg3])
           if category:
                    del cleaned_data["brand"]

        if brand and custom_brand:
            msg4 = "either choose  a brand or add a custom brand,not both"
            self._errors["brand"] = self.error_class([msg4])
            if category:
                    del cleaned_data["brand"]
        return self.cleaned_data

    def clean_category(self):
        category = self.cleaned_data.get("category")
        custom_category = self.cleaned_data.get("custom_category")
        if category and custom_category:
            raise forms.ValidationError("Choose either a category or custom category.not both")
        return category
    
    def clean_phone(self):
        mobile = self.cleaned_data['phone']
        mobile = str(mobile)
        length=len(mobile)
        if length < 9:
            raise forms.ValidationError("Enter atleast 9 digits for phone number")
        else:
            return mobile

class AdImagesForm(forms.ModelForm):
    class Meta:
        model = AdImages
        fields = ('image',)
        
class RepliesForm(forms.ModelForm):
    class Meta:
        model = Replies
        fields = ('name','email','phone','comment')

    def clean_phone(self):
        mobile = self.cleaned_data['phone']
        try:
            mob = int(mobile)
            return mob
        except:
            raise forms.ValidationError("Please enter a valid phone number")

    def save(self,**kwargs):
        #ls = kwargs.pop('ls')
        ad = kwargs.pop('ad')
        reply = super(RepliesForm, self).save(commit=False)
        reply.ad=ad
        reply.save()

class CommnetForm(forms.ModelForm):
    class Meta:


        model = Comments
        fields = ('name','text','email',)
    def save(self,**kwargs):
        #ls = kwargs.pop('ls')
        ad = kwargs.pop('ad')
        comments = super(CommnetForm, self).save(commit=False)
        comments.ad=ad
        comments.save()

    def __init__(self, *args, **kwargs):
        super(CommnetForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['style'] = 'font-size:16px'
        self.fields['text'].widget.attrs['style'] = 'font-size:16px'
	self.fields['email'].widget.attrs['style'] = 'font-size:16px'
        



    
