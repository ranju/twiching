from django.shortcuts import render, redirect,render_to_response
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse,Http404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.conf import settings
from django.contrib import messages
from django.core.urlresolvers import reverse
from twiching.apps.general.models import StoreAdminEmails
from twiching.apps.classifieds.forms import AdForm,AdImagesForm,RepliesForm,CommnetForm
from django.template import RequestContext,loader,Context
from twiching.apps.classifieds.models import AdImages,Ad,Category, Brand, Comments,Featured
from endless_pagination.decorators import page_template
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
import json
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.utils.html import strip_tags
from django.core.mail import EmailMultiAlternatives
from mailer.engine import send_all
from registration.models import RegistrationProfile
from twiching.apps.general.views import _sendNotification

#from django.core.paginator import EmptyPage
#from django.core.paginator import InvalidPage
#from django.core.paginator import Paginator
#from django.core.urlresolvers import reverse
#from django.db.models import Q
#from django.http import Http404
#from django.http import HttpResponseRedirect
#from django.shortcuts import get_object_or_404
#from django.shortcuts import render_to_response
#from django.template import RequestContext
#from django.utils.encoding import force_unicode
#from django.utils.html import strip_tags
#from django.views.generic.list_detail import object_detail
#from django.views.generic.list_detail import object_list

@login_required(login_url='/accounts/login/')
def add_classified(request):
    categories = Category.objects.all()
    form = AdForm()
    form1 = AdImagesForm()
    if request.method == 'POST':
        form = AdForm(data=request.POST)
        form1 = AdImagesForm(request.POST, request.FILES)
        
        if form.is_valid() and form1.is_valid():
            ls=request.FILES.getlist('image')
            current_user = request.user
            ad=form.save(user=current_user)
            ad.main_image=ls[0]
	    ls = ls[1:]
            ad.save()
            i=0
            images = []
            for img in ls:
                img_obj = AdImages()
                images.append(img_obj)
            for img in ls:
                images[i].ad=ad
                images[i].image=img
                images[i].save()
                i += 1
            
            #message = 'Succesfully added your product'
#            return render_to_response('classifieds/add_classifieds.html',{'form':form,'form1':form1,'message':message,'categories':categories,},context_instance=RequestContext(request),)
	    
            return redirect('/accounts/myads/')
    return render_to_response('classifieds/add_classifieds.html',{'form':form,'form1':form1,'categories':categories,},context_instance=RequestContext(request),)

@csrf_exempt 
@login_required(login_url='/accounts/login/')
def add_brand(request):
    if request.method == 'POST':
        
        cat_id = request.POST.get('cat')
        if cat_id:
            print "cat_id is",cat_id
            category=Category.objects.get(id=cat_id)
            brand = category.brands.all()
        else:
            brand = Brand.objects.all()
        brand_dict = {}
        for x in brand:
            brand_dict[x.id] = x.name
        return HttpResponse(json.dumps(brand_dict),mimetype='application/json')

@csrf_exempt
def delete_image(request):
    print "in delete!!!!!!!"
    if request.method == 'POST':
        print "in post"
        id = request.POST.get('id')
        ad_id = request.POST.get('ad_id')
        print "ad is",ad_id
        if id == "main_image":
            ad = Ad.objects.get(id = ad_id)
            ad_images = AdImages.objects.filter(ad=ad)
            if ad_images:
                ad.main_image = ad_images[0].image
                ad_images[0].delete()
                ad.save()
                return HttpResponse(json.dumps("success"),mimetype='application/json')
            else:
                return HttpResponse(json.dumps("failure"),mimetype='application/json')
        else:
            image_todelete = AdImages.objects.get(id=id)
            ad = image_todelete.ad
            images = AdImages.objects.filter(ad=ad).count()
            image_todelete.delete()
            return HttpResponse(json.dumps("success"),mimetype='application/json')


@page_template("classifieds/list_classifieds_page.html")
def list_items(request,template='classifieds/list_classifieds.html',extra_context=None):
    items = Ad.objects.all().filter(approve = True).order_by('-added_date')
    try:
        featuredobj = Featured.objects.get()
        featured_ad = featuredobj.featured
    except:
        featured_ad = None

    categories = Category.objects.all()
    return render_to_response(template,{'items':items,'categories':categories,'featured':featured_ad,},context_instance=RequestContext(request),)

def detail_classified(request,id):
    commnt_form = CommnetForm()
    try:
        ad = Ad.objects.get(id=id)
    except:
        return redirect('/classifieds/')
    comments = Comments.objects.filter(ad=ad).order_by("-id")
    if not ad.approve:
        return redirect('/classifieds/')
    curret_category = ad.category
    posted_user = ad.user
    similar_ads = Ad.objects.filter(category = curret_category).exclude(id=id).order_by('?')[:5]
    
    #list=[]
    categories = Category.objects.all()
    form = RepliesForm()
    if request.method == 'POST':
        
        form = RepliesForm(data=request.POST)
        if form.is_valid():
            form.save(ad=ad)
            message = 'Your reply added succesfully'
            
#            return redirect(reverse('detail_classified', args=(id,)))
            return render_to_response('classifieds/view_classifieds.html',{'commnt_form':commnt_form,'comments':comments,'form':form,'ad':ad,'categories':categories,'similar_ads':similar_ads,'posted_user':posted_user,'message':message,},context_instance=RequestContext(request),)

    return render_to_response('classifieds/view_classifieds.html',{'commnt_form':commnt_form, 'comments':comments,'form':form,'ad':ad,'categories':categories,'similar_ads':similar_ads,'posted_user':posted_user},context_instance=RequestContext(request),)
    
def add_comment(request, id):
    ad = Ad.objects.get(id=id)
    if request.method == 'POST':
        commnt_form = CommnetForm(data=request.POST)
        if commnt_form.is_valid():
            commnt_form.save(ad=ad)
	    photo_name = ad.title
            photo_url = 'http://' + request.META['HTTP_HOST'] + '/classifieds/' + str(ad.id)
            photo_comment = request.POST['text']
            commenter_name = request.POST['name']
            subject = commenter_name + ' commented on '+ photo_name
            photo_owner_name = ad.user.first_name+" "+ad.user.last_name
            
            admin_emails = StoreAdminEmails.objects.values_list('email')
            emails = []
            emails.append(ad.user.email)
            for mail in admin_emails:
                emails.append(mail[0])
            to_email = emails
            email_template = "user_comment_classifieds"
            http_host = request.META['HTTP_HOST']
            _sendNotification(
                subject=subject,
                content=photo_comment,
                to_email=to_email,
                http_host=http_host,
                commenter_name=commenter_name,
                photo_name=photo_name,
                photo_owner_name = photo_owner_name,
                photo_url=photo_url,
                email_template = email_template,
            )
            return redirect(reverse('detail_classified', args=(id,)))
    return redirect(reverse('detail_classified', args=(id,)))
    
    
def search(request):
    categories = Category.objects.all()
    q = request.GET.get('q')
    category = request.GET.get('category')
    results = []
    info_dict = {}
    if q:
        if category == 'All Categories':
            ads = Ad.objects.all()
        else:
            cate = Category.objects.get(name=category)
            ads = Ad.objects.filter(category=cate)

        ad = ads.filter(Q(title__icontains=q))

        results = []
        for a in ad:
            try:
                results.append((a.title, a.get_absolute_url(),a.main_image,'ad'))

            except:
                pass
    
    return render_to_response('classifieds/search_classifieds.html',{'results':results,'categories':categories},context_instance=RequestContext(request))
       
def send_mail_to(request):
    if request.method == 'POST':
        ad_mail = request.POST.get('ad_mail')
	ad_fname = request.POST.get('ad_fname')
        query = request.POST.get('query')
        visitor_mail = request.POST.get('visitor_mail')
        ad_title = request.POST.get('ad_title')
        current_site = Site.objects.get_current()
        subject = "Visitor query for your product %s on %s " %(ad_title,current_site.name)
        t = loader.get_template('classifieds/query_mail.html')
        c = Context({'site':current_site,'query':query,'visitor_mail':visitor_mail,'ad_fname':ad_fname})
        rendered = t.render(c)
        text_content = strip_tags(rendered)
        subject, from_email, to = subject,settings.DEFAULT_FROM_EMAIL,ad_mail
        text_content = text_content
        html_content = rendered
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        send_all()
    return HttpResponse("Your query has been successfully submitted.")
       
@login_required(login_url='/accounts/login/')
def accountsAds(request):
    userId = request.user.id

    try:
        regProfile = RegistrationProfile.objects.get(user=request.user)
    except RegistrationProfile.DoesNotExist:
        messages.error(request, _('Error occurred while processing your request!'))
        return redirect('/accounts/dashboard/')
    if regProfile.activation_key == 'ALREADY_ACTIVATED' and not request.user.is_active:
        messages.error(request, _('You need to active your account before using any personal features of this site'))
        return redirect('/accounts/activate/')

    myads = Ad.objects.filter(user = userId )
    print "my ads are",myads

    return render_to_response('classifieds/accounts_myads.html',{'myads':myads,},context_instance=RequestContext(request))

@csrf_exempt
def delete_ad(request):
    if request.method == 'POST':
        id = request.POST.get('id')
        ad = Ad.objects.get(id = id )
        ad.delete()
    return HttpResponse(json.dumps("success"),mimetype='application/json')

        
@login_required(login_url='/accounts/login/')
def edit_classified(request,id):
    try:
        ad = Ad.objects.get(id=id)
    except:
        return redirect('/accounts/myads/')
    
    categories = Category.objects.all()
    form = AdForm(instance = ad)
    form1 = AdImagesForm()
    if request.method == 'POST':
        form = AdForm(data=request.POST)
        main_image = request.FILES.get('main_image',None)
        if form.is_valid():
            print "valid ....."
            images = AdImages.objects.filter(ad=ad)
            if images:
                i = 0
                for image in images:
                    i = i + 1
                    img_id = 'img_%s'%i
                    val = 'id_%s'%i

                    new_image = request.FILES.get(val,None)
                    if new_image:
                        id_img = request.POST[img_id]
                        ad_image = images.get(id = id_img)
                        ad_image.image = new_image
                        ad_image.save()
            if main_image:
                ad.main_image = main_image
                ad.save()
            ls=request.FILES.getlist('image')
            i=0
            images = []
            for img in ls:
                img_obj = AdImages()
                images.append(img_obj)
            for img in ls:
                images[i].ad=ad
                images[i].image=img
                images[i].save()
                i += 1
            
            ad.title = request.POST.get('title')
            cat_id = request.POST.get('category')
            if cat_id:
                cat = Category.objects.get(id = cat_id)
                ad.category = cat
            custom_category = request.POST.get('custom_category')
            if custom_category:
                ad.custom_category = request.POST.get('custom_category')
                category = Category()
                category.name = custom_category
                category.save()
                ad.category = category

            brand_id=request.POST.get('brand',None)
            if brand_id:
                brand_id = int(brand_id)
                br = Brand.objects.get(id=brand_id)
                ad.brand = br
                if custom_category:
                    category.brands.add(br)
            custom_brand = request.POST.get('custom_brand')
            if custom_brand:
                ad.custom_brand = request.POST.get('custom_brand')
                brand = Brand()
                brand.name = custom_brand
                brand.save()
                ad.brand = brand

                if cat_id:
                    cat.brands.add(brand)
                else:
                    category.brands.add(brand)
            ad.price = request.POST.get('price')
            #ad.condition = request.POST.get('condition')
	    if request.POST.get('condition') == 'True':
                ad.usage_years = None
                ad.usage_months = None
                ad.usage_days = None
                ad.condition = 1
            else:
                ad.condition = 0

            ad.description = request.POST.get('description')
            ad.tag = request.POST.get('tag')
            if request.POST.get('usage_years'):
                ad.usage_years = request.POST.get('usage_years')
            if  request.POST.get('usage_months'):
                ad.usage_months = request.POST.get('usage_months')
            if request.POST.get('usage_days'):
                ad.usage_days = request.POST.get('usage_days')
            ad.location = request.POST.get('location')
            ad.landmark = request.POST.get('landmark')
            ad.features = request.POST.get('features')
            ad.color = request.POST.get('color')
            material = request.POST.get('material')
            ad.extra_feature = request.POST.get('extra_feature')
            ad.email = request.POST.get('email')
            ad.phone = request.POST.get('phone')
            ad.latitude = request.POST.get('latitude')
            ad.longitude = request.POST.get('longitude')
            ad.save();

            return redirect('/accounts/myads')
        
                    



    return render_to_response('classifieds/edit_classifieds.html',{'form':form,'form1':form1,'categories':categories,'ad':ad,},context_instance=RequestContext(request),)
