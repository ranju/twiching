from django.contrib import admin
from twiching.apps.classifieds.models import *

class CategoryAdmin(admin.ModelAdmin):
    filter_horizontal=['brands']
admin.site.register(Category,CategoryAdmin)

admin.site.register(Brand)

class AdImagesAdmin(admin.TabularInline):
    model = AdImages

class RepliesAdmin(admin.TabularInline):
    model = Replies

class AdAdmin(admin.ModelAdmin):
    model = Ad
    exclude = ['custom_category','custom_brand']
    list_display = ['title','user','approve','approve_mail']
    list_editable = ['approve','approve_mail']
    list_filter = ['user','approve']
    inlines = [AdImagesAdmin,RepliesAdmin]

admin.site.register(Ad,AdAdmin)

class CommentsAdmin(admin.ModelAdmin):
   list_display = ('ad', 'name', 'text', 'is_approved')
   list_editable = ('is_approved',)
admin.site.register(Comments,CommentsAdmin)
admin.site.register(Featured)
