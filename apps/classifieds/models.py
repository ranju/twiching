from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.sites.models import Site

site = Site.objects.get(pk=1)

class Category(models.Model):
    name = models.CharField(max_length=200)
    sort_order = models.PositiveIntegerField(default=0)
    brands = models.ManyToManyField('Brand',null=True,)

    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

class Brand(models.Model):
    #category = models.ForeignKey(Category)
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = "Brand"
        verbose_name_plural = "Brands"

class Ad(models.Model):

    status_choices=(
    ('1','for sale'),
    ('2','sold'),
    ('3','cancelled'),
    )
    
    year_choices=(
    (1,'1'),
    (2,'2'),
    (3,'3'),
    (4,'4'),
    (5,'5'),
    (6,'6'),
    (7,'7'),
    (8,'8'),
    (9,'9'),
    (10,'10'),
    (11,'11'),
    (12,'12'),
    (13,'13'),
    (14,'14'),
    (15,'15'),
    (16,'16'),
    (17,'17'),
    (18,'18'),
    (19,'19'),
    (20,'20'),
    (21,'21'),
    (22,'22'),
    (23,'23'),
    (24,'24'),
    (25,'25'),
    (26,'26'),
    (27,'27'),
    (28,'28'),
    (29,'29'),
    (30,'30'),
    (31,'31'),
    (32,'32'),
    (33,'33'),
    (34,'34'),
    (35,'35'),
    (36,'36'),
    (37,'37'),
    (38,'38'),
    (39,'39'),
    (40,'40'),
    (41,'41'),
    (42,'42'),
    (43,'43'),
    (44,'44'),
    (45,'45'),
    (46,'46'),
    (47,'47'),
    (48,'48'),
    (49,'49'),
    (50,'50'),
    (51,'51'),
    (52,'52'),
    (53,'53'),
    (54,'54'),
    (55,'55'),
    (56,'56'),
    (57,'57'),
    (58,'58'),
    (59,'59'),
    (60,'60'),
    (61,'61'),
    (62,'62'),
    (63,'63'),
    (64,'64'),
    (65,'65'),
    (66,'66'),
    (67,'67'),
    (68,'68'),
    (69,'69'),
    (70,'70'),
    (71,'71'),
    (72,'72'),
    (73,'73'),
    (74,'74'),
    (75,'75'),
    (76,'76'),
    (77,'77'),
    (78,'78'),
    (79,'79'),
    (80,'80'),
    (81,'81'),
    (82,'82'),
    (83,'83'),
    (84,'84'),
    (85,'85'),
    (86,'86'),
    (87,'87'),
    (88,'88'),
    (89,'89'),
    (90,'90'),
    (91,'91'),
    (92,'92'),
    (93,'93'),
    (94,'94'),
    (95,'95'),
    (96,'96'),
    (97,'97'),
    (98,'98'),
    (99,'99'),
    )

    month_choices=(
    (1,'1'),
    (2,'2'),
    (3,'3'),
    (4,'4'),
    (5,'5'),
    (6,'6'),
    (7,'7'),
    (8,'8'),
    (9,'9'),
    (10,'10'),
    (11,'11'),
    (12,'12'),
    )

    day_choices=(
    (1,'1'),
    (2,'2'),
    (3,'3'),
    (4,'4'),
    (5,'5'),
    (6,'6'),
    (7,'7'),
    (8,'8'),
    (9,'9'),
    (10,'10'),
    (11,'11'),
    (12,'12'),
    (13,'13'),
    (14,'14'),
    (15,'15'),
    (16,'16'),
    (17,'17'),
    (18,'18'),
    (19,'19'),
    (20,'20'),
    (21,'21'),
    (22,'22'),
    (23,'23'),
    (24,'24'),
    (25,'25'),
    (26,'26'),
    (27,'27'),
    (28,'28'),
    (29,'29'),
    (30,'30'),
    (31,'31'),
    
    )



    condition_choices = (
    (True, "New"),
    (False, "Used"),
)

    brand = models.ForeignKey(Brand,null = True, blank = True)
    category = models.ForeignKey(Category,null = True, blank = True)
    custom_category = models.CharField(max_length=200,blank=True)
    custom_brand = models.CharField(max_length=200,blank=True)
    user = models.ForeignKey(User)
    status = models.CharField(max_length=1,default=1,choices=status_choices)
    added_date = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    condition = models.BooleanField(choices=condition_choices,max_length=1,default=True)
#    dob = models.DateField()
    description = models.TextField()
    tag = models.CharField(max_length=200,blank=True)
   # usage = models.CharField(max_length=500,) #not using now
    usage_years = models.IntegerField(choices=year_choices,null = True,blank=True)
    usage_months = models.IntegerField(choices=month_choices,null = True,blank=True)
    usage_days = models.IntegerField(choices=day_choices,null = True,blank=True)

    location = models.CharField(max_length=500)
    landmark= models.CharField(max_length=500)
    features = models.CharField(max_length=500,blank=True)
    color = models.CharField(max_length=200,blank=True)
    material = models.CharField(max_length=200,blank=True)
    extra_feature = models.CharField(max_length=500,blank=True)
    main_image = models.ImageField(verbose_name='Image',upload_to=
                'images/ad/main/%Y/%m/%d/',max_length=200,blank=True)
    email = models.EmailField()
    phone = models.CharField(max_length = 25)
    latitude = models.FloatField(null=True,)
    longitude = models.FloatField(null=True,)
    approve = models.BooleanField(default=False)
    approve_mail = models.BooleanField('Approved Mail Sent',default=False)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/classifieds/%s/" % (self.id)

    def save(self, *args, **kwargs):
        from twiching.apps.general.views import _sendNotification
        try:
            super(Ad, self).save()
            if self.approve and not self.approve_mail:
                photo_name = self.title
                photo_url = 'http://' + site.domain + '/classifieds/' + str(self.id)
                photo_owner_name = self.user.first_name+" "+self.user.last_name
                subject = ' Admin approved your Ad in Twiching.com Classifieds.'
                email_template = "admin_classifieds_approve"
                to_email = self.user.email
                http_host = site.domain
                self.featured_mail = True
                _sendNotification(
                    subject=subject,
                    to_email=to_email,
                    http_host=http_host,
                    photo_name=photo_name,
                    photo_url=photo_url,
                    photo_owner_name=photo_owner_name,
                    email_template=email_template
                )
                self.approve_mail = True
                super(Ad, self).save()
        except:
            pass

class AdImages(models.Model):
    ad = models.ForeignKey(Ad,related_name='ad_images')
    title = models.CharField('Title',max_length=252)
    image = models.ImageField(verbose_name='Image',upload_to=
                'images/ad/gal/%Y/%m/%d/',max_length=200)
    order = models.IntegerField("Sort order",default=0)

    def __unicode__(self):
        return self.title
    class Meta:
        verbose_name = 'Ad Image'
        verbose_name_plural = 'Ad Images'
        ordering = ['order']

class Replies(models.Model):
    ad = models.ForeignKey(Ad,related_name='ad_replies')
    name = models.CharField(max_length=200)
    email = models.EmailField()
    phone = models.CharField(max_length=20)
    comment = models.TextField()

    class Meta:
        verbose_name = "Reply"
        verbose_name_plural="Replies"

class Comments(models.Model):
    ad = models.ForeignKey(Ad,related_name='ad_comment')
    name = models.CharField(max_length=50)
    text = models.TextField()
    is_approved = models.BooleanField("Approve", default=False)
    email = models.EmailField("Email")

    class Meta:
        verbose_name = "Comment"
        verbose_name_plural="Comments"

class Featured(models.Model):
    site=models.OneToOneField(Site)
    featured = models.ForeignKey(Ad)

    def __unicode__(self):
        return self.featured.title

    class Meta:
        verbose_name = "Featured Classified"
        verbose_name_plural = "Featured Classified"
