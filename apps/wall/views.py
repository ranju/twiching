"""
Functionality related to wall

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""
import os
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.contrib import messages
from django.template import RequestContext
from django.contrib.auth.models import User
from django.db.models import Q
from twiching.apps.wall.models import NewsFeedComments, NewsFeedStory, NewsFeedLikes
from twiching.apps.comment.models import Comments
from twiching.apps.photography.models import Photos, PhotoCategories
from twiching.apps.accounts.models import UserProfile, UserFollow
from django.contrib.auth.decorators import login_required
from twiching.settings import ENDLESS_PAGINATION_PER_PAGE, PROFILE_IMAGE_UPLOAD_PATH, DATE_FORMAT, PHOTOGRAPH_UPLOAD_PATH, MEDIA_ROOT

@login_required(login_url='/accounts/login/')
def Index(request):
    """
    This functionality would get all the latest wall posts and display in on the wall page
    """
    storyLimit = ENDLESS_PAGINATION_PER_PAGE

    #get total records to decide if we require to show the load more option or not
    followingObject = UserFollow.objects.filter(follower = request.user).only('following')
    followingList = list()
    for data in followingObject:
        followingList.append(data.following)
        #let us get all the latest feeds based on the story-limit and story-offset
    recordCount =  NewsFeedStory.objects.filter(user__in = followingList).count()
    if recordCount > storyLimit:
        showLoadDiv = True
    else:
        showLoadDiv = False

    data = _getNewFeeds(request.user, storyLimit)

    #Additional Data
    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Wall', 'url': '/wall/'},
    ]

    templateVariables = {
        'newFeedData': data,
        'breadcrumb': breadcrumb,
        'PROFILE_IMAGE_UPLOAD_PATH': PROFILE_IMAGE_UPLOAD_PATH,
        'PHOTOGRAPH_UPLOAD_PATH' : PHOTOGRAPH_UPLOAD_PATH,
        'DATE_FORMAT': DATE_FORMAT,
        'showLoadDiv': showLoadDiv,
        'currentPage': 1,
        }

    return render_to_response('wall/index.html', templateVariables, context_instance=RequestContext(request))

def wallAjax(request, mode = None):
    from django.utils import simplejson
    if mode is None:
        return render_to_response('common/ajax.html', {'output': 'no_mode_error'})
    elif mode == 'load':
        #this mode will load the next set of wall content based on the post value page
        page = request.POST.get('pages', 1)
        if page == 1:
            return render_to_response('common/ajax.html', {'output': 'first_page_error'})
        storyLimit = int(ENDLESS_PAGINATION_PER_PAGE)*int(page)
        storyOffset = ((int(page) - 1)*ENDLESS_PAGINATION_PER_PAGE)
        data = _getNewFeeds(request.user, storyLimit, storyOffset)

        templateVariables = {
            'PROFILE_IMAGE_UPLOAD_PATH': PROFILE_IMAGE_UPLOAD_PATH,
            'newFeedData': data,
            'DATE_FORMAT': DATE_FORMAT,
            'PHOTOGRAPH_UPLOAD_PATH' : PHOTOGRAPH_UPLOAD_PATH,
            }
        wallHtml = render_to_response('wall/wall_ajax.html', templateVariables, context_instance=RequestContext(request))
        recordCount = NewsFeedStory.objects.count()
        if recordCount > storyLimit:
            showLoadDiv = True
        else:
            showLoadDiv = False
        templateVariables = {
            'wallHtml': str(wallHtml),
            'page': int(page)+1,
            'showLoadDiv': showLoadDiv,
            }
        return HttpResponse(simplejson.dumps(templateVariables), mimetype="application/json")
    elif mode == 'comment':
        #this mode will enable the user to comment on posts
        comment = request.POST.get('comment', '')
        storyId = request.POST.get('storyId', 0)
        comment = comment.strip()
        if comment == '':
            return HttpResponse('no_comment_found_error')

        if not storyId:
            return HttpResponse('invalid_story_error')

        #let us check if the current user is logged in
        if not request.user.is_authenticated():
            messages.error(request, _('You need to login to comment on wall posts!'))
            return HttpResponse('user_not_logged_in_error')

        commentObject = Comments()
        commentObject.user = User(pk = request.user.id)
        commentObject.comment = comment
        commentObject.save()

        newsCommentObject = NewsFeedComments()
        newsCommentObject.newsfeed = NewsFeedStory(pk = storyId)
        newsCommentObject.comment = Comments(pk = commentObject.id)
        try:
            newsCommentObject.save()
        except Exception:
            c = Comments.objects.get(pk = commentObject.id)
            c.delete()
            return HttpResponse('comment_saving_error')
        else:
            #let us get all the comments for this photo and pass it to the view function
            commentData = NewsFeedComments.objects.filter(newsfeed = NewsFeedStory(pk = storyId)).values('comment_id')
            commentsId = list()
            for commentId in commentData:
                commentsId.insert(len(commentsId),commentId['comment_id'])

            #now let us get all the comments
            comments = Comments.objects.filter(id__in=commentsId)

            templateVariables = {
                'value': request.POST.get('comment'),
                'comments': comments,
                'PROFILE_IMAGE_UPLOAD_PATH': PROFILE_IMAGE_UPLOAD_PATH,
                'DATE_FORMAT': DATE_FORMAT,
                }

            return render_to_response('wall/wall_comments.html', templateVariables, context_instance=RequestContext(request))
    elif mode == 'like':
        #this mode will enable the user to like other posts
        storyId = request.POST.get('storyId', 0)
        if not storyId:
            return HttpResponse('invalid_story_error')

        #let us check if the current user is logged in
        if not request.user.is_authenticated():
            messages.error(request, _('You need to login to like any wall posts!'))
            return HttpResponse('user_not_logged_in_error')
        newsFeedLikeObject = NewsFeedLikes()
        newsFeedLikeObject.newsfeed = NewsFeedStory(pk = storyId)
        newsFeedLikeObject.user = User(pk = request.user.id)
        try:
            newsFeedLikeObject.save()
        except:
            return HttpResponse('exception_error')
        else:
            #get the count of likes
            likeCount = NewsFeedLikes.objects.filter(newsfeed = NewsFeedStory(pk = storyId)).count()
            templateVariables = {
                'likeCount': likeCount,
                'status': 'success'
            }
            return HttpResponse(simplejson.dumps(templateVariables), mimetype="application/json")

def _getNewFeeds(current_user, limit, offset = 0):
    """
    A private function which would get the news feed records
    """
    #get all the following list for this user
    followingObject = UserFollow.objects.filter(follower = current_user).only('following')
    followingList = list()
    for data in followingObject:
        followingList.append(data.following)
    #let us get all the latest feeds based on the story-limit and story-offset
    wallData =  NewsFeedStory.objects.filter(user__in = followingList).order_by('-updated_on')[offset:limit]
    if len(wallData) > 0:
        newData = list()
        for data in wallData:
            #we need to get the latest 3 snaps uploaded by the user
            photoList = Photos.objects.filter(~Q(image = None), user = data.user, category = PhotoCategories(pk = data.category.id))\
                        .values('id', 'image', 'title').order_by('-created_on')[:3]
            if not len(photoList):
                #if there are no photos then lets skip this record
                continue
                #get the likes count
            likeCounts = NewsFeedLikes.objects.filter(newsfeed = NewsFeedStory(pk = data.id)).count

            #get the comments for the story
            #        commentRelations = NewsFeedComments.objects.filter(newsfeed = NewsFeedStory(pk = data.id)).values('comment_id')
            #        commentsId = list()
            #        for commentId in commentRelations:
            #            commentsId.insert(len(commentsId),commentId['comment_id'])

            #now let us get all the comments
            #comments = Comments.objects.filter(id__in=commentsId)
            #commentsCount = len(comments)

            try:
                userProfile = data.user.get_profile()
            except UserProfile.DoesNotExist:
                profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/no-profile-medium.jpg"
            else:
                profileImage = userProfile.avatar
                if profileImage == '' or profileImage is None:
                    if userProfile.gender == 'M':
                        profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male.jpg"
                    elif userProfile.gender == 'F':
                        profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female.jpg"
                else:
                    profileImagePath = MEDIA_ROOT+PROFILE_IMAGE_UPLOAD_PATH + "/" + profileImage
                    if not os.path.exists(profileImagePath):
                        #If the file does not exist then load the file from other sources
                        if userProfile.gender == 'M':
                            profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male.jpg"
                        elif userProfile.gender == 'F':
                            profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female.jpg"
                    else:
                        profileImage =  PROFILE_IMAGE_UPLOAD_PATH + "/" +profileImage

            story = {
                'story_id': data.id,
                'story': data.story,
                'story_actor_username': data.user.username,
                'story_actor_firstname': data.user.first_name,
                'story_actor_lastname': data.user.last_name,
                'story_actor_image': profileImage,
                'photos': photoList,
                'likeCounts': likeCounts,
                'postDate': data.updated_on,
                #            'comments': comments,
                #            'commentsCount': commentsCount
            }

            newData.append(story)
    else:
        newData = None
    return newData

