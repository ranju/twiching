"""
Models for wall application

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from twiching.apps.photography.models import PhotoCategories
from twiching.apps.comment.models import Comments

# Create your models here.

class NewsFeedStory(models.Model):
    """
    This will display the news feed of all the latest photos uploaded by other users
    """
    user = models.ForeignKey(User)
    category = models.ForeignKey(PhotoCategories, default=0)
    story = models.TextField(_('Story'))
    created_on = models.DateTimeField(auto_now_add = True)
    updated_on = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        return self.story

class NewsFeedLikes(models.Model):
    """
    This model will store all the likes recieved for a News Feed
    """
    newsfeed = models.ForeignKey(NewsFeedStory)
    user = models.OneToOneField(User)
    created_on = models.DateTimeField(auto_now_add = True)
    updated_on = models.DateTimeField(auto_now = True)

class NewsFeedComments(models.Model):
    """
    Comments for a feed item is linked here
    """
    newsfeed = models.ForeignKey(NewsFeedStory)
    comment = models.ForeignKey(Comments)
