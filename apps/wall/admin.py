"""
Admin related code for wall application goes here

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""
from django.contrib import admin
from twiching.apps.wall.models import NewsFeedComments

admin.site.register(NewsFeedComments)