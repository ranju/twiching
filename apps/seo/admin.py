"""
This will enable the admin page for the seo tags

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""

from django.contrib import admin
from twiching.apps.seo.models import Tags

admin.site.register(Tags)

