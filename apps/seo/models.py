"""
This model will store the SEO related data like meta tags and description

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""

from django.db import models
from django.utils.translation import ugettext_lazy as _

class Tags(models.Model):
    """
    All the seo tags will be stored in this model
    """
    uri = models.CharField(_('URI'), max_length=250)
    title = models.CharField(_('Title'), max_length=250)
    meta_keyword = models.TextField(_('Meta Keyword'))
    meta_description = models.TextField(_('Meta Description'))

    def __unicode__(self):
        return self.uri

