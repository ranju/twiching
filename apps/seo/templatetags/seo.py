"""
This is a template tag which would add seo related meta tags

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""
__author__ = 'Swaroop Shankar V <swaroop@opentechnics.com>'

from django.template import Library
from twiching.apps.seo.models import Tags

register = Library()

@register.inclusion_tag('seo/custom_tag_output.html', takes_context=True)
def meta_keyword(context):
    path = context['request'].META['PATH_INFO']
    try:
        keyword = Tags.objects.get(uri = path).meta_keyword
    except Tags.DoesNotExist:
        keyword = ''
    return {
        'value': keyword,
    }

@register.inclusion_tag('seo/custom_tag_output.html', takes_context=True)
def meta_description(context):
    path = context['request'].META['PATH_INFO']
    try:
        description = Tags.objects.get(uri = path).meta_description
    except Tags.DoesNotExist:
        description = ''
    return {
        'value': description,
        }

@register.inclusion_tag('seo/custom_tag_output.html', takes_context=True)
def meta_title(context):
    path = context['request'].META['PATH_INFO']
    try:
        title = Tags.objects.get(uri = path).title
    except Tags.DoesNotExist:
        title = ''
    return {
        'value': title,
        }
