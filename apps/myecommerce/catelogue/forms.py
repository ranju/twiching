from django import forms
from django.forms import ModelForm
from twiching.apps.myecommerce.catelogue.models import *


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comments
        fields = ('name','text','email')

    def __init__(self, *args, **kwargs):
        super(CommentForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['style'] = 'font-size:16px'
        self.fields['text'].widget.attrs['style'] = 'font-size:16px'
	self.fields['email'].widget.attrs['style'] = 'font-size:16px'

    def clean_name(self):
        name = self.cleaned_data.get('name',False)
        if not name:
            raise forms.ValidationError("Please enter your name.")
        return name

    def clean_text(self):
        text = self.cleaned_data.get('text',False)
        if not text or len(text)>5000:
            raise forms.ValidationError("Please enter your comment.")
        return text

    def save(self,**kwargs):
        product = kwargs.pop('product')
        comments = super(CommentForm, self).save(commit=False)
        comments.product=product
        comments.save()

   
