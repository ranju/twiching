from oscar.apps.catalogue.models import Product
from oscar.apps.catalogue.views import ProductListView as CoreProductListView
from oscar.apps.catalogue.views import ProductCategoryView as CoreProductCategoryView
from oscar.apps.catalogue.views import ProductDetailView as CoreProductDetailView
from oscar.apps.order.models import Order

from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.http import HttpResponsePermanentRedirect, Http404
from django.views.generic import ListView, DetailView
from django.db.models import get_model
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
from oscar.apps.catalogue.models import *
from oscar.core.loading import get_class
from oscar.apps.catalogue.signals import product_viewed, product_search
from twiching.apps.myecommerce.catelogue.forms import CommentForm
from endless_pagination.decorators import page_template
from endless_pagination.views import AjaxListView
from django.utils.decorators import method_decorator
from twiching.apps.general.views import _sendNotification
from twiching.apps.general.models import StoreAdminEmails

Product = get_model('catalogue', 'product')
ProductReview = get_model('reviews', 'ProductReview')
Category = get_model('catalogue', 'category')
ProductAlert = get_model('customer', 'ProductAlert')
ProductAlertForm = get_class('customer.forms',
    'ProductAlertForm')

def product_view(request,id=None):
    if id:
        product = get_object_or_404(Product,id=id)
        
        return redirect('/products/catalogue/%s_%s/'%(product.slug,id))
    return redirect('/products/')

def change_order_status(request):
    if request.method == 'POST':
        id = request.POST.get('selected_order',None)
        status = request.POST.get('order_status',None)
        if id and status:
            try:
                order = Order.objects.get(id=id)
                old_status = order.status
                order.status = status
                order.save()
                messages.success(request,'Sucessfully changed status from %s to %s of Order #%s'%(old_status,status,order.number))
                return redirect('/products/dashboard/orders/')
            except:
                pass
        else:
            if not id:
                messages.error(request,'Please select an order.')
    
    return redirect('/products/dashboard/orders/')

def add_comment(request):
    from twiching.apps.myecommerce.catelogue.forms import CommentForm
    if request.method == 'POST':
        id = request.POST['id']
        product = get_object_or_404(Product,id=id)
        form = CommentForm(data=request.POST)
        if form.is_valid():
            form.save(product=product)
	    #messages.success(request,'Successfully submitted your comment and waiting for admin approval.')
	    admin_emails = StoreAdminEmails.objects.values_list('email')
            emails = []
            for mail in admin_emails:
		try:
		    if not mail[0] == 'store@twiching.com':
                        emails.append(mail[0])
		except:
		    pass
            if not request.user.is_anonymous:
                commenter_name = request.user.first_name + ' ' + request.user.last_name
                commenter_profile_link = 'http://' + request.META['HTTP_HOST'] + '/user/' + request.user.username
            else:
                commenter_name = request.POST['name']
                commenter_profile_link = 'mailto:'+request.POST['email']
            photo_name = product.title
            photo_url = 'http://' + request.META['HTTP_HOST'] + '/products/catalogue/%s_%s/'%(product.slug,product.id)
            photo_comment = request.POST['text']

            subject = commenter_name + ' commented on '+ product.title
	    email_template = "user_comment"
            to_email = emails
            http_host = request.META['HTTP_HOST']
            _sendNotification(
                subject=subject,
                content=photo_comment,
                to_email=to_email,
                http_host=http_host,
                commenter_name=commenter_name,
                commenter_profile_link=commenter_profile_link,
                photo_name=photo_name,
                photo_url=photo_url,
		email_template = email_template
            )
	else:
	    if request.POST['name'] == '' or request.POST['text'] == '':
                messages.error(request,'Comments: Required fields should not be empty')
		return redirect('/products/catalogue/%s_%s/'%(product.slug,product.id))

	    if not request.POST['text'] == '' and len(request.POST['text'])>5000:
                messages.error(request,'Comment size exceeded maximum limit.')
		return redirect('/products/catalogue/%s_%s/'%(product.slug,product.id))
	    messages.error(request,'Enter a valid email.')
	    return redirect('/products/catalogue/%s_%s/'%(product.slug,product.id))
    	return redirect('/products/catalogue/%s_%s/'%(product.slug,product.id))
    return redirect('/products/')



class ProductCategoryView(CoreProductCategoryView,AjaxListView):
    """
Browse products in a given category
"""
    context_object_name = "products"
    model = Product
    template_name = 'myecommerce/catalogue/browse.html'
    paginate_by = None
    page_template = "myecommerce/catalogue/pagination/load_products.html"

#    def get_categories(self):
#        """
#Return a list of the current category and it's ancestors
#"""
#        slug = self.kwargs['category_slug']
#        try:
#            category = Category.objects.get(slug=slug)
#        except Category.DoesNotExist:
#            raise Http404()
#        categories = list(category.get_descendants())
#        categories.append(category)
#        return categories
#
#    def get_context_data(self, **kwargs):
#        context = super(ProductCategoryView, self).get_context_data(**kwargs)
#
#        categories = self.get_categories()
#        context['categories'] = categories
#        context['category'] = categories[-1]
#        context['summary'] = categories[-1].name
#        return context
#
#    def get_queryset(self):
#        return get_product_base_queryset().filter(
#            categories__in=self.get_categories()



class ProductListView(CoreProductListView,AjaxListView):
    """
A list of products
"""
    context_object_name = "products"
    model = Product
    template_name = 'myecommerce/catalogue/browse.html'
    paginate_by = None
    search_signal = product_search
    page_template = "myecommerce/catalogue/pagination/load_products.html"

    #@method_decorator(page_template("myecommerce/catalogue/pagination/load_products.html"))
    def dispatch(self, *args, **kwargs):
        return super(ProductListView, self).dispatch(*args, **kwargs)

#
#    def get_search_query(self):
#        q = self.request.GET.get('q', None)
#        return q.strip() if q else q
#
#    def get_queryset(self):
#        q = self.get_search_query()
#        if q:
#            # Send signal to record the view of this product
#            self.search_signal.send(sender=self, query=q, user=self.request.user)
#            return get_product_base_queryset().filter(title__icontains=q)
#        else:
#            return get_product_base_queryset()
#
#    def get_context_data(self, **kwargs):
#        context = super(ProductListView, self).get_context_data(**kwargs)
#        q = self.get_search_query()
#        if not q:
#            context['summary'] = _('All products')
#        else:
#            context['summary'] = _("Products matching '%(query)s'") % {'query': q}
#            context['search_term'] = q
#        return context


class ProductDetailView(CoreProductDetailView):

    context_object_name = 'product'
    model = Product
    view_signal = product_viewed
    template_folder = "myecommerce/catalogue"

#    def get_object(self):
#        # Use a cached version to prevent unnecessary DB calls
#        if not hasattr(self, '_product'):
#            setattr(self, '_product',
#                super(CoreProductDetailView, self).get_object())
#        return self._product
#
#    def get_context_data(self, **kwargs):
#        ctx = super(CoreProductDetailView, self).get_context_data(**kwargs)
#        ctx['reviews'] = self.get_reviews()
#        ctx['alert_form'] = self.get_alert_form()
#        ctx['has_active_alert'] = self.get_alert_status()
#        return ctx
#
#    def get_alert_status(self):
#        # Check if this user already have an alert for this product
#        has_alert = False
#        if self.request.user.is_authenticated():
#            alerts = ProductAlert.objects.filter(
#                product=self.object, user=self.request.user,
#                status=ProductAlert.ACTIVE)
#            has_alert = alerts.count() > 0
#        return has_alert
#
#    def get_alert_form(self):
#        return ProductAlertForm(user=self.request.user,
#            product=self.object)
#
#    def get_reviews(self):
#        return self.object.reviews.filter(status=ProductReview.APPROVED)
#
#    def get(self, request, **kwargs):
#        # Ensure that the correct URL is used
#
#        product = self.get_object()
#        correct_path = product.get_absolute_url()
#        if correct_path != request.path:
#            return HttpResponsePermanentRedirect(correct_path)
#
#        response = super(CoreProductDetailView, self).get(request, **kwargs)
#        self.send_signal(request, response, product)
#        return response
#
#    def send_signal(self, request, response, product):
#        self.view_signal.send(sender=self, product=product, user=request.user,
#            request=request, response=response)

#    def get_template_names(self):
#        """
#        Return a list of possible templates.
#
#        We try 2 options before defaulting to catalogue/detail.html:
#        1). detail-for-upc-<upc>.html
#        2). detail-for-class-<classname>.html
#
#        This allows alternative templates to be provided for a per-product
#        and a per-item-class basis.
#        """
#
#        product = self.get_object()
#
#        names = ['%s/detail-for-upc-%s.html' % (self.template_folder, product.upc),
#                 '%s/detail-for-class-%s.html' % (self.template_folder, product.product_class.name.lower()),
#                 '%s/detail.html' % (self.template_folder)]
#
#        return names
