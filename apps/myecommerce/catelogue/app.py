from oscar.apps.catalogue.app import BaseCatalogueApplication as CoreBaseCatalogueApplication
from oscar.apps.catalogue.app import ReviewsApplication as CoreReviewsApplication
from django.conf.urls import patterns, url, include
from twiching.apps.myecommerce.catelogue import views
from twiching.apps.myecommerce.catelogue.reviews.app import application as reviews_app


class BaseCatalogueApplication(CoreBaseCatalogueApplication):
    name = 'catalogue'
    index_view = views.ProductListView
    category_view = views.ProductCategoryView
    detail_view = views.ProductDetailView

#    def get_urls(self):
#        urlpatterns = super(CoreBaseCatalogueApplication, self).get_urls()
#        urlpatterns += patterns('',
#            url(r'^$', self.index_view.as_view(), name='index'),
#            url(r'^(?P<product_slug>[\w-]*)_(?P<pk>\d+)/$',
#                self.detail_view.as_view(), name='detail'),
#            url(r'^(?P<category_slug>[\w-]+(/[\w-]+)*)/$',
#                self.category_view.as_view(), name='category')
#           )
#        return self.post_process_urls(urlpatterns)


class ReviewsApplication(CoreReviewsApplication):
    name = None
    reviews_app = reviews_app

    def get_urls(self):
        urlpatterns = super(CoreReviewsApplication, self).get_urls()
        urlpatterns += patterns('',
            url(r'^(?P<product_slug>[\w-]*)-(?P<product_pk>\d+)/reviews/',
                include(self.reviews_app.urls)),
        )
        return self.post_process_urls(urlpatterns)


application = BaseCatalogueApplication()