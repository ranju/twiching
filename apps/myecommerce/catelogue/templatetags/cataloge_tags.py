"""
This script would define template tags which would display blocks which are related to products or product categories
"""
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import Library
from django.utils.translation import ugettext_lazy as _
from oscar.apps.catalogue.models import Category, ProductClass, Product, ProductCategory, Comments
from twiching.apps.myecommerce.catelogue.forms import CommentForm
from oscar.apps.shipping.models import OrderAndItemCharges
import os

register = Library()

@register.inclusion_tag('myecommerce/catalogue/product_filter_tag.html', takes_context=True)
def get_categories(context):
    categories = Category.objects.all()
    classes = ProductClass.objects.all().order_by('id')
    products = Product.objects.all()
    product_cats = ProductCategory.objects.all()
    
    results=[]
    

    for cls in classes:
        new_pros = products.filter(product_class=cls)
        
        data = {}
        sub_results=[]
        for cat in categories:
            count = 0
            dict = {}
            for pro in new_pros:
                p_cats = product_cats.filter(product=pro, category=cat)
                if p_cats:
                    for p_cat in p_cats:
                        count = count + 1
            if count > 0:
                dict={'cat':cat,'count':count}
                sub_results.append(dict)
        if sub_results:
            data = {'class':cls,'val':sub_results}
            results.append(data)
    return { 'results':results}



@register.inclusion_tag('myecommerce/catalogue/best_buy_tag.html', takes_context=True)
def get_best_buy(context):
    categories = Category.objects.all()
    classes = ProductClass.objects.all().order_by('id')
    products = Product.objects.all()
    result = {}
    best_buy = []

    for cls in classes:
        new_pros = products.filter(product_class=cls,is_best_buy=True)
        if new_pros:
            result = {'class':cls,'products':new_pros}
            best_buy.append(result)

    #for cat in categories:
        #product_cat = cat.product_category.all()
        #result_products = []
        #for pro_cat in product_cat:
           # if pro_cat.product.is_best_buy:
               # result_products.append(pro_cat)
        #if result_products:
            #result = {'category':cat,'products':result_products}
            #best_buy.append(result)
    return {'results':best_buy}

@register.inclusion_tag('myecommerce/catalogue/featured_accessories_tag.html', takes_context=True)
def get_featured_accessories(context):
    all_products = Product.objects.filter(is_featured=True)
    classes = ProductClass.objects.filter(name = 'Accessories')
    if classes:
        classes = classes[0]
        products = all_products.filter(product_class=classes)
    return {'result':products,'cat':classes}
    #return {'result':products}


@register.inclusion_tag('myecommerce/catalogue/comments.html', takes_context=True)
def get_comments(context , id):
    product = get_object_or_404(Product,id=id)
    comments = Comments.objects.filter(product=product)
    return { 'product':product,'comments':comments,'form':CommentForm}


@register.simple_tag()
def get_shipping(basket):
    charge =  OrderAndItemCharges.objects.all()[0]
    amt = 0
    shipping = 0
    for line in basket.lines.all():
        amt += line.line_price_incl_tax
    if amt < charge.free_shipping_threshold:
        shipping = basket.num_lines * charge.price_per_item
        shipping += charge.price_per_order
        return shipping
    return '0.00'

@register.simple_tag()
def get_total_with_shipping(basket):
    shp = get_shipping(basket)
    amt = 0
    shipping = 0
    charge =  OrderAndItemCharges.objects.all()[0]
    for line in basket.lines.all():
        amt += line.line_price_incl_tax

    if amt < charge.free_shipping_threshold:
        shipping = basket.num_lines * charge.price_per_item
        shipping += charge.price_per_order
    return amt+shipping


