from oscar.apps.promotions.app import PromotionsApplication as CorePromotionsApplication
from twiching.apps.myecommerce.promotions.views import HomeView

class PromotionsApplication(CorePromotionsApplication):
    home_view  = HomeView

application = PromotionsApplication()