from django import forms
from django.contrib.sites.models import Site

from oscar.apps.order.models import Order,ShippingAddress,BillingAddress,Line,Country,LinePrice
from oscar.apps.shipping.models import OrderAndItemCharges

site = Site.objects.get(pk=1)

class CheckOutForm(forms.ModelForm):
    email = forms.EmailField(label="Email Address",required=True)
    mobile = forms.CharField(label="Phone Number",required=True)
    terms = forms.BooleanField(required=True)

    class Meta:
        model = ShippingAddress
        exclude = ('search_text',)

    def clean_first_name(self):
        fname = self.cleaned_data.get('first_name',False)

        if not fname:
            raise forms.ValidationError('This field is required.')
        
        return fname

    def clean_mobile(self):
        mob = self.cleaned_data.get('mobile',False)
        mobile = None
        try:
            mobile = int(mob)
        except:
            raise forms.ValidationError('Invalid phone number.')

        if len(mob)>10:
            raise forms.ValidationError('Number of digits should be less than 10.')

        return mobile

    

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', False)
        super(CheckOutForm, self).__init__(*args, **kwargs)
        self.fields['country'].widget.attrs['class'] = 'checkout-select'

    def save(self,**kwargs):
        basket = kwargs.pop('basket', False)
        user = kwargs.pop('user', False)
        shipping = ShippingAddress()
        shipping.first_name = self.cleaned_data['first_name']
        shipping.last_name = self.cleaned_data['last_name']
        shipping.line1 = self.cleaned_data['line1']
        shipping.line2 = self.cleaned_data['line2']
        shipping.state = self.cleaned_data['line3']
        shipping.postcode = self.cleaned_data['postcode']
        shipping.country = self.cleaned_data['country']
        shipping.phone_number	 = self.cleaned_data['mobile']
        shipping.save()

        billing = BillingAddress()
        billing.first_name = self.cleaned_data['first_name']
        billing.last_name = self.cleaned_data['last_name']
        billing.line1 = self.cleaned_data['line1']
        billing.line2 = self.cleaned_data['line2']
        billing.state = self.cleaned_data['line3']
        billing.postcode = self.cleaned_data['postcode']
        billing.country = self.cleaned_data['country']
        billing.save()


	charge =  OrderAndItemCharges.objects.all()[0]
        shipping_charge = 0.00
        if basket.total_incl_tax < charge.free_shipping_threshold:
            shipping_charge = basket.num_lines * charge.price_per_item
            shipping_charge += charge.price_per_order

        order = Order()
        order.number = billing.id + 1000
        order.site = site
        order.basket_id = basket.id
        order.user = user
        order.billing_address = billing
        if shipping_charge > 0:
            order.total_incl_tax = shipping_charge + basket.total_incl_tax
            order.total_excl_tax = shipping_charge + basket.total_excl_tax
            order.shipping_incl_tax = shipping_charge
            order.shipping_excl_tax = shipping_charge
        else:
            order.total_incl_tax = basket.total_incl_tax
            order.total_excl_tax = basket.total_excl_tax
            order.shipping_incl_tax = shipping_charge
            order.shipping_excl_tax = shipping_charge
        order.shipping_address = shipping
        order.shipping_method = 'Cash On Delivery'
        order.status = 'Order Placed'
        order.save()

        for line in basket.all_lines():
            if line.quantity > 0:
                order_line = Line()
                order_line.order = order
                order_line.partner = line.product.stockrecord.partner
                order_line.partner_name = line.product.stockrecord.partner.name
                order_line.partner_sku = line.product.stockrecord.partner_sku
                order_line.title = line.product.title
                order_line.upc = line.product.upc
                order_line.product = line.product
                order_line.quantity = line.quantity
                order_line.line_price_incl_tax = line.line_price_incl_tax
                order_line.line_price_excl_tax = line.line_price_excl_tax
                order_line.line_price_before_discounts_incl_tax = line.line_price_incl_tax_and_discounts
                order_line.line_price_before_discounts_excl_tax = line.line_price_excl_tax_and_discounts
                order_line.unit_cost_price = line.unit_price_incl_tax
                order_line.unit_price_incl_tax = line.unit_price_incl_tax
                order_line.unit_price_excl_tax = line.unit_price_excl_tax
                order_line.unit_retail_price = line.unit_price_incl_tax
                order_line.partner_line_reference = line.line_reference
                order_line.status = 'Order Placed'
                order_line.save()

                line.product.stockrecord.num_in_stock -= order_line.quantity
                line.product.stockrecord.save()

            

        return order





        



