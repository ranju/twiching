from django.conf.urls import patterns, url, include
from oscar.app import Shop
#from oscar.core.application import Application
#from oscar.apps.catalogue.app import application as catalogue_app
#from oscar.apps.customer.app import application as customer_app
#from oscar.apps.basket.app import application as basket_app
#from oscar.apps.checkout.app import application as checkout_app
#
#from oscar.apps.search.app import application as search_app
#from oscar.apps.offer.app import application as offer_app
#from oscar.apps.dashboard.app import application as dashboard_app

from twiching.apps.myecommerce.promotions.app import  application as promotions_app
from twiching.apps.myecommerce.catelogue.app import application as catalogue_app
from twiching.apps.myecommerce.basket.app import application as basket_app
from twiching.apps.myecommerce.checkout.app import application as checkout_app
#from twiching.urls import urlpatterns


class BaseApplication(Shop):
    promotions_app = promotions_app
    catalogue_app = catalogue_app
#    customer_app = customer_app
    basket_app = basket_app
    checkout_app = checkout_app
#    search_app = search_app
#    dashboard_app = dashboard_app
#    offer_app = offer_app


    def get_urls(self):
#        urlpatterns = super(twitching, self).get_urls()
        urlpatterns =  patterns('',
            (r'^catalogue/', include(self.catalogue_app.urls)),
            (r'^cart/', include(self.basket_app.urls)),
            (r'^checkout/', include(self.checkout_app.urls)),
            (r'^accounts/', include(self.customer_app.urls)),
            (r'^search/', include(self.search_app.urls)),
            (r'^dashboard/', include(self.dashboard_app.urls)),
            (r'^offers/', include(self.offer_app.urls)),
            (r'^home/', include(self.promotions_app.urls)),
            (r'', include(self.catalogue_app.urls)),

        )
        return urlpatterns

application = BaseApplication()