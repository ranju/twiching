from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from twiching.apps.myecommerce.basket.views import (BasketView, SavedView,
                                     VoucherAddView, BasketAddView, VoucherRemoveView)
from oscar.apps.basket.app import BasketApplication as CoreBasketApplication

class BasketApplication(CoreBasketApplication):
    name = 'basket'
    summary_view = BasketView
    saved_view = SavedView
    add_view = BasketAddView
    add_voucher_view = VoucherAddView
    remove_voucher_view = VoucherRemoveView

    def get_urls(self):
        urlpatterns = patterns('',
            url(r'^$', self.summary_view.as_view(), name='summary'),
            url(r'^add-to-cart/$', self.add_view.as_view(), name='add_cart'),
            url(r'^vouchers/add/$', self.add_voucher_view.as_view(),
                name='vouchers-add'),
            url(r'^vouchers/(?P<pk>\d+)/remove/$',
                self.remove_voucher_view.as_view(), name='vouchers-remove'),
            url(r'^saved/$', login_required(self.saved_view.as_view()),
                name='saved'),
        )
        return self.post_process_urls(urlpatterns)


application = BasketApplication()