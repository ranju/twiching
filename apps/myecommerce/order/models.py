from oscar.apps.order.models import *
from django.db import models
from twiching.apps.my_ecommerce.order.abstract_models import AbstractOrder

class Order(AbstractOrder):

    site = models.ForeignKey('sites.Site', related_name='order_Site')