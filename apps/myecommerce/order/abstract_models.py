from django.db import models
from oscar.apps.order.abstract_models import *

class AbstractOrder(models.Model):
    site = models.ForeignKey('sites.Site', related_name='order_Site')