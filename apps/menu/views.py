"""
This application will enable the menus for the application. The menu system will be dynamic and it will enable 
the admin to add, edit and remove menu items. There will be specific template tags and filters for the menu
"""

from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.contrib.flatpages.models import FlatPage
from django.utils.translation import ugettext_lazy as _
from twiching.apps.menu.models import Menu