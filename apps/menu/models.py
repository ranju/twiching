"""
Model for the menu apps

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

from django.db import models
from django.utils.translation import ugettext_lazy as _
import mptt
from mptt.models import MPTTModel, TreeForeignKey
from django.contrib.auth.models import Group

class MenuRegion(models.Model):
    '''
    This class would specify the menu regions like header, footer etc that is available on the site
    '''
    name = models.CharField(_('Menu Region Name'), max_length=50, null=False)
    
    def __unicode__(self):
        return self.name

    
class Menu(MPTTModel):
    '''
    This model is where we will store all the menu links and its hierarchy 
    '''
    name = models.CharField(_('Menu Name'), max_length=50, null=False)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    url = models.TextField(_('URL'), null = False)
    new_window = models.BooleanField(_('Display in New Window'), null = False, default = 0)
    region = models.ForeignKey(MenuRegion)
    order = models.IntegerField(_('Priority'))
    active = models.BooleanField(_('Active'), null = False, default = 1)
    user_type = models.ForeignKey(Group, null = True, blank=True)
    login_required = models.BooleanField(_('Login Required'), default=0)
    created_on = models.DateTimeField(auto_now_add = True, null=True)
    updated_on = models.DateTimeField(auto_now=True)
    
    class MPTTMeta:
        order_insertion_by = ['order']
    
    def __unicode__(self):
        return self.name