"""
This script would have the template tags for generating menus
"""

from django import template
from twiching.apps.menu.models import Menu, MenuRegion
from product.models import Category
from django.contrib.sites.models import Site
from django.core.cache import cache
from django.utils.translation import get_language
import logging
from satchmo_store.shop.templatetags.satchmo_category import recurse_for_children

log = logging.getLogger('shop.templatetags')

try:
    from xml.etree.ElementTree import Element, SubElement, tostring
except ImportError:
    from elementtree.ElementTree import Element, SubElement, tostring

register = template.Library()

@register.inclusion_tag('menu/custom_tag_header_menu.html', takes_context=True)
def header_menu(context):
    menuList = Menu.objects.all().filter(active = True, region = MenuRegion(pk = 1)).order_by('order')
    #rebuild the menu list based on the permissions and group
    if context['request'].user.is_authenticated():
        authenticated = 1
        if context['request'].user.is_superuser or context['request'].user.is_staff:
            group = None
        else:
            group = context['request'].user.groups.values_list('id',flat=True)

    else:
        authenticated = 0
        group = None
    return { 'nodes':menuList, 'authenticated': authenticated, 'group': group}

@register.inclusion_tag('menu/custom_tag_footer_menu.html', takes_context=True)
def footer_menu(context):
    if context['request'].user.is_authenticated():
        username = context['request'].user.username
        userAvailable = 1
    else:
        userAvailable = 0
        username = None

    return {'userAvailable':userAvailable, 'username': username}
@register.inclusion_tag('menu/custom_tag_header_menu_vertical.html', takes_context=True)
def header_vertical_menu(context):
    menuList = Menu.objects.all().filter(active = True).order_by('order')
    #rebuild the menu list based on the permissions and group
    if context['request'].user.is_authenticated():
        authenticated = 1
        if context['request'].user.is_superuser or context['request'].user.is_staff:
            group = None
        else:
            group = context['request'].user.groups.values_list('id',flat=True)

    else:
        authenticated = 0
        group = None

    return { 'nodes':menuList, 'authenticated': authenticated, 'group': group}

@register.inclusion_tag('menu/custom_tag_static_menu.html', takes_context=True)
def static_menu(context):
    if context['request'].user.is_authenticated():
        username = context['request'].user.username
        userAvailable = 1
    else:
        userAvailable = 0
        username = None

    return {'userAvailable':userAvailable, 'username': username}


@register.simple_tag
def ecommerce_category(id=None):
    """
    Creates an unnumbered list of the categories.
    """
    active_cat = None
    if id:
        try:
            active_cat = Category.objects.active().get(id=id)
        except Category.DoesNotExist:
            active_cat = None
        # We call the category on every page so we will cache
    # The actual structure to save db hits
    lang = get_language()
    current_site = Site.objects.get_current()
    cache_key = "cat-%s-%s" % (current_site.id, lang)
    existing_tree = cache.get(cache_key, None)
    if existing_tree is None:
        root = Element("ul")
        root.attrib["class"] = "dropmenu"
        root.attrib["id"] = "nav-two"
        for cats in Category.objects.root_categories():
            recurse_for_children(cats, root, active_cat)
        existing_tree = root
        cache.set(cache_key, existing_tree)
        # If we have an active cat, search through and identify it
    # This search is less expensive than the multiple db calls
    if active_cat:
        active_cat_id = "category-%s" % active_cat.id
        for li in existing_tree.iter("li"):
            if li.attrib["id"] == active_cat_id:
                link = li.find("a")
                link.attrib["class"] = "current"
                break
    return tostring(existing_tree, 'utf-8')
        