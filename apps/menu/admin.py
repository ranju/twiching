"""
Admin related code for accounts application goes here

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from twiching.apps.menu.models import Menu, MenuRegion

class MenuAdmin(MPTTModelAdmin):
    list_display = ('name', 'region', 'active',)
    list_filter = ('region',)
admin.site.register(Menu, MenuAdmin)
admin.site.register(MenuRegion)

