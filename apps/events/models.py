"""
Here we have the database related code for the accounts application

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""
from os import path
import hashlib
import time
from django.db import models
from django.utils.translation import ugettext_lazy as _
from twiching.thirdparty.countries.models import Country
from twiching.settings import EVENTS_IMAGE_UPLOAD_PATH

def get_image_path(instance, filename):
    """
    This will rename the uploaded file based on a random generated string based on the uploaded time and filename etc
    """
    randomString = hashlib.md5(filename).hexdigest()+str(int(time.time()))
    filename =  str(randomString) + filename
    return path.join(EVENTS_IMAGE_UPLOAD_PATH,  filename)

class EventCategory(models.Model):
    """
    Defines the model for storing different categories of an event
    """
    name = models.CharField(_('Category Name'), max_length=250)
    description = models.TextField(_('Description'), null = True, blank = True)
    created_on = models.DateTimeField(auto_now_add = True)
    updated_on = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        return self.name

class Event(models.Model):
    """
    Stores the event information
    """
    name = models.CharField(_('Event Name'), max_length=250)
    city = models.CharField(_('Event City'), max_length=250)
    country = models.ForeignKey(Country)
    category = models.ForeignKey(EventCategory)
    date = models.DateField(_('Event Date'))
    sponsor = models.CharField(_('Event Sponsor'), max_length=250, null = True, blank=True)
    content = models.TextField(_('Event Description'))
    created_on = models.DateTimeField(auto_now_add = True, null=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name

class EventPhotos(models.Model):
    """
    Stores the photographs of an events
    """
    event = models.ForeignKey(Event)
    title = models.CharField(_('Title'), max_length=250, null = True, blank = True)
    description = models.TextField(_('Description'), null = True, blank = True)
    image = models.ImageField(_('Image'), upload_to=get_image_path)
    default_image = models.BooleanField(_('Default Image'), default=False)
    created_on = models.DateTimeField(auto_now_add = True)
    updated_on = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        return self.title
