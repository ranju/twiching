"""
Admin related code for events application goes here

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""

from django.contrib import admin
from twiching.apps.events.models import EventCategory, Event, EventPhotos

class EventPhotosInline(admin.StackedInline):
    model = EventPhotos

class EventsAdmin(admin.ModelAdmin):
    inlines = [EventPhotosInline,]


admin.site.register(EventCategory)
admin.site.register(Event, EventsAdmin)