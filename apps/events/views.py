"""
View for events functionality

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""

from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
from twiching.apps.events.models import Event, EventCategory, EventPhotos
from twiching.settings import DATE_FORMAT, PROFILE_IMAGE_UPLOAD_PATH, PHOTOGRAPH_UPLOAD_PATH
from django.http import Http404

def eventsList(request, categoryId = 0):

    """
    This function would list all the events paginated
    @param categoryId The category id of which the events should be displayed
    """
    categoryList = EventCategory.objects.all()

    if categoryId == 0:
        eventList = Event.objects.all().order_by('-created_on')
    else:
        eventList = Event.objects.filter(category = EventCategory(pk = categoryId))
    eventListNew = list()
    for event in eventList:
        #get all the photos for this event
        photoDefault = dict()
        photoList= list()
        data = dict()
        try:
            photoObject = EventPhotos.objects.filter(event = Event(pk = event.id))
        except EventPhotos.DoesNotExist:
            pass
        else:
            for photos in photoObject:
                if photos.default_image:
                    photoDefault['image'] = photos.image
                    photoDefault['description'] = photos.title
                else:
                    photo = dict()
                    photo['image'] = photos.image
                    photo['description'] = photos.title
                    photoList.append(photo)

            data['event'] = event
            data['default_photo'] = photoDefault
            data['photos'] = photoList
        eventListNew.append(data)

    #Additional Data
    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Events', 'url': '/events/'},
    ]

    templateVariables = {
        'eventList': eventListNew,
        'categoryId': int(categoryId),
        'breadcrumb': breadcrumb,
        'DATE_FORMAT': DATE_FORMAT,
        'PROFILE_IMAGE_UPLOAD_PATH': PROFILE_IMAGE_UPLOAD_PATH,
        'PHOTOGRAPH_UPLOAD_PATH': PHOTOGRAPH_UPLOAD_PATH,
        'categoryList': categoryList,
    }
    return render_to_response('events/event_listing.html', templateVariables, context_instance=RequestContext(request))

def event(request, event_id = 0):
    """
    The individual event item listing
    """
    if event_id == 0:
        raise Http404

    categoryList = EventCategory.objects.all()

    try:
        event = Event.objects.get(pk = event_id)
    except Event.DoesNotExist:
        messages.error(request, _('Sorry! You don\'t have permission to access that profile'))
        return redirect('/events')

    data = dict()
    photoDefault = dict()
    photoList = list()

    try:
        photoObject = EventPhotos.objects.filter(event = Event(pk = event.id))
    except EventPhotos.DoesNotExist:
        pass
    else:
        for photos in photoObject:
            if photos.default_image:
                photoDefault['image'] = photos.image
                photoDefault['description'] = photos.title
            else:
                photo = dict()
                photo['image'] = photos.image
                photo['description'] = photos.title
                photoList.append(photo)

        data['event'] = event
        data['default_photo'] = photoDefault
        data['photos'] = photoList

        #Additional Data
    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Events', 'url': '/events/'},
            {'name': event.name, 'url': '/event/'+str(event.id)},
    ]

    templateVariables = {
        'data': data,
        'breadcrumb': breadcrumb,
        'DATE_FORMAT': DATE_FORMAT,
        'PROFILE_IMAGE_UPLOAD_PATH': PROFILE_IMAGE_UPLOAD_PATH,
        'PHOTOGRAPH_UPLOAD_PATH': PHOTOGRAPH_UPLOAD_PATH,
        'categoryList': categoryList,
        }
    return render_to_response('events/single_event.html', templateVariables, context_instance=RequestContext(request))



