"""
This script would define template tags which would display blocks which are related to users
"""
__author__ = 'swaroop'

from django import template
from twiching.settings import MEDIA_URL, DATE_FORMAT, STATIC_URL
from django.utils.translation import ugettext_lazy as _
from twiching.apps.events.models import Event, EventPhotos

register = template.Library()

@register.inclusion_tag('events/custom_tag_latest_event.html', takes_context=True)
def latest_event(context):
    #get the latest event
    data = dict()
    eventData = Event.objects.order_by('-date')[:1]
    for event in eventData:
        data['id'] = event.id
        data['name'] = event.name
        data['city'] = event.city
        data['country'] = event.country.printable_name
        data['sponsor'] = event.sponsor
        data['date'] = event.date
        try:
            defaultPhoto = EventPhotos.objects.get(event = Event(pk = event.id), default_image = True)
        except EventPhotos.DoesNotExist:
            data['defaultPhoto'] = None
        else:
            data['defaultPhoto'] = defaultPhoto.image
    return {
        'eventData': data,
        'MEDIA_URL': MEDIA_URL,
        'DATE_FORMAT': DATE_FORMAT,
        'STATIC_URL': STATIC_URL
    }
