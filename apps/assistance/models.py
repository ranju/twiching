"""
Here we have the database related code for the assistance page

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""

__author__ = 'Swaroop Shankar V <swaroop@opentechnics.com>'

import os
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site
from twiching.settings import MEDIA_URL


def get_image_path(instance, filename):
    """
    This will rename the uploaded file based on a random generated string based on the uploaded time and filename etc
    """
    import time
    import hashlib
    from twiching.settings import PHOTOGRAPH_ORIGINAL_UPLOAD_PATH
    randomString = hashlib.md5(filename).hexdigest() + str(int(time.time()))
    filename = str(randomString) + filename
    return os.path.join(PHOTOGRAPH_ORIGINAL_UPLOAD_PATH,  filename)


class AssistanceSetting(models.Model):
    '''
    Setting for assistance
    '''
    site = models.OneToOneField(Site, related_name='Site')
    name = models.CharField(_('Name'), max_length=250, null=False, blank=False)
    note = models.TextField(_('Note'), null=False, blank=False)
    contact_address = models.TextField(_('Contact Address'), null=False, blank=False)
    contact_telephone = models.CharField(_('Contact Telephone (Format: 971 2345 432 181)'), max_length=20, null=True, blank=True)
    contact_email = models.EmailField(_('Contact Email'), null=True, blank=True)
    printing_assistance_url = models.URLField(_('Printing Assistance URL'), null=True, blank=True)

    def __unicode__(self):
        return self.site.name


class Category(models.Model):
    title = models.CharField(_('Name'), max_length=250, null=False, blank=False)
    description = models.TextField('Description', null=True, blank=True)
    active = models.BooleanField(_('Active'), default=1)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title


class Album(models.Model):
    '''
    Album for assistance
    '''
    title = models.CharField(_('Name'), max_length=250, null=False, blank=False)
    category = models.ForeignKey(Category)
    description = models.TextField('Description', null=True, blank=True)
    location = models.CharField(_('Location'), max_length=250, null=True, blank=True)
    photographers = models.TextField(_('Photographers'), null=True, blank=True)
    active = models.BooleanField(_('Active'), default=1)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title


class Photograph(models.Model):
    title = models.CharField(_('Title'), max_length=250, null=True, blank=True)
    album = models.ForeignKey(Album)
    album_image = models.BooleanField(_('Set as Album Image'), default=0, null=False, blank=True)
    description = models.TextField(_('Description'), null=True, blank=True)
    image = models.ImageField(_('Image'), upload_to=get_image_path)
    active = models.BooleanField(_('Active'), default=1)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def photograph(self):
        return '<img height="86px" width="116px" src="' + MEDIA_URL + '%s" />' % self.image
    photograph.allow_tags = True

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        #we need to check if there is already a default album image assigned, if yes then remove that and set the new
        #image as the album image
        normal_save = kwargs.get('normal_save', False)
        if normal_save == True:
            super(Photograph, self).save()
        else:
            if self.album_image == True:
                try:
                    photographyObject = Photograph.objects.get(album_image=True, album=Album(pk=self.album))
                except Photograph.DoesNotExist:
                    pass
                else:
                    photographyObject.album_image = False
                    photographyObject.save(normal_save=True)
            super(Photograph, self).save()
