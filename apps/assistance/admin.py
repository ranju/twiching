"""
Admin related code for assistance application goes here

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""

__author__ = 'Swaroop Shankar V <swaroop@opentechnics.com>'

from django.contrib import admin
from twiching.apps.assistance.models import Album, AssistanceSetting, Photograph, Category
from twiching.apps.assistance.views import _deletePhotographs

class AlbumAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    list_filter = ('category',)

class PhotographAdmin(admin.ModelAdmin):
    list_display = ('title','photograph', 'album', 'created_on')
    search_fields = ('title',)
    list_filter = ('album',)

    def delete_model(self, request, obj):
        """
        On deletion of an photograph record we need to delete the images that's stored in the drive too
        """
        imageName = obj.image
        _deletePhotographs(imageName)

admin.site.register(Category)
admin.site.register(AssistanceSetting)
admin.site.register(Album, AlbumAdmin)
admin.site.register(Photograph, PhotographAdmin)