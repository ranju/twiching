"""
View page for the assistance application.

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""

__author__ = 'Swaroop Shankar V <swaroop@opentechnics.com>'

import os
from django.contrib.sites.models import Site
from django.http import Http404
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from twiching.settings import MEDIA_ROOT, SITE_ID, DATE_FORMAT
from twiching.apps.assistance.models import Album, AssistanceSetting, Photograph, Category


def Index(request, category_id=None):
    """
    This is the page that is displayed while clicking on to the assistance page
    """
    try:
        settings = AssistanceSetting.objects.get(site=Site(pk=SITE_ID))
    except AssistanceSetting.DoesNotExist:
        raise Http404

    categories = Category.objects.all().filter(active=1)
    current_category = None
    if category_id is not None:
        #get current category name
        try:
            current_category = Category.objects.get(pk=int(category_id))
        except Category.DoesNotExist:
            raise Http404
        else:
            category_id = int(category_id)
            try:
                albumsObject = Album.objects.all().filter(category=Category(pk=category_id), active=1)
            except Album.DoesNotExist:
                raise Http404
    else:
        try:
            albumsObject = Album.objects.all().filter(active=1)
        except Album.DoesNotExist:
            raise Http404

    albumList = list()
    for album in albumsObject:
        try:
            albumImage = Photograph.objects.get(album=album, album_image=1).image
        except Photograph.DoesNotExist:
            albumImage = None
        data = {
            'id': album.id,
            'title': album.title,
            'album_image': albumImage,
            'category':  album.category.title,
            'category_id': album.category.id,
            'created_on': album.created_on,
        }
        albumList.append(data)

    #Continue rest of the processing
    if current_category is not None:
        breadcrumb = [
                {'name': 'Home', 'url': '/'},
                {'name': settings.name, 'url': '/assistance'},
                {'name': current_category.title, 'url': '/assistance/' + str(current_category.id)},
        ]
    else:
        breadcrumb = [
                {'name': 'Home', 'url': '/'},
                {'name': settings.name, 'url': '/assistance'},
        ]

    templateVariables = {
        'breadcrumb': breadcrumb,
        'albums': albumList,
        'settings': settings,
        'categories': categories,
        'category_id': category_id,
        'DATE_FORMAT': DATE_FORMAT,
        'current_category': current_category,
    }

    return render_to_response('assistance/index.html', templateVariables, context_instance=RequestContext(request))


def AlbumPhotos(request, album_id=None):
    if album_id is None:
        raise Http404

    #check if the album exist
    try:
        album = Album.objects.get(pk=album_id)
    except Album.DoesNotExist:
        raise Http404

    settings = AssistanceSetting.objects.get(site=Site(pk=SITE_ID))
    categories = Category.objects.all().filter(active=1)
    photographs = Photograph.objects.filter(album=Album(pk=album_id), active=1)
    if len(photographs) == 0:
        messages.error(request, _("No images uploaded under this section"))

    #Continue rest of the processing
    breadcrumb = [
        {'name': 'Home', 'url': '/'},
        {'name': settings.name, 'url': '/assistance'},
        {'name': album.category.title, 'url': '/assistance/' + str(album.category.id)},
        {'name': album.title, 'url': '/assistance/album/' + str(album.id)}
    ]

    templateVariables = {
        'breadcrumb': breadcrumb,
        'photographs': photographs,
        'album': album,
        'DATE_FORMAT': DATE_FORMAT,
        'settings': settings,
        'categories': categories,
    }
    return render_to_response('assistance/album_photos.html', templateVariables, context_instance=RequestContext(request))


def _deletePhotographs(image):
    """
    This function would delete all the photographs that uploaded by the user based on the image passed
    @param image: The name of the image that needs to be deleted
    @access private
    """
    try:
        imagePath = MEDIA_ROOT + str(image)
        os.unlink(imagePath)
    except IOError:
        pass

    return True
