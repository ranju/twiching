"""
View page for the search application.

@author: Aswathy Rajesh, Swaroop Shankar V
@organization: Opentechnics
@contact: aswathi.r@opentechnics.com, swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""


import os
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth.models import User, Group
from django.http import Http404
from twiching.apps.photography.models import PhotoCategories, Photos
from twiching.apps.accounts.models import UserProfile
from haystack.query import SearchQuerySet
from twiching.apps.atoz.models import Post
#from product.models import Product
from oscar.apps.catalogue.models import Product
from twiching.settings import PROFILE_IMAGE_UPLOAD_PATH, MEDIA_ROOT,ENABLED_GROUP_ID_MAPPING, LEARNING_CENTER_ID, DATE_FORMAT
from urllib import unquote
from django.db.models import Q
from endless_pagination.decorators import page_template

def searchSite(request,action,sTerm,categoryId=0,sOrder="asc"):
    """
    This is the common function for searching photos, learning center, store and users
    """
    sTerm = unquote_u(sTerm)

    if action == "user":
        result = _searchUser(request,sTerm,categoryId,sOrder)
        if result["template"]:
            template = result["template"]
        else:
            template = 'search/result/search_result_profile.html'
        return render_to_response(template, result["templateVariables"],
            context_instance=RequestContext(request))

    elif action == "photo":
        result = _searchPhoto(request,sTerm,categoryId,sOrder)
        
        if result["template"]:
            template = result["template"]
        else:
            template = 'search/result/search_result_photos.html'
        return render_to_response(template, result["templateVariables"],
            context_instance=RequestContext(request))

    elif action == "learnCenter":
        result = _searchLearnCenter(request,sTerm,categoryId,sOrder)

	if result["template"]:
            template = result["template"]
        else:
            template = 'search/result/search_result_learning.html'
        return render_to_response(template, result["templateVariables"],
            context_instance=RequestContext(request))
            
    elif action == "store":
        result = _searchStore(request,sTerm,categoryId,sOrder)
	if result["template"]:
            template = result["template"]
        else:
            template = 'search/result/search_result_store.html'
        return render_to_response(template, result["templateVariables"],
            context_instance=RequestContext(request))

@page_template("search/result/users_pagination.html")
def _searchUser(request,sTerm, categoryId=0,sOrder="asc",extra_context=None,template=None):
    """
    This function search for users with given criteria
    """
    result = {}
    
    #for set sort order
    if sOrder == "desc" :
        sortBy = "-first_name"
    else:
        sortBy = "first_name"

#    results = SearchQuerySet().filter(text=sTerm).models(User).order_by(sortBy)
    results = User.objects.filter(
                      Q(username__icontains=sTerm) |
                      Q(first_name__icontains=sTerm) |
                      Q(last_name__icontains=sTerm)|
                      Q(email__icontains=sTerm)
                      )
    
    pList = []
    UserObj = []
    userCnt = 0
    for userData in results:
        UserObj.append(userData)

    for userData in UserObj:
        try:
            userProfile = userData.get_profile()
        except UserProfile.DoesNotExist:
            profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/no-profile-medium.jpg"
            countryIso = None
        else:
            profileImage = userProfile.avatar
            if profileImage == '' or profileImage is None:
                if userProfile.gender == 'F':
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female-small.jpg"
                elif userProfile.gender == 'M':
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male-small.jpg"
            else:
                profileImagePath = MEDIA_ROOT + PROFILE_IMAGE_UPLOAD_PATH + "/" + profileImage
                if not os.path.exists(profileImagePath):
                    #If the file does not exist then load the file from other sources
                    if userProfile.gender == 'F':
                        profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-female-small.jpg"
                    elif userProfile.gender == 'M':
                        profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/avatar-male-small.jpg"
                else:
                    profileImage = PROFILE_IMAGE_UPLOAD_PATH + "/" + profileImage
            try:
                countryIso = str(userProfile.country.iso).lower()
            except AttributeError:
                countryIso = None
                #get group name
        try:
            groupName = Group.objects.get(user=User(userData.id)).name
        except Group.DoesNotExist:
            groupName = ''

        data = {
            'username': userData.username,
            'first_name': userData.first_name,
            'last_name': userData.last_name,
            'full_name': userData.first_name + ' ' + userData.last_name,
            'avatar': profileImage,
            'group_name': groupName,
            'last_login': userData.last_login,
            'country_iso': countryIso,
            'twichers': userProfile.twichers,

        }
        pList.append(data)

    #let us get the group names
    #groupList = Group.objects.all()
    groupList = ""
    group_id = ''
    breadcrumb = [
            {'name': sTerm},
            {'name': 'Profiles'},
    ]

    templateVariables = {
        'data': pList,
        'DATE_FORMAT': DATE_FORMAT,
        'groupList': groupList,
        'filter': filter,
        'group_id': group_id,
        'searchTerm':sTerm,
        'userCnt':results.count,
        'sOrder':sOrder,
        'title': "User search results",
        'searchType': "user",
        }

    if extra_context is not None:
           templateVariables.update(extra_context)
    result = {"templateVariables":templateVariables,"template":template}

    return result

@page_template("search/result/photos_pagination.html")
def _searchPhoto(request,sTerm, categoryId=0,sOrder="asc",extra_context=None,template=None):
    """
    This function search for photos with given criteria
    """

    group_id = 1

    #Get all Categories
    categoryList = PhotoCategories.objects.all()

    try:
        groupObject = Group.objects.get(pk=group_id)
    except Group.DoesNotExist:
        raise Http404

    groupName = groupObject.name.title()

    #for set sort order
    if sOrder == "desc" :
        sortBy = "-title"
    else:
        sortBy = "title"
    #if category id, filter with it
    if int(categoryId) > 0 :
        results = SearchQuerySet().filter(text=sTerm, CategId=categoryId).models(Photos).order_by(sortBy)
        photos = Photos.objects.filter(
                      Q(title__icontains=sTerm) |
                      Q(description__icontains=sTerm) 
                     
                      )
    else:
        results = SearchQuerySet().filter(text=sTerm).models(Photos).order_by(sortBy)
        photos = Photos.objects.filter(
                      Q(title__icontains=sTerm) |
                      Q(description__icontains=sTerm) 
                      
                      
                      )
    photoObjects = []

    for photoData in photos:
        photoObjects.append(photoData)

    group_id_mapping = list()
    for key in ENABLED_GROUP_ID_MAPPING:
        value = ENABLED_GROUP_ID_MAPPING[key]

        if group_id == value:
            continue
        else:
            group_id_mapping.append(value)

     #Additional Data
    templateVariables = {
        'photoObjects': photoObjects,
        'group_id': int(group_id),
        'group_id_mapping': group_id_mapping,
        'categoryList': categoryList,
        'searchTerm':sTerm,
        'imgCnt':photos.count,
        'selectedCat':int(categoryId),
        'sOrder':sOrder,
        'title': "Photo search results",
        'searchType': "photos",
        }

    if extra_context is not None:
           templateVariables.update(extra_context)
    result = {"templateVariables":templateVariables,"template":template}

    return result

@page_template("search/result/learningcenter_pagination.html")
def _searchLearnCenter(request,sTerm, categoryId=0,sOrder="asc",extra_context=None,template=None):
    """
    This function search for news, articles and twichorials with given criteria
    """
    #for set sort order
    result = {}
    
    if sOrder == "desc" :
        sortBy = "-title"
    else:
        sortBy = "title"
    if int(categoryId) > 0 :
        #results = SearchQuerySet().filter(content=sTerm, categId=categoryId).models(Post).order_by(sortBy)
        posts = Post.objects.filter(
                      Q(title__icontains=sTerm) |
                      Q(summary__icontains=sTerm) |
                      Q(content__icontains=sTerm)|
                      Q(meta_tags__icontains=sTerm) |
                      Q(meta_content__icontains=sTerm)
                      )
    else:
        #results = SearchQuerySet().filter(content=sTerm).models(Post,).order_by(sortBy)
        posts = Post.objects.filter(
                      Q(title__icontains=sTerm) |
                      Q(summary__icontains=sTerm) |
                      Q(content__icontains=sTerm)|
                      Q(meta_tags__icontains=sTerm) |
                      Q(meta_content__icontains=sTerm)
                      )

    
    learnObjects = []
#    for lrnData in results:
#        learnObjects.append(lrnData.object)
    for lrnData in posts:
        learnObjects.append(lrnData)

    templateVariables = {
        'featuredPosts': learnObjects,
        'DATE_FORMAT': DATE_FORMAT,
        'LEARNING_CENTER_ID': LEARNING_CENTER_ID,
        'searchTerm':sTerm,
        'postCnt':posts.count,
        'selectedCat':int(categoryId),
        'sOrder':sOrder,
        'title': "Learning center search results",
        'searchType': "learn",
        }

    if extra_context is not None:
           templateVariables.update(extra_context)
    result = {"templateVariables":templateVariables,"template":template}

    return result

@page_template("search/result/store_pagination.html")
def _searchStore(request,sTerm, categoryId=0,sOrder="asc",extra_context=None,template=None):
    """
    This function search for products with given criteria
    """
    #for sorting
    if sOrder == "desc" :
        sortBy = "-name"
    else:
        sortBy = "name"

    #results = SearchQuerySet().filter(text=sTerm).models(Product).order_by(sortBy)
    results = Product.objects.filter(Q(title__icontains=sTerm))

                      
    products = []

#    for prdData in results:
#        products.append(prdData.object)
    for prdData in results:
         products.append(prdData)
    templateVariables = {
        'products': products,
        'searchTerm':sTerm,
        'prdCnt':results.count,
        'sOrder':sOrder,
        'title': "Store search results",
        'searchType': "store",
    }

    if extra_context is not None:
           templateVariables.update(extra_context)
    result = {"templateVariables":templateVariables,"template":template}

    return result

def unquote_u(source):
    result = unquote(source)
    if '%u' in result:
        result = result.replace('%u','\\u').decode('unicode_escape')
    return result
