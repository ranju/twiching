"""
A simple commenting application

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

class Comments(models.Model):
    """
    Table where comments will be stored
    """
    user = models.ForeignKey(User)
    comment = models.TextField(_('Comment'))
    like = models.IntegerField(_('Like'), default=0)
    created_on = models.DateTimeField(auto_now_add = True, null=True)
    updated_on = models.DateTimeField(auto_now=True)

class Flag(models.Model):
    """
    This table will have the comment flagged by other users
    """
    user = models.ForeignKey(User)
    comment = models.ForeignKey(Comments)
    flag = models.CharField(_('Flag'), max_length=250)
    flagged_on = models.DateTimeField(auto_now_add = True, null=True)