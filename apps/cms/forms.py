"""
Forms for contact us and other stuffs related to cms module

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""


from django import forms
from django.utils.translation import ugettext_lazy as _
from captcha.fields import ReCaptchaField
from twiching.settings import SUPPORT_TOPIC_CHOICES

# place form definition here
class ContactUsForm(forms.Form):

    full_name = forms.CharField(label=_('Name'), max_length=250, required=True)
    topic = forms.ChoiceField(choices=SUPPORT_TOPIC_CHOICES, label=_('Support topic'))
    email = forms.EmailField(label=_('Email'), max_length=75, required=True)
    subject = forms.CharField(label=_('Subject'), max_length=250, required=True)
    message = forms.CharField(label=_('Enter your message'), widget=forms.Textarea(attrs={'cols': '45', 'rows': '5'}),  required=True)
    attachment = forms.FileField(label=('Attachment'), required=False)
    captcha = ReCaptchaField()