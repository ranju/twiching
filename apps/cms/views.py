"""
View page for the cms application

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import Http404, HttpResponseRedirect
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
from twiching.apps.cms.models import Post
from twiching.apps.cms.forms import ContactUsForm
from twiching.settings import SUPPORT_EMAIL_LIST


def page(request, slug=None):
    """
    This action would display a page that is created via the admin. Super user have the ability to view even unpublished posts
    """
    if slug is None:
        raise Http404
    try:
        if request.user.is_superuser:
            pageData = Post.objects.get(slug=slug)
        else:
            pageData = Post.objects.get(slug=slug, publish = True)
    except Post.DoesNotExist:
        raise Http404

    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': str(pageData.title), 'url': '/' + str(pageData.slug)},
    ]

    templateVariables = {

        'pageData': pageData,
        'meta_tag': pageData.meta_tags,
        'meta_description': pageData.meta_content,
        'breadcrumb': breadcrumb,
    }

    return render_to_response('cms/post.html', templateVariables, context_instance=RequestContext(request))

def ContactUs(request):
    """
    This action would display the support form for the user to send any queries
    """

    if request.method == 'POST':
        form = ContactUsForm(request.POST)
        if form.is_valid():
            name = request.POST.get('full_name',None)
            from_email = '"'+name+'" <'+request.POST.get('email', None)+'>'
            topic = request.POST.get('topic', None)
            subject = "New "+str(topic)+" Message: "+request.POST.get('subject', None)
            message = request.POST.get('message', None)
            attachment = request.FILES.get('attachment', None)

            textEmail = get_template('emails/contact_us_email.txt')
            htmlEmail = get_template('emails/contact_us_email.html')

            variables = {
                'name': name,
                'message': message,
                'subject': request.POST.get('subject', None),
                'topic': topic,
                'email': request.POST.get('email', None),
            }

            variables = Context(variables)
            text_content = textEmail.render(variables)
            html_content = htmlEmail.render(variables)
            message = EmailMultiAlternatives(subject, text_content, from_email, SUPPORT_EMAIL_LIST)
            message.attach_alternative(html_content, "text/html")
            if attachment is not None:
                message.attach(attachment.name, attachment.read(), attachment.content_type)
            try:
                message.send()
            except Exception:
                messages.error(request,
                    _('Oops! Some error occurred while sending your message. Please try again later'))
            else:
                messages.success(request,
                    _('Thank You! Your message is send to our team successfully. They would respond to you shortly'))
            return HttpResponseRedirect('')
    else:
        if request.user.is_authenticated():
            initial = {
                'full_name' : request.user.first_name+' '+request.user.last_name,
                'email': request.user.email,
            }
            form = ContactUsForm(initial=initial)
        else:
            form = ContactUsForm()

    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Contact Us', 'url': '/contact'},
    ]

    templateVariables = {
        'form': form,
        'breadcrumb': breadcrumb,
    }

    return render_to_response('cms/contact_us.html', templateVariables, context_instance=RequestContext(request))
