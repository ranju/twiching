"""
This will enable the admin page for the new flatpage app

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""

from django.contrib import admin
from twiching.apps.cms.models import Post

class PostAdmin(admin.ModelAdmin):
    list_display = ('title','slug','publish',)
    search_fields = ('title',)
    list_filter = ('publish',)
    exclude = ('slug',)

admin.site.register(Post, PostAdmin)

