"""
A custom build simple cms as a replacement for django flatpage

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""

from django.db import models
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField
from django.template.defaultfilters import slugify
from twiching.apps.menu.models import Menu, MenuRegion

class Post(models.Model):
    title = models.CharField(_('Title'), max_length=250)
    slug = models.SlugField(_('Slug'), null = True, blank = True)
    summary = models.TextField(_('Summary'), null = False, blank = False)
    content = RichTextField(null = False, blank = False)
    meta_tags = models.CharField(_('Meta Tags'), max_length=250, null = True, blank = True)
    meta_content = models.TextField(_('Meta Description'), null = True, blank = True)
    publish = models.BooleanField(_('Publish'), default=1)
    created_on = models.DateTimeField(auto_now_add = True, null=True)
    updated_on = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)
