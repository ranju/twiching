"""
Search index implementation for store

@author: Aswathy Rajesh
@organization: Opentechnics
@contact: aswathi.r@opentechnics.com
@copyright: (c) 2012, Twiching
"""
import datetime
#from haystack import indexes
#from haystack import site
from twiching.apps.photography.models import Photos


#class PhotosIndex(indexes.SearchIndex, indexes.Indexable):
    #text = indexes.CharField(document=True, use_template=True)
    #title = indexes.CharField(model_attr='title')
    #desc = indexes.CharField(model_attr='description')
    #image = indexes.CharField(model_attr='image')
    #created_date = indexes.DateTimeField(model_attr='created_on')
    #CategId = indexes.IntegerField(model_attr='category_id')
    #tags = indexes.CharField(model_attr='tags')
    #username = indexes.CharField(model_attr='user__username')
    #def get_model(self):
        #return Photos

    #def index_queryset(self):
       # """Used when the entire index for model is updated."""
        #return Photos.objects.filter(created_on__lte=datetime.datetime.now())

#site.register(Photos, PhotosIndex)
