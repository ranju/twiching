"""
The view file for photography related functionality

@author: Swaroop Shankar V, Aswathi Rajesh
@organization: Opentechnics
@contact: swaroop@opentechnics.com, aswathi.r@opentechnics.com
@copyright: (c) 2011, Twiching
"""

# for HTTP response
from django.http import HttpResponse, Http404
# for os manipulations
import os
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.contrib import messages

from django.db.models import Q, Sum, Avg
from django.utils import simplejson
from django.contrib.contenttypes.models import ContentType
from djangoratings.views import AddRatingView
from djangoratings.models import Score
from endless_pagination.decorators import page_template
from ajaxutils.decorators import ajax
from registration.models import RegistrationProfile
from sorl.thumbnail.shortcuts import delete
from django.utils.translation import ugettext_lazy as _
from twiching.settings import MEDIA_ROOT, PROFILE_IMAGE_UPLOAD_PATH, DATE_FORMAT, ENABLED_GROUP_ID_MAPPING,\
    DEFAULT_CATEGORY, FILEUPLOAD_SETTINGS
from twiching.apps.photography.models import PhotoCategories, Photos,\
    PhotoComments, PhotoLikes, PhotoAbuse, PhotoAbuseReasons, PhotoRating
from twiching.apps.photography.forms import ImageUploadForm, ImageEditForm
from twiching.apps.comment.models import Comments
from twiching.apps.accounts.models import ExtraInformation
from twiching.apps.general.views import _sendNotification


@login_required(login_url='/accounts/login/')
def accountsPhotoList(request, selectedCategoryId=0):
    """
    This function would display the photography section in user
    account page. Enables the user to upload photographs
    """
    userId = request.user.id

    selectedCategoryId = int(selectedCategoryId)

    try:
        regProfile = RegistrationProfile.objects.get(user=request.user)
    except RegistrationProfile.DoesNotExist:
        messages.error(request, _('Error occurred while processing your request!'))
        return redirect('/accounts/dashboard/')
    if regProfile.activation_key == 'ALREADY_ACTIVATED' and not request.user.is_active:
        messages.error(request, _('You need to active your account before using any personal features of this site'))
        return redirect('/accounts/activate/')

    #Check if the current logged in user got the permission to access this page
    if not request.user.has_perm('photography.view_account_photo_list'):
        messages.error(request, _('You do not have permission to access that page!'))
        return redirect('/')

    #check if current logged in user got the ability to upload any files or not
    if not request.user.has_perm('photography.add_photos'):
        addPhotosPermission = False
    else:
        addPhotosPermission = True

    #check if current logged in user got the ability to delete a photo
    if not request.user.has_perm('photography.delete_photos'):
        deletePhotosPermission = False
    else:
        deletePhotosPermission = True

    if not request.user.has_perm('photography.change_photos'):
        editPhotosPermission = False
    else:
        editPhotosPermission = True

    #Get all the categories from the db
    photoCategories = PhotoCategories.objects.all()

    #get the images from db
    imageLists = None
    if not selectedCategoryId:
        imageLists = Photos.objects.filter(user=User(pk=userId)).order_by('-updated_on')
    else:
        imageLists = Photos.objects.filter(user=User(pk=userId),
            category=PhotoCategories(pk=selectedCategoryId)).order_by('-updated_on')

    openStatus = 0
    #   meaning user has triggered an upload action
    if request.method == 'POST':

        form = ImageUploadForm(request.POST, request.FILES[u'files[]'])
        file = request.FILES[u'files[]']

        photosObject = Photos()
        photosObject.user = User(id=request.user.id)
        photosObject.title = file.name
        photosObject.description = file.name
        photosObject.category = PhotoCategories(id=DEFAULT_CATEGORY)
        photosObject.tags = request.POST.get('tags', '')
        photosObject.private = request.POST.get('private', 0)
        photosObject.image = file
        try:
            photosObject.save()
        except Exception:
            response_data = {
                "error": "database save failed",
            }
        else:
            response_data = {
                "name": file.name,
                "size": file.size,
                "type": file.content_type
            }
        #messages.warning(request, _('Photo uploaded successfully. Photo displayed only after admins approval'))
        # generate the json data
        # the response data which will be returned to the uploader as json

        response_data['message'] = "Photo uploaded successfully. Photo displayed only after admins approval'"
        response_data = simplejson.dumps([response_data])
        #return HttpResponseRedirect('')
        return HttpResponse(response_data, mimetype="application/json")
        #else:
            #messages.error(request, _('Please correct following errors before continuing!'))
            #openStatus = 1  # This flag will keep the image upload div in open state
    else:
        form = ImageUploadForm()

    #Continue rest of the processing
    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': 'Account Dashboard', 'url': '/accounts/dashboard'},
            {'name': 'Manage Photographs', 'url': '/accounts/photography'},
    ]

    templateVariables = {
        'addPhotosPermission': addPhotosPermission,
        'deletePhotosPermission': deletePhotosPermission,
        'editPhotosPermission': editPhotosPermission,
        'breadcrumb': breadcrumb,
        'photoCategories': photoCategories,
        'form': form,
        'openStatus': openStatus,
        'imageLists': imageLists,
        'selectedCategory': int(selectedCategoryId),
        "open_tv": u'{{',
        "close_tv": u'}}',
        "maxfilesize": FILEUPLOAD_SETTINGS['MAX_FILESIZE'],
        "minfilesize": FILEUPLOAD_SETTINGS['MIN_FILESIZE'],
        }

    return render_to_response('photography/accounts_photo_list.html', templateVariables,
        context_instance=RequestContext(request))


@login_required(login_url='/accounts/login/')
def deletePhoto(request, photoId):
    """
    This function would delete the image based on the permissions provided
    """
    #Check if the current logged in user got the permission to access this page
    if not request.user.has_perm('photography.delete_photos'):
        messages.error(request, _('You do not have permission to access that page!'))
        return redirect('/accounts/photography/')

    #Let us check if the photo id is valid
    count = Photos.objects.filter(id=photoId).count()
    if not count:
        messages.error(request, _('The photograph you are trying to delete do now exist!'))
        return redirect('/accounts/photography/')

    #check if the current logged in user is the owner of the photograph
    userId = Photos.objects.get(pk=photoId).user_id
    if userId != request.user.id and request.user.is_superuser == False:
        messages.error(request, _('You do not have permission to delete that photograph!'))
        return redirect('/accounts/photography/')

    #if all above conditions are satisfied then let us get the image name, delete the image record from db, images from
    # the disk
    photoObject = Photos.objects.get(pk=photoId)
    #assert False
    image = photoObject.image

    try:
        photoObject.delete()
    except Exception:
        messages.error(request, _('Failed to delete the photograph. Please try again!'))
        return redirect('/accounts/photography/')
    else:
        #now remove the files from disk
        _deletePhotographs(image)

        messages.success(request, _('Image deleted successfully!'))
        return redirect('/accounts/photography/')


@login_required(login_url='/accounts/login/')
def editPhotoInformation(request, photoId=0):
    """
    This functionality is to edit information supplied for a photograph. The photo information will be loaded to a form
    based on the id supplied
    """
    #Check if the current logged in user got the permission to access this page
    if not request.user.has_perm('photography.change_photos'):
        messages.error(request, _('You do not have permission to access that page!'))
        return redirect('/accounts/photography/')

    #Let us check if the photo id is valid
    count = Photos.objects.filter(id=photoId).count()
    if not count:
        messages.error(request, _('The photograph information you are trying to edit do now exist!'))
        return redirect('/accounts/photography/')

    #check if the current logged in user is the owner of the photograph
    userId = Photos.objects.get(pk=photoId).user_id
    if userId != request.user.id and request.user.is_superuser == False:
        messages.error(request, _('You do not have permission to edit that photograph!'))
        return redirect('/accounts/photography/')

    closeColorbox = False

    #Load the form object
    if request.method == 'POST':
        form = ImageEditForm(request.POST)
        if form.is_valid():
            if int(photoId) > 0:
                photoObject = Photos.objects.get(pk=photoId)
                photoObject.title = request.POST.get('title')
                photoObject.description = request.POST.get('description')
                categoryId = request.POST.get('category', 0)
                if int(categoryId) > 0:
                    photoObject.category = PhotoCategories(id=categoryId)
                photoObject.private = request.POST.get('private')
                photoObject.tags = request.POST.get('tags')
                try:
                    photoObject.save()
                except Exception:
                    messages.error(request, _(
                        'Failed to update your from due to some internal error. Please try again! If the problem persist contact our customer support'))
                    closeColorbox = True
                else:
                    messages.success(request, _('Successfully updated photograph information!'))
                    closeColorbox = True
            else:
                messages.error(request, _('The photograph information you are trying to edit do now exist!'))
                closeColorbox = True
    else:
        #fetch the existing data from the database
        photoObject = Photos.objects.get(pk=photoId)
        initial = {
            'title': photoObject.title,
            'description': photoObject.description,
            'category': photoObject.category,
            'tags': photoObject.tags,
            'private': photoObject.private,
            }
        form = ImageEditForm(initial=initial)

    #Continue rest of the processing
    templateVariables = {
        'form': form,
        'photoId': photoId,
        'closeColorbox': closeColorbox,
        }

    return render_to_response('photography/accounts_photo_details_edit.html', templateVariables,
        context_instance=RequestContext(request))

@page_template("photography/photo_list_pagination.html")
def showAllPhotos(request, group_id=None, filter="featured", categoryId=0,template='photography/photo_list.html', extra_context=None):
    """
    This will display all the photos uploaded by users as a thumbnails
    """
    if group_id is None:
        group_id = 1
    else:
        group_id = int(group_id)

    try:
        groupObject = Group.objects.get(pk=group_id)
    except Group.DoesNotExist:
        raise Http404

    #if the categoryId is the id  set as DEFAULT_CATEGORY, raise error
    if DEFAULT_CATEGORY == categoryId:
        raise Http404

    groupName = groupObject.name.title()

    #Get all Categories
    categoryList = PhotoCategories.objects.all().filter(~Q(id=DEFAULT_CATEGORY))

    #Get the list of all the images public images uploaded by the users
    if filter == "latest":
        if int(categoryId) > 0:
            photoObjects = Photos.objects.all().filter(~Q(image=None), ~Q(category=DEFAULT_CATEGORY), private=0, approved=True, user__groups__id=group_id, user__is_active=1,
                category=PhotoCategories(pk=categoryId)).order_by('-created_on')
        else:
            photoObjects = Photos.objects.all().filter(~Q(image=None), ~Q(category=DEFAULT_CATEGORY), private=0, approved=True, user__groups__id=group_id,
                user__is_active=1).order_by('-created_on')
        title = "Latest " + groupName + ' Photos'
    elif filter == "popular":
        if int(categoryId) > 0:
            photoObjects = Photos.objects.all().filter(~Q(image=None), ~Q(category=DEFAULT_CATEGORY), private=0, approved=True, user__groups__id=group_id, user__is_active=1,
                category=PhotoCategories(pk=categoryId)).order_by('-popularity')
        else:
            photoObjects = Photos.objects.all().filter(~Q(image=None), ~Q(category=DEFAULT_CATEGORY), private=0, approved=True, user__groups__id=group_id,
                user__is_active=1).order_by('-popularity')
        title = "Most Popular " + groupName + ' Photos'
    elif filter == "views":
        if int(categoryId) > 0:
            photoObjects = Photos.objects.all().filter(~Q(image=None), ~Q(category=DEFAULT_CATEGORY), private=0, approved=True, user__groups__id=group_id, user__is_active=1,
                category=PhotoCategories(pk=categoryId)).order_by('-photorating__views')
        else:
            photoObjects = Photos.objects.all().filter(~Q(image=None), ~Q(category=DEFAULT_CATEGORY), private=0, approved=True, user__groups__id=group_id,
                user__is_active=1).order_by('-photorating__views')
        title = "Most Viewed " + groupName + ' Photos'
    elif filter == "featured":
        if int(categoryId) > 0:
            photoObjects = Photos.objects.all().filter(~Q(image=None), ~Q(category=DEFAULT_CATEGORY), private=0, approved=True, featured=True, user__groups__id=group_id,
                user__is_active=1, category=PhotoCategories(pk=categoryId)).order_by('-updated_on')
        else:
            photoObjects = Photos.objects.all().filter(~Q(image=None), ~Q(category=DEFAULT_CATEGORY), private=0, featured=True, approved=True, user__groups__id=group_id,
                user__is_active=1).order_by('-updated_on')
        title = "Featured " + groupName + ' Photos'
    else:
        if int(categoryId) > 0:
            photoObjects = Photos.objects.all().filter(~Q(image=None), ~Q(category=DEFAULT_CATEGORY), private=0, approved=True, user__groups__id=group_id, user__is_active=1,
                category=PhotoCategories(pk=categoryId)).order_by('-created_on')
        else:
            photoObjects = Photos.objects.all().filter(~Q(image=None), ~Q(category=DEFAULT_CATEGORY), private=0, approved=True, user__groups__id=group_id,
                user__is_active=1).order_by('-created_on')
        title = _("Latest Photographs")

    group_id_mapping = list()
    for key in ENABLED_GROUP_ID_MAPPING:
        value = ENABLED_GROUP_ID_MAPPING[key]

        if group_id == value:
            continue
        else:
            group_id_mapping.append(value)

    #Additional Data
    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': title, 'url': '/photograph/list/' + filter},
    ]

    templateVariables = {
        'breadcrumb': breadcrumb,
        'photoObjects': photoObjects,
        'filter': filter,
        'title': title,
        'categoryList': categoryList,
        'selectedCategory': int(categoryId),
        'group_id': int(group_id),
        'group_id_mapping': group_id_mapping,
        }

    if extra_context is not None:
        templateVariables.update(extra_context)
    return render_to_response(template, templateVariables, context_instance=RequestContext(request))

@page_template("photography/user_photo_list_pagination.html")
def userPhotoLists(request, user_name='', filter="latest",template='photography/user_photo_list.html', extra_context=None):
    """
    This function would display the photos uploaded by a specific user
    """

    if user_name == '':
        messages.error(request, _('User not found!'))
        return redirect('/')

    try:
        user = User.objects.get(username=str(user_name))
    except User.DoesNotExist:
        messages.error(request, _('User not found!'))
        return redirect('/')

    if not user.is_active:
        raise Http404

    if request.user.is_authenticated():
        current_user_id = request.user.id
    else:
        current_user_id = 0

    #check if the user got access to contact form
    if not request.user.has_perm('accounts.send_user_message'):
        show_contact = 0
    else:
        show_contact = 1

    #now get all the photographs of the user which are public
    if filter == "latest":
        photosData = Photos.objects.all().filter(user=user, private=0, approved=True).order_by('-created_on')
        title = _("Latest Photographs")
    elif filter == "popular":
        photosData = Photos.objects.all().filter(user=user, private=0, approved=True).order_by('-popularity')
        title = _("Most Popular Photographs")
    elif filter == "views":
        photosData = Photos.objects.all().filter(user=user, private=0, approved=True).order_by('-photorating__views')
        title = _("Most Viewed Photographs")
    else:
        photosData = Photos.objects.all().filter(user=user, private=0, approved=True).order_by('-created_on')
        title = _("Latest Photographs")

    #Additional Data
    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': user.first_name + ' ' + user.last_name, 'url': '/user/' + user.username},
            {'name': 'Photographs', 'url': '/user/' + str(user.username) + '/photographs/'},
    ]

    templateVariables = {
        'breadcrumb': breadcrumb,
        'photoData': photosData,
        'userData': user,
        'title': title,
        'filter': filter,
        'current_user_id': current_user_id,
        'user_name': user_name,
        'show_contact': show_contact,
        }

    if extra_context is not None:
        templateVariables.update(extra_context)
    return render_to_response(template, templateVariables,
        context_instance=RequestContext(request))


def showPhoto(request, user_name=None, photoId=0):
    """
    Display an individual photos. Will have functionality to like, comment on a photo
    """
    from twiching.library.imageManipulation import get_exif

    disableStarRating = False

    if user_name is None:
        messages.error(request, _('User not found!'))
        return redirect('/')
    else:
        #let us get the photo user id
        try:
            userObject = User.objects.get(username=user_name)
        except User.DoesNotExist:
            raise Http404

        if not userObject.is_active:
            raise Http404

    photoUserId = userObject.id

    if not int(photoId):
        messages.error(request, _('Photograph not found!'))
        return redirect('/user/' + user_name + '/photographs/')

    if request.user.is_authenticated():
        current_user_id = request.user.id
        authenticated = True
    else:
        current_user_id = 0
        authenticated = False

    #check if the user got access to contact form
    if not request.user.has_perm('accounts.send_user_message'):
        show_contact = 0
    else:
        show_contact = 1

    #Let us get the photograph
    try:
        photoObject = Photos.objects.get(pk=photoId, private=0, user=User(pk=photoUserId))
    except Photos.DoesNotExist:
        messages.error(request, _('Photograph not found!'))
        return redirect('/photographs')

    #get the ratings for the photograph
    try:
        photoRatingObject = PhotoRating.objects.get(photo=photoObject)
    except PhotoRating.DoesNotExist:
        photoRatingObject = PhotoRating()
        photoRatingObject.photo = photoObject
        views = 0
        if request.user.is_authenticated():
            if request.user.id != userObject.id:
                views = 1
        else:
            views = 1

        photoRatings = {
            'likes': 0,
            'dislikes': 0,
            'starRating': 0,
            'popularity': 0,
            'views': views,
            }
    else:
        photoRatings = {
            'likes': photoRatingObject.likes,
            'dislikes': photoRatingObject.dislikes,
            'starRating': photoRatingObject.star_rating.get_rating(),
            'popularity': photoObject.popularity,
            'views': photoRatingObject.views,
            }

    if request.user.is_authenticated():
        if request.user.id != userObject.id:
            photoRatingObject.views = int(photoRatingObject.views) + 1
            photoRatingObject.save()
    else:
        photoRatingObject.views = int(photoRatingObject.views) + 1
        photoRatingObject.save()

    #we need to figure out if the current logged in user have the permission to rate a photo
    if not authenticated:
        # if user is not authenticated
        disableStarRating = True
    elif photoUserId == current_user_id:
        # if the user is same as the owner of the photograph
        disableStarRating = True
    else:
        # if the user had already voted
        userRating = photoRatingObject.star_rating.get_rating_for_user(request.user, request.META['REMOTE_ADDR'])
        if userRating > 0:
            disableStarRating = True

    #Let us get similar photos based on category
    similarPhotosList = Photos.objects.filter(~Q(id=photoId), private=0,
        category=PhotoCategories(pk=photoObject.category_id)).values('id', 'user_id', 'image').order_by('?')[:6]
    photoList = list()
    for photos in similarPhotosList:
        userName = User.objects.get(id=photos['user_id']).username
        if photos['image'] is None:
            continue
        photoImagePath = MEDIA_ROOT + photos['image']
        if not os.path.exists(photoImagePath):
            continue
        photoList.append(
                {
                'id': photos['id'],
                'image': photos['image'],
                'username': userName,
                })
    similarPhotosList = photoList

    #Let us get the same user's other photographs
    userOtherPhotographs = Photos.objects.filter(~Q(id=int(photoId)),
        user=User(pk=User.objects.get(username=user_name).id)).values('id', 'user_id', 'image').order_by('?')[:6]
    photoList = list()
    for photos in userOtherPhotographs:
        userName = user_name
        if photos['image'] is None:
            continue
        photoImagePath = MEDIA_ROOT + photos['image']
        if not os.path.exists(photoImagePath):
            continue
        photoList.append(
                {
                'id': photos['id'],
                'image': photos['image'],
                'username': userName,
                })
    userOtherPhotographs = photoList

    #let us get all the comments for this photo and pass it to the view function
    commentData = PhotoComments.objects.filter(photo=Photos(pk=photoId)).values('comment_id')
    commentsId = list()
    for commentId in commentData:
        commentsId.insert(len(commentsId), commentId['comment_id'])

    #now let us get all the comments
    comments = Comments.objects.filter(id__in=commentsId)

    #get the exif information of the image
    imagePath = MEDIA_ROOT + str(photoObject.image)
    try:
        exifInformation = get_exif(imagePath)
    except IOError:
        messages.error(request, _('Photograph not found!'))
        return redirect('/user/' + user_name + '/photographs/')

    focalLength = exifInformation.get('Exif.Photo.FocalLength', None)
    if focalLength is not None:
        focalLength = str(focalLength)
        focalLength = focalLength.split('/')
        focalLength = int(focalLength[0]) / int(focalLength[1])
        focalLength = str(focalLength) + ' mm'
    else:
        focalLength = 'Not Available'

    aperture = exifInformation.get('Exif.Photo.FNumber', None)
    if aperture is not None:
        aperture = str(aperture)
        aperture = aperture.split('/')
        aperture = int(aperture[0]) / int(aperture[1])
        aperture = 'F' + str(aperture)
    else:
        aperture = 'Not Available'

    exifInfo = {
        'Created': exifInformation.get('Exif.Image.DateTime', 'Not Available'),
        'Camera': exifInformation.get('Exif.Image.Model', 'Not Available'),
        'FocalLength': focalLength,
        'ShutterSpeed': exifInformation.get('Exif.Photo.ExposureTime', 'Not Available'),
        'Aperture': aperture,
        'ISOSpeed': exifInformation.get('Exif.Photo.ISOSpeedRatings', 'Not Available'),
        'Category': photoObject.category.name,
        'Copyright': photoObject.user.first_name + ' ' + photoObject.user.last_name,
        'Uploaded': photoObject.created_on,
        }

    abuseReasons = PhotoAbuseReasons.objects.all()

    #Additional Data
    breadcrumb = [
            {'name': 'Home', 'url': '/'},
            {'name': photoObject.user.first_name + ' ' + photoObject.user.last_name,
             'url': '/user/' + photoObject.user.username},
            {'name': 'Photographs', 'url': '/user/' + str(photoObject.user.username) + '/photographs/'},
            {'name': photoObject.title, 'url': '/photograph/' + str(photoObject.id)},
    ]

    templateVariables = {
        'breadcrumb': breadcrumb,
        'photoData': photoObject,
        'similarPhotosList': similarPhotosList,
        'similarPhotosListCount': len(similarPhotosList),
        'photos': Photos.objects.latest('id'),
        'authenticated': authenticated,
        'comments': comments,
        'PROFILE_IMAGE_UPLOAD_PATH': PROFILE_IMAGE_UPLOAD_PATH,
        'exifInfo': exifInfo,
        'DATE_FORMAT': DATE_FORMAT,
        'current_user_id': current_user_id,
        'show_contact': show_contact,
        'otherPhotos': userOtherPhotographs,
        'user_name': user_name,
        'disableStarRating': disableStarRating,
        'photoRatings': photoRatings,
        'abuseReasons': abuseReasons,
        }

    return render_to_response('photography/view_photo.html', templateVariables,
        context_instance=RequestContext(request))


@login_required(login_url='/accounts/login/')
def saveComments(request):
    """
    This will be an ajax request to save the comments
    """
    global comments
    commentText = request.POST.get('comment', '')
    photoId = request.POST.get('photoid', '')
    commentId = request.POST.get('commentid', '')

    if commentText != '':
        #check commentd id, if present, update the comment else add new comment
        if commentId:
            #return HttpResponse("innnnn")
            commentObject = Comments.objects.get(pk=commentId)
            commentObject.user = User(pk=request.user.id)
            commentObject.comment = commentText
            commentObject.save()
        else:
            #return HttpResponse("ellll")
            #first save the comment
            commentObject = Comments()
            commentObject.user = User(pk=request.user.id)
            commentObject.comment = commentText
            commentObject.save()

        #now save the comments id and the photo id
        photoObject = Photos.objects.get(pk=photoId)
        photoCommentObject = PhotoComments()
        photoCommentObject.comment = Comments(pk=commentObject.id)
        photoCommentObject.photo = photoObject

        try:
            photoCommentObject.save()
        except Exception:
            #if failed then let us delete the comments
            c = Comments.objects.get(pk=commentObject.id)
            c.delete()
            return render_to_response('common/ajax.html')
        else:
            #let us get all the comments for this photo and pass it to the view funtion
            commentData = PhotoComments.objects.filter(photo=Photos(pk=photoId)).values('comment_id')
            commentsId = list()
            for commentId in commentData:
                commentsId.insert(len(commentsId), commentId['comment_id'])

            #now let us get all the comments
            comments = Comments.objects.filter(id__in=commentsId)

            if not request.POST.get('commentid', ''):
                if request.user.id != photoObject.user.id:
                    #send an email to the photo owner
                    commenter_name = request.user.first_name + ' ' + request.user.last_name
                    commenter_profile_link = 'http://' + request.META['HTTP_HOST'] + '/user/' + request.user.username
                    photo_name = photoObject.title
                    photo_url = 'http://' + request.META['HTTP_HOST'] + '/user/' + photoObject.user.username + '/photo/' + str(photoObject.id)
                    photo_comment = commentText
                    photo_owner_name = photoObject.user.first_name + ' ' + photoObject.user.last_name

                    subject = commenter_name + ' commented your photograph in Twiching.com'

                    to_email = photoObject.user.email
                    http_host = request.META['HTTP_HOST']
                    _sendNotification(
                        subject=subject,
                        content=photo_comment,
                        to_email=to_email,
                        http_host=http_host,
                        commenter_name=commenter_name,
                        commenter_profile_link=commenter_profile_link,
                        photo_name=photo_name,
                        photo_url=photo_url,
                        photo_owner_name=photo_owner_name
                    )

    templateVariables = {
        'value': request.POST.get('comment'),
        'comments': comments,
        'PROFILE_IMAGE_UPLOAD_PATH': PROFILE_IMAGE_UPLOAD_PATH,
        'photoid': photoId,
        }
    return render_to_response('photography/comment_list.html', templateVariables,
        context_instance=RequestContext(request))


def feedback(request):
    """
    This would save the like and dislike voted for a photo
    """

    #check if the user is authenticated or not
    if request.user.is_authenticated():
        photoId = request.POST.get('photoid', '')
        type = request.POST.get('type', '')
        userId = request.user.id

        if photoId == '':
            return HttpResponse('failed')

        try:
            photoRatingObject = PhotoRating.objects.get(photo=Photos(pk=int(photoId)))
            photoUserId = photoRatingObject.photo.user.id
        except PhotoRating.DoesNotExist:
            photoRatingObject = PhotoRating()
            photoObject = Photos.objects.get(pk=int(photoId))
            photoUserId = photoObject.user.id

            photoRatingObject.photo = photoObject

        if photoUserId == userId:
            return HttpResponse('same_user')

        #check if user and photo id combination exist
        photoLikeCount = PhotoLikes.objects.all().filter(user=User(pk=userId), photo=Photos(pk=photoId)).count()
        if not photoLikeCount:
            #If the record is not there then let us create a new entry
            photoLikeObject = PhotoLikes()
            photoLikeObject.user = User(pk=userId)
            photoLikeObject.photo = Photos(pk=photoId)
            if type == 'like':
                photoLikeObject.likes = True
                photoLikeObject.dislikes = False
            elif type == 'dislike':
                photoLikeObject.likes = False
                photoLikeObject.dislikes = True
            photoLikeObject.save()

            #let us update the photo tables like and dislike count
            if type == 'like':
                photoRatingObject.likes += 1

		like_User = Photos.objects.get(pk=photoId).user
                liker_name = request.user.first_name + ' ' + request.user.last_name
                photo_url = 'http://' + request.META['HTTP_HOST'] + '/user/' + like_User.username+'/photo/'+photoId+'/'
                commenter_profile_link = 'http://' + request.META['HTTP_HOST'] + '/user/' + request.user.username
                subject = liker_name + ' likes your photo at Twiching.com'
                photo_name = Photos.objects.get(pk=photoId).title

                to_email = like_User.email
                http_host = request.META['HTTP_HOST']
                likeUser_name = like_User.first_name + ' ' + like_User.last_name
                email_template = 'user_photo_like'
                email_sent_status = _sendNotification(
                    subject=subject,
                    to_email=to_email,
                    http_host=http_host,
                    commenter_name=liker_name,
                    commenter_profile_link=commenter_profile_link,
                    photo_url=photo_url,
                    photo_owner_name = likeUser_name,
                    photo_name = photo_name,
                    email_template = email_template
                )
            elif type == 'dislike':
                photoRatingObject.dislikes += 1
            photoRatingObject.total_votes += 1

            photo_like_rating = int(photoRatingObject.likes / photoRatingObject.total_votes)
            photoRatingObject.vote_rating = photo_like_rating

            #let us update the records
            photoRatingObject.save()

            #let us save this information into user table
            try:
                userExtraInfoObject = ExtraInformation.objects.get(user=photoRatingObject.photo.user)
                if type == 'like':
                    userExtraInfoObject.total_likes += 1
                elif type == 'dislike':
                    userExtraInfoObject.total_dislikes += 1
            except ExtraInformation.DoesNotExist:
                userExtraInfoObject = ExtraInformation()
                userExtraInfoObject.user = User(pk=userId)
                if type == 'like':
                    userExtraInfoObject.total_likes = 1
                    userExtraInfoObject.total_dislikes = 0
                elif type == 'dislike':
                    userExtraInfoObject.total_dislikes = 1
                    userExtraInfoObject.total_likes = 0
            userExtraInfoObject.save()
            return HttpResponse('success')
        else:
            return HttpResponse('already_voted')
    else:
        messages.error(request, _('You need to login to vote any photos!'))
        return HttpResponse('auth_failed')


def savePhotoRating(request):

    photoId = request.POST.get('idBox', 0)
    if not photoId:
        result = {
            'status': 'photo_id_missing'
        }
        return HttpResponse(simplejson.dumps(result), mimetype='application/json')
    starRating = request.POST.get('rate', 0)

    #get the photo rating record
    try:
        photoRatingObject = PhotoRating.objects.get(photo=Photos(pk=int(photoId)))
    except PhotoRating.DoesNotExist:
        photoRatingObject = PhotoRating()
        photoRatingObject.photo = Photos(pk=int(photoId))
        photoRatingObject.save()
        object_id = photoRatingObject.id
    else:
        object_id = photoRatingObject.id

    content_type_id = ContentType.objects.get_for_model(PhotoRating).id
    params = {
        'content_type_id': content_type_id,
        'object_id': object_id,
        'field_name': 'star_rating',  # this should match the field name defined in your model
        'score': starRating,  # the score value they're sending
    }
    response = AddRatingView()(request, **params)

    if response.status_code == 200:
        status = 'success'
    elif response.status_code == 403:
        status = response.content

    result = {
        'status': status
    }
    return HttpResponse(simplejson.dumps(result), mimetype='application/json')


@ajax(login_required=False, require_POST=True)
def reportAbuse(request):
    """
    This is an ajax action which would register any photo abuse reports
    """
    abuse_type = request.POST.get('abuse_type', None)
    if abuse_type is None:
        return {
            'status': 'error',
            'error': 'abuse_type_missing',
            }
    photo_id = request.POST.get('photo_id', None)
    if photo_id is None:
        return {
            'status': 'error',
            'error': 'photo_missing',
            }
    user = request.user

    try:
        PhotoAbuseReasonsObject = PhotoAbuseReasons(pk=int(abuse_type))
    except PhotoAbuseReasons.DoesNotExist:
        return {
            'status': 'error',
            'error': 'photo_abuse_reason_not_found',
            }

    try:
        photoObject = Photos(pk=int(photo_id))
    except PhotoAbuseReasons.DoesNotExist:
        return {
            'status': 'error',
            'error': 'photo_not_found',
            }

    abuseObject = PhotoAbuse()
    abuseObject.user = user
    abuseObject.reason = PhotoAbuseReasonsObject
    abuseObject.photo = photoObject
    try:
        abuseObject.save()
    except Exception:
        return {
            'status': 'error',
            'error': 'database_save_error',
            }
    else:
        photoObject = Photos.objects.get(pk=int(photo_id))
        photoObject.report_abuse += 1
        photoObject.save()
        return {
            'status': 'success',
            }


def _deletePhotographs(imageName):
    """
    This function would delete all the photographs that uploaded by the user based on the imageName passed
    @param imageName: The name of the image that needs to be deleted
    @access private
    """
    if not delete(imageName):
        # if the generated thumbnails failed to get deleted then do something
        #TODO: If thumbnails are not deleted then need to decide what needs to be done. Currently it just gets passed
        pass

    try:
        os.unlink(MEDIA_ROOT + imageName.name)
    except OSError:
        pass

    return True


def photoPopularity(request):
    """
    This is a private function which would calculate and update a photo popularity based on its star rating and like/dislikes percentage
    """

    # let us get all the photos which are not set to private
    photoRatingList = PhotoRating.objects.filter(photo__private=0)

    totalVotesAverage = PhotoRating.objects.exclude(total_votes=0).aggregate(Avg('total_votes'))

    total_all_likes = PhotoRating.objects.aggregate(Sum('likes'))
    total_all_dislikes = PhotoRating.objects.aggregate(Sum('dislikes'))
    avg_vote_rating = total_all_likes['likes__sum'] / (total_all_likes['likes__sum'] + total_all_dislikes['dislikes__sum'])

    content_type = ContentType.objects.get_for_model(PhotoRating)
    #get the whole star rating
    avg_num_star_votes = Score.objects.filter(content_type=content_type).aggregate(Avg('votes'))
    avg_num_star_votes = avg_num_star_votes['votes__avg']
    avg_star_rating = Score.objects.filter(content_type=content_type).aggregate(Avg('score'))
    avg_star_rating = avg_star_rating['score__avg']
    for rating in photoRatingList:

        #calculate popularity with votes
        if rating.likes:
            no_of_votes = rating.total_votes
            this_rating = rating.likes / no_of_votes
            totalVotesPopularity = ((totalVotesAverage['total_votes__avg'] * avg_vote_rating) + (no_of_votes * this_rating))\
            / (totalVotesAverage['total_votes__avg'] + no_of_votes)
        else:
            totalVotesPopularity = 0

        this_num_star_votes = rating.star_rating_votes
        this_star_rating = rating.star_rating_score
        try:
            starPopularityRating = ((avg_num_star_votes * avg_star_rating) + (this_num_star_votes * this_star_rating)) / (avg_num_star_votes + this_num_star_votes)
        except Exception:
            starPopularityRating = 0

        #get the photo aging

        popular = int(round(starPopularityRating, 2) * 10 + round(totalVotesPopularity, 2) * 100) / 2
        if popular > 100:
            popular = 100
        rating.photo.popularity = popular

        try:
            rating.photo.save()
        except Exception:
            pass

    return HttpResponse('Done')


def deleteComment(request):
    """
    This is for deleting a comment
    """

    #check if the user is authenticated or not
    if request.user.is_authenticated():
        commentId = request.POST.get('commentId', '')

        photoId = request.POST.get('photoId', '')

        if commentId == '':
            return HttpResponse('failed')
            #photoObject.delete()
        try:
            c = Comments.objects.get(pk=commentId)
        except c.DoesNotExist:
            return HttpResponse('failed')
        else:
            c.delete()
            #return HttpResponse('success')

        #let us get all the comments for this photo and pass it to the view funtion
        commentData = PhotoComments.objects.filter(photo=Photos(pk=photoId)).values('comment_id')
        commentsId = list()
        for commentId in commentData:
            commentsId.insert(len(commentsId), commentId['comment_id'])

        #now let us get all the comments
        comments = Comments.objects.filter(id__in=commentsId)
        templateVariables = {
            'value': request.POST.get('comment'),
            'comments': comments,
            'PROFILE_IMAGE_UPLOAD_PATH': PROFILE_IMAGE_UPLOAD_PATH,
            'photoid': photoId,
            }
        return render_to_response('photography/comment_list.html', templateVariables,
            context_instance=RequestContext(request))

    else:
        messages.error(request, _('You need to login to delete comments!'))
        return HttpResponse('auth_failed')
