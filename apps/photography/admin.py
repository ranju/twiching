"""
Admin related code for accounts application goes here

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

from django.contrib import admin
from twiching.apps.photography.models import PhotoCategories, Photos, PhotoOfTheMonth,\
    PhotoAbuseReasons, PhotoAbuse, PhotoRating
from twiching.apps.photography.views import _deletePhotographs
from sorl.thumbnail import default



class PhotoOfTheMonthInline(admin.StackedInline):
    model = PhotoOfTheMonth


class PhotoRatingsInline(admin.StackedInline):
    model = PhotoRating
    readonly_fields = ('views', 'likes', 'dislikes',)


class PhotosAdmin(admin.ModelAdmin):
    """
    Photos admin section
    """
    list_display = ('title', 'photograph', 'user', 'category', 'featured', 'popularity', 'report_abuse','approved','approved_mail','featured_mail')
    search_fields = ('title',)
    list_editable = ( 'featured','approved',)
    list_filter = ('category', 'featured','approved')
    exclude = ('image', )
    readonly_fields = ('user', 'report_abuse', 'popularity',)
    fieldsets = (
        ('Photograph Details', {
            'fields': ('user', 'title', 'description', 'category', 'tags', 'private', 'featured', 'report_abuse','approved','approved_mail','featured_mail')
        }),
        )
    inlines = [PhotoOfTheMonthInline, PhotoRatingsInline]

    def photograph(self, obj):
        if obj.image:
            try :
                thumb = default.backend.get_thumbnail(obj.image.file, "86x116")
                return u'<img  src="%s" />' % (thumb.url)
            except :
                return  "No Image"
        else:
            return "No Image"
    photograph.short_description = 'My Thumbnail'
    photograph.allow_tags = True



    def has_add_permission(self, request):
        """
        We are removing the add photograph feature for admin users since its unnecessary
        """
        return False

    def delete_model(self, request, obj):
        """
        On deletion of an photograph record we need to delete the images that's stored in the drive too
        """
        imageName = obj.image
        _deletePhotographs(imageName)


class PhotoAbuseAdmin(admin.ModelAdmin):
    list_display = ('user', 'photograph', 'reason', 'created_on',)
    list_filter = ('photo',)
    readonly_fields = ('user', 'photo', 'reason',)

    def has_add_permission(self, request):
        """
        We are removing the add abuse feature for admin users since its unnecessary
        """
        return False

admin.site.register(PhotoCategories)
admin.site.register(Photos, PhotosAdmin)
admin.site.register(PhotoAbuseReasons)
admin.site.register(PhotoAbuse, PhotoAbuseAdmin)
