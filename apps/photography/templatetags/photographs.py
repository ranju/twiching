"""
This script would define template tags which would display blocks which are related to photography

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2012, Twiching
"""
__author__ = 'Swaroop Shankar V <swaroop@opentechnics.com>'

import random
from django.template import Library
from django.db.models import Q
from twiching.apps.photography.models import Photos, PhotoOfTheMonth
from twiching.settings import DATE_FORMAT, STATIC_URL, ENABLED_GROUP_ID_MAPPING,DEFAULT_CATEGORY

register = Library()

@register.inclusion_tag('photography/custom_tag_featured_photographs.html')
def get_photos(group_id = None, type = None):

    type_list = ['featured', 'popular', 'latest', 'views']
    if group_id is not None and type is not None:
        group_id = int(group_id)
        type = str(type)
    elif group_id is not None and type is None:
        #this case the group id is the one that needs to be excluded rather than included
        newGroupList = list()
        for value in ENABLED_GROUP_ID_MAPPING:
            if value == group_id:
                continue
            else:
                newGroupList.append(value)
        group_id = int(ENABLED_GROUP_ID_MAPPING[random.choice(newGroupList)])
        type = random.choice(type_list)
    else:
        #get a random group id and a type
        group_id = int(ENABLED_GROUP_ID_MAPPING[random.choice(ENABLED_GROUP_ID_MAPPING.keys())])
        type = random.choice(type_list)

    photoList = None
    try:
        if type == "featured":
            title = 'Featured'
            photoList = Photos.objects.filter(~Q(image=None),~Q(category=DEFAULT_CATEGORY), featured=True, private=False,approved=True, user__groups__id=group_id).order_by('?')[:4]
        elif type == 'popular':
            title = 'Popular'
            photoList = Photos.objects.filter(~Q(image=None),~Q(category=DEFAULT_CATEGORY), private=False,approved=True, user__groups__id=group_id).order_by('-popularity')[:4]
        elif type == 'latest':
            title = 'Latest'
            photoList = Photos.objects.filter(~Q(image=None),~Q(category=DEFAULT_CATEGORY), private=False,approved=True, user__groups__id=group_id).order_by('-created_on')[:4]
        elif type == 'views':
            title = 'Most Viewed'
            photoList = Photos.objects.filter(~Q(image=None),~Q(category=DEFAULT_CATEGORY), private=False,approved=True, user__groups__id=group_id).order_by('-photorating__views')[:4]
    except Photos.DoesNotExist:
        photoList = None

    if group_id == ENABLED_GROUP_ID_MAPPING['PHOTOGRAPHER']:
        title += ' Photographer Photos'
    elif group_id == ENABLED_GROUP_ID_MAPPING['MODEL']:
        title += ' Model Photos'
#    elif group_id == ENABLED_GROUP_ID_MAPPING['MAKE_UP_ARTIST']:
#        title += ' Make Up Artist Photos'
    elif group_id == ENABLED_GROUP_ID_MAPPING['FASHION_DESIGNER']:
#        title += ' Designer Photos'
        group_id = ENABLED_GROUP_ID_MAPPING['PHOTOGRAPHER']
        title += ' Photographer Photos'
    return {
        'photographs': photoList,
        'STATIC_URL': STATIC_URL,
        'title': title,
        'group_id': str(group_id),
        'type': type,
        }

@register.inclusion_tag('photography/custom_tag_photo_of_the_month.html', takes_context=True)
def photo_of_the_month(context):
    from datetime import datetime
    from calendar import month_abbr
    currentYear = datetime.now().year
    currentMonth = datetime.now().month
    try:
        photo = PhotoOfTheMonth.objects.get(year=currentYear, month=currentMonth)
    except PhotoOfTheMonth.DoesNotExist:
        photo = None

    return {
        'month': month_abbr[int(currentMonth)],
        'year': int(currentYear),
        'photo': photo,
        'DATE_FORMAT': DATE_FORMAT,
    }
