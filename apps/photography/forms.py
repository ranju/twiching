"""
Forms for photography application

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

from django import forms
from django.utils.translation import ugettext_lazy as _
from twiching.apps.photography.models import PhotoCategories
from tagging_autocomplete.widgets import TagAutocomplete
from tagging.forms import TagField

class ImageUploadForm(forms.Form):
    """
    This class would define the fields for the image upload form in the photography section
    """
    #title = forms.CharField(label=_('Title'), max_length=30, required=True)
    #description = forms.CharField(label=_('Description'), widget=forms.Textarea(attrs={'cols': '5', 'rows': '3'}),  required=False)
    #category = forms.ModelChoiceField(queryset=PhotoCategories.objects.all(), label=_("Category"), required=True)
    #tags = TagField(widget=TagAutocomplete(), label=_('Tags'),required=False)
    #private = forms.BooleanField(label=_('Make this photo private?'), required=False)
    image = forms.ImageField(label=_('Image'), required=True)

class ImageEditForm(forms.Form):
    """
    This class would define the fields for the image upload form in the photography section
    """
    title = forms.CharField(label=_('Title'), max_length=30, required=True)
    description = forms.CharField(label=_('Description'), widget=forms.Textarea(attrs={'cols': '19', 'rows': '4'}),  required=False)
    category = forms.ModelChoiceField(queryset=PhotoCategories.objects.all(), label=_("Category"), required=True)
    tags = forms.CharField(label=_('Tags'), max_length=255, required=False)
    private = forms.BooleanField(label=_('Make this photo private?'), required=False)