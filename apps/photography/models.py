"""
Here we have the database related code for the photography related application

@author: Swaroop Shankar V
@organization: Opentechnics
@contact: swaroop@opentechnics.com
@copyright: (c) 2011, Twiching
"""

import os
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from tagging.fields import TagField
from twiching.apps.comment.models import Comments
from twiching.settings import MEDIA_URL
from djangoratings.fields import RatingField
from django.db.models import get_model
from twiching.apps.general.views import _sendNotification
from django.contrib.sites.models import Site

site = Site.objects.get(pk=1)

def get_image_path(instance, filename):
    """
    This will rename the uploaded file based on a random generated string based on the uploaded time and filename etc
    """
    import time
    import hashlib
    from twiching.settings import PHOTOGRAPH_ORIGINAL_UPLOAD_PATH

    randomString = hashlib.md5(filename).hexdigest() + str(int(time.time()))
    filename = str(randomString) + filename
    return os.path.join(PHOTOGRAPH_ORIGINAL_UPLOAD_PATH, filename)


# Create your models here.
class PhotoCategories(models.Model):
    """
    This model will have the category list into which all the photos will be grouped
    """
    name = models.CharField(_('Name'), max_length=250)
    description = models.TextField(_('Description'), null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class Photos(models.Model):
    """
    This model will enable the users to upload photos
    """
    user = models.ForeignKey(User)
    title = models.CharField(_('Title'), max_length=250, null=True, blank=True)
    description = models.TextField(_('Description'), null=True, blank=True)
    category = models.ForeignKey(PhotoCategories, null=True, blank=True)
    image = models.ImageField(_('Image'), upload_to=get_image_path)
    tags = TagField(null=True, blank=True)
    private = models.BooleanField(_('Private'), default=0)
    approved = models.BooleanField(_('Approved'), default=0)  # If admin have not approved then set 0 (Default) else 1.
    featured = models.BooleanField(_('Featured'), default=0)
    report_abuse = models.IntegerField(_('Abuse Reported'), default=0)
    popularity = models.IntegerField(_('Popularity'), default=0)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    approved_mail = models.BooleanField(_('Approved Mail Sent'), default=0)
    featured_mail = models.BooleanField(_('Featured Mail Sent'), default=0)

    def save(self, *args, **kwargs):
        """
        We need to check if there is already a default album image assigned, if yes then remove that and set the new
        image as the album image
        """
        admin_approve = self.approved
        private = self.private
        categoryId = self.category.id
        userId = self.user.id
        username = self.user.username
        fname = self.user.first_name
        lname = self.user.last_name

        fullName = fname + ' ' + lname

        try:
            super(Photos, self).save()
            if admin_approve and not self.approved_mail:
                photo_name = self.title
                photo_url = 'http://' + site.domain + '/user/' + self.user.username + '/photo/' + str(self.id)
                photo_owner_name = self.user.first_name+" "+self.user.last_name
                subject = ' Your photograph has been approved in Twiching.com'
                email_template = "admin_approve"
                to_email = self.user.email
                http_host = site.domain
                self.approved_mail = True
                _sendNotification(
                    subject=subject,
                    to_email=to_email,
                    http_host=http_host,
                    photo_name=photo_name,
                    photo_url=photo_url,
                    photo_owner_name=photo_owner_name,
                    email_template=email_template
                )

            if self.featured and not self.featured_mail:
                photo_name = self.title
                photo_url = 'http://' + site.domain + '/user/' + self.user.username + '/photo/' + str(self.id)
                photo_owner_name = self.user.first_name+" "+self.user.last_name
                subject = ' Admin selected your photograph as featured in Twiching.com'
                email_template = "admin_featured"
                to_email = self.user.email
                http_host = site.domain
                self.featured_mail = True
                _sendNotification(
                    subject=subject,
                    to_email=to_email,
                    http_host=http_host,
                    photo_name=photo_name,
                    photo_url=photo_url,
                    photo_owner_name=photo_owner_name,
                    email_template=email_template
                )
            super(Photos, self).save()
        except Exception:
            pass
        else:

	    

            #if not private image and admin approved, then add this to the wall
            if admin_approve and not private:
            #Story for the current entry
	        
                if not categoryId:
                    story = '<a href="/user/' + username + '">' + fullName + '</a> has uploaded new photos'
                else:
                    categoryObject = PhotoCategories.objects.get(pk=categoryId)
                    story = '<a href="/user/' + username + '">' + fullName + '</a> has uploaded new photo(s) in ' + categoryObject.name + ' Category'

                #Let us check if there is any existing NewsFeedStory
                wall_model = get_model('wall', 'newsfeedstory')
                feedData = None
                try:
                    feedData = wall_model.objects.get(user=User(pk=userId), category=PhotoCategories(pk=categoryId))
                except wall_model.DoesNotExist:
                    #If no such entry then let us create one
                    if feedData is None:
                        newsFeedStoryObject = wall_model(
                            user=User(pk=userId),
                            category=PhotoCategories(pk=categoryId),
                            story=story
                        )
                        newsFeedStoryObject.save()
                else:
                    #if such entry is there then let us update the existing one
                    newsFeedStoryObject = wall_model.objects.get(pk=feedData.id)
                    newsFeedStoryObject.story = story
                    newsFeedStoryObject.save()

    def __unicode__(self):
        return self.title

    def photograph(self):
        return '<img height="86px" width="116px" src="' + MEDIA_URL + '%s" />' % self.image

    photograph.allow_tags = True

    def abuse(self):
        return '<a />'

    class Meta:
        permissions = (
            ("view_account_photo_list", "Manage self uploaded photographs"),
            )


class PhotoRating(models.Model):
    """
    This model would store all sorts of rating available for a photo
    """
    photo = models.OneToOneField(Photos)
    views = models.IntegerField(_('Views'), default=0)
    likes = models.IntegerField(_('Likes'), default=0)
    dislikes = models.IntegerField(_('Dislike'), default=0)
    vote_rating = models.IntegerField(_('Vote Rating'), default=0)  # will store the rating of all the likes and dislikes received for the current photo
    total_votes = models.IntegerField(_('Total Votes'), default=0)  # will store the total votes received for the photo
    star_rating = RatingField(range=5)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)


class PhotoComments(models.Model):
    """
    This model will have the photo comment table links
    """
    photo = models.ForeignKey(Photos)
    comment = models.ForeignKey(Comments)


class PhotoLikes(models.Model):
    """
    All the likes and dislikes received for the photos are stored here
    """
    photo = models.ForeignKey(Photos)
    user = models.ForeignKey(User)
    likes = models.NullBooleanField(_('Likes'), blank=True, null=True)
    dislikes = models.NullBooleanField(_('Dislike'), blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)


class PhotoOfTheMonth(models.Model):
    """
    An admin can set a photo of the month
    """
    from datetime import datetime

    monthChoice = (
        (1, 'January'),
        (2, 'February'),
        (3, 'March'),
        (4, 'April'),
        (5, 'May'),
        (6, 'June'),
        (7, 'July'),
        (8, 'August'),
        (9, 'September'),
        (10, 'October'),
        (11, 'November'),
        (12, 'December'),
        )

    currentYear = datetime.now().year
    yearChoice = (
        (currentYear, currentYear),
        (currentYear + 1, currentYear + 1),
        (currentYear + 2, currentYear + 2),
        (currentYear + 3, currentYear + 3),
        )

    photo = models.ForeignKey(Photos)
    month = models.IntegerField(_('Month'), max_length=2, choices=monthChoice)
    year = models.IntegerField(_('Year'), max_length=4, choices=yearChoice)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)


class PhotoAbuseReasons(models.Model):
    """
    The reason list for photo abuse
    """
    type = models.CharField(_('Title'), max_length=250, null=True, blank=True)

    def __unicode__(self):
        return self.type


class PhotoAbuse(models.Model):
    """
    This model would records all the photo abuse that user reports
    """
    photo = models.ForeignKey(Photos)
    user = models.ForeignKey(User)
    reason = models.ForeignKey(PhotoAbuseReasons)
    created_on = models.DateTimeField(_('Reported Date'), auto_now_add=True)

    def photograph(self):
        return '<a href="/admin/photography/photos/' + str(
            self.photo.id) + '/"><img height="86px" width="116px" src="' + MEDIA_URL + '%s" /></a>' % self.photo.image

    photograph.allow_tags = True
