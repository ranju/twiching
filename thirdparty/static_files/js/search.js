jQuery(document).ready(function () {
    jQuery( '#frm_search' ).bind('keypress', function(e){
        if ( e.keyCode == 13 ) {
            // $( this ).find( 'input[type=submit]:first' ).click();
            searchText = encodeURIComponent(trim(document.getElementById('search_text_box').value));
            var newtext1= searchText.replace(/^\s+/,'').replace(/\s+$/,'');
            if(searchText == '' || searchText == "Search" || newtext1 == ""){
                alert("Please enter a keyword for search");
                return false;
            }else if(searchText.length <= 3){
                alert("Please enter a valid keyword with more than 3 characters");
                return false;
            }else{
                searchText = encodeURIComponent(searchText);
                url = document.getElementById('frm_search').action += searchText +"/";
                window.location = url;
            }
        }
    });
    jQuery( '#frm_search_result' ).bind('keypress', function(e){
        if ( e.keyCode == 13 ) {
            //setTimeout("setResultAction()",100);
            searchText  = encodeURIComponent(trim(document.getElementById('searchText').value));
            searchCat = '';
            if(document.getElementById('selCat'))
                searchCat   = document.getElementById('selCat').value;
            searchOrder = document.getElementById('sortBy').value;

            var newtext1= searchText.replace(/^\s+/,'').replace(/\s+$/,'');
            if(searchText == '' || searchText == "Search" || newtext1 == ""){
                alert("Please enter a keyword for search");
                return false;
            }else if(searchText.length <= 3){
                alert("Please enter a valid keyword with more than 3 characters");
                return false;
            }else{
                setTimeout("redirect()",100);

            }

        }
    });

});

function setSearchType(){
    if(document.getElementById('seach_type').value == "user")
        document.getElementById('frm_search').action="/search/user/";
    if(document.getElementById('seach_type').value == "photos")
        document.getElementById('frm_search').action="/search/photo/";
    if(document.getElementById('seach_type').value == "learn")
        document.getElementById('frm_search').action="/search/learnCenter/";
    if(document.getElementById('seach_type').value == "store")
        document.getElementById('frm_search').action="/search/store/";
}
function setSearchAction(){ //alert(document.getElementById('frm_search').action);
    searchText = encodeURIComponent(trim(document.getElementById('search_text_box').value));
    var newtext1= searchText.replace(/^\s+/,'').replace(/\s+$/,'');
    if(searchText == '' || searchText == "Search" || newtext1 == ""){
        alert("Please enter a keyword for search");
        return false;
    }else if(searchText.length <= 3){
        alert("Please enter a valid keyword with more than 3 characters");
        return false;
    }else
    {
        searchText = encodeURIComponent(searchText);
        url = document.getElementById('frm_search').action;
        url+= searchText +"/";
        window.location = url;
    }

}
//function for search result pages
function setResultAction(){
    searchText  = encodeURIComponent(trim(document.getElementById('searchText').value));
    searchCat = '';
    if(document.getElementById('selCat'))
        searchCat   = document.getElementById('selCat').value;
    searchOrder = document.getElementById('sortBy').value;

    var newtext1= searchText.replace(/^\s+/,'').replace(/\s+$/,'');
    if(searchText == '' || searchText == "Search" || newtext1 == ""){
        alert("Please enter a keyword for search");
        return false;
    }else if(searchText.length <= 3){
        alert("Please enter a valid keyword with more than 3 characters");
        return false;
    }else{
        searchText = encodeURIComponent(searchText);
        url = document.getElementById('frm_search_result').action ;
        url += searchText +"/";
        if(searchCat)
            url += searchCat +"/";
        else
            url += "0/";
        if(searchOrder)
            url += searchOrder +"/";
        window.location = url;
    }
}
function redirect(){
    searchText  = encodeURIComponent(trim(document.getElementById('searchText').value));
    searchCat = '';
    if(document.getElementById('selCat'))
        searchCat   = document.getElementById('selCat').value;
    searchOrder = document.getElementById('sortBy').value;
    searchText = encodeURIComponent(searchText);
    url = document.getElementById('frm_search_result').action ;
    url += searchText +"/";
    if(searchCat)
        url += searchCat +"/";
    else
        url += "0/";
    if(searchOrder)
        url += searchOrder +"/";
    window.location = url;
}