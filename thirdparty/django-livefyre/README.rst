INTRODUCTION
============
This is a Django application to make Livefyre integration a little easier.

MANAGEMENT COMMANDS REQUIRED SETTINGS
=====================================
``LIVEFYRE_ACTOR_TOKEN="bGZ..Wc9"``
    A single line of base64-encoded data from `http://livefyre.com/auth/token/`

``LIVEFYRE_DOMAIN="example.fyre.co"``
    The DNS name of your Custom Domain

``LIVEFYRE_URL="http://example.com"`` (Optional)
    The full URL of the website/blog that this site object is for. 

``LIVEFYRE_ALLOW_DELETE_OPS=False``
    DELETE operations are disabled by default

``HTTP_CONNECTION_DEBUG_LEVEL=0``
    HTTPConnection.set_debuglevel_

SITE MANAGEMENT COMMANDS
------------------------
 - Get Site objects
        ``$ manage.py livefyre -r site``

 - Create a new site
        ``$ manage.py livefyre -r site --create  # url is settings.LIVEFYRE_URL``
   OR
        ``$ manage.py livefyre -r site --create --url <url>``

 - Get the site information with id {id} 
        ``$ manage.py livefyre -r site --get --id <id>``

 - Set postback_url for the specified site
        ``$ manage.py livefyre -r site --set --id <id> --property postback_url=http://...``

 - Delete the specified site
        ``$ manage.py livefyre -r site --delete --id <id>``


PROFILE MANAGEMENT COMMANDS
---------------------------
 - Get Profile objects that belong to the Domain
        ``$ manage.py livefyre -r profile --get``

 - Get the Profile identified by jid
        ``$ manage.py livefyre -r profile --get --jid <jid>``

 - Create a new profile with the specified jid
        ``$ manage.py livefyre -r profile --create --jid <jid>``

 - Get the collection of Profiles who own the Domain
        ``$ manage.py livefyre -r owner --get``

 - Get the Profile identified by jid
        ``$ manage.py livefyre -r owner --get --jid <jid>``


OWNER MANAGEMENT COMMANDS
-------------------------
 - Get the collection of Profiles who own the Domain
        ``$ manage.py livefyre -r owner --get``

 - Add the specified jid as an owner
        ``$ manage.py livefyre -r owner --get --jid <jid>``

 - Delete the specified jid as an owner
        ``$ manage.py livefyre -r owner --delete --jid <jid>``

ADMIN MANAGEMENT COMMANDS
-------------------------
 - Get the Profiles who are admins of the Domain
        ``$ manage.py livefyre -r admin --get``

 - Create a new admin
        ``$ manage.py livefyre -r admin --create --jid <jid>``

 - Delete the specified jid as an admin
        ``$ manage.py livefyre -r admin --delete --jid <jid>``


RUNTIME REQUIRED SETTINGS
=========================
``LIVEFYRE_ACTOR_TOKEN="bGZ..Wc9"``
    A single line of base64-encoded data from `http://livefyre.com/auth/token/`

``LIVEFYRE_DOMAIN="example.fyre.co"``
    The DNS name of your Custom Domain

``LIVEFYRE_URL="http://example.com"``
    The full URL of the website/blog that this site object is for

``LIVEFYRE_SITE_ID=28..``
    The id of the site within the domain.

``LIVEFYRE_DOMAIN_API_SECRET="FM9..="``
    As return by POST to `http://{domain}/sites/?actor_token={t}&url={u}` or the convenient command `$ manage.py livefyre -r site --create`


HOW TO TEST
=========== 
::

    $ git clone git://github.com/phuihock/django-livefyre.git
    $ virtualenv --distribute django-livefyre
    $ cd django-livefyre
    $ source bin/activate
    $ pip install -r req.txt
    $ python manage.py livefyre --help


Before any command can be run, required settings for management commands must be defined. `php` executable is required to run tests. 
::

    $ python manage.py test livefyre

.. _HTTPConnection.set_debuglevel: http://docs.python.org/library/httplib.html#httplib.HTTPConnection.set_debuglevel
