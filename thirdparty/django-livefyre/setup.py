from distutils.core import setup
import os

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    long_description = readme.read()

setup(
    name='django-livefyre',
    version='0.1.0',
    url='https://github.com/phuihock/django-livefyre',
    license='GPLv3',
    author='Chang Phui Hock',
    author_email='phuihock@gmail.com',
    description="Livefyre integration made easy.",
    long_description=long_description,
    include_package_data=True,
    packages=['livefyre'],
    install_requires=(
    ),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Web Environment",
        "Framework :: Django",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 2",
    ],
)
