"""
This module is contains many image manipulation function useful for various functionality of the site

Created on Nov 13, 2011

@author: Swaroop Shankar
"""

from PIL import Image
import pyexiv2
#from PIL.ExifTags import TAGS

def resize(img, box, fit, out, crop=False):
    """
    Downsample the image.
    @param img: Image -  an Image-object
    @param box: tuple(x, y) - the bounding box of the result image
    @param fit: boolean - crop the image to fill the box
    @param out: file-like-object - save the image into the output stream
    """
    if img.mode != "RGB":
        img = img.convert("RGB")
    #preresize image with factor 2, 4, 8 and fast algorithm

    factor = 1
    while img.size[0]/factor > 2*box[0] and img.size[1]*2/factor > 2*box[1]:
        factor *=2
       
    if factor > 1:
        img.thumbnail((img.size[0]/factor, img.size[1]/factor), Image.ANTIALIAS)
 
    #calculate the cropping box and get the cropped part
    if crop == True:
        if fit:
            x1 = y1 = 0
            x2, y2 = img.size
            wRatio = 1.0 * x2/box[0]
            hRatio = 1.0 * y2/box[1]
            if hRatio > wRatio:
                y1 = int(y2/2-box[1]*wRatio/2)
                y2 = int(y2/2+box[1]*wRatio/2)
            else:
                x1 = int(x2/2-box[0]*hRatio/2)
                x2 = int(x2/2+box[0]*hRatio/2)
            img = img.crop((x1,y1,x2,y2))
 
    #Resize the image with best quality algorithm ANTI-ALIAS
    img.thumbnail(box, Image.ANTIALIAS)

    #save it into a file-like object
    try:
        img.save(out, "JPEG", quality=80)
    except IOError:
        return False
    else:
        return True

def imageCrop(img, x, y, x2, y2, out):
    try:
        box = (int(x), int(y), int(x2), int(y2))
    except ValueError:
        return False
    else:
        region = img.crop(box)
        try:
            region.save(out, "JPEG", quality=100)
        except IOError:
            return False
        else:
            return True
    
def get_exif(file):
    """
    Retrieves EXIF information from a image
    """
    ret = {}
    metadata = pyexiv2.ImageMetadata(str(file))
    metadata.read()
    info = metadata.exif_keys
    for key in info:
        data = metadata[key]
        try:
            ret[key] = data.raw_value
        except pyexiv2.exif.ExifValueError:
            pass
    return ret

def write_exif(originFile, destinationFile, **kwargs):
    """
    Through this function one could write an exif information of a file to another file
    """
    ret = {}
    metadata = pyexiv2.ImageMetadata(str(originFile))
    metadata.read()
    info = metadata.exif_keys
    for key in info:
        data = metadata[key]
        try:
            ret[key] = data.value
        except pyexiv2.exif.ExifValueError:
            pass
    exifInformation = ret

    metadata = pyexiv2.ImageMetadata(str(destinationFile))
    metadata.read()
    for key, value in exifInformation.iteritems():
        metadata[key] = value

    copyrightName = kwargs.get('copyright', None)
    if copyrightName != None:
        metadata['Exif.Image.Copyright'] = copyrightName

    image = Image.open(destinationFile)
    metadata['Exif.Photo.PixelXDimension'] = image.size[0]
    metadata['Exif.Photo.PixelYDimension'] = image.size[1]

    try:
        metadata.write()
    except:
        return False
    else:
        return True