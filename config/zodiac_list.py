"""
This is a list of 12 zodiac signs. The list comprise of a key and value.
The key is build using the last date and month of a zodiac sign.
"""
ZODIAC_SIGN = {
    120: {'name': 'Capricorn', 'image': 'profile-sign10.png'},
    219: {'name': 'Aquarius', 'image': 'profile-sign11.png'},
    320: {'name': 'Pisces', 'image': 'profile-sign12.png'},
    420: {'name': 'Aries', 'image': 'profile-sign2.png'},
    521: {'name': 'Taurus', 'image': 'profile-sign3.png'},
    621: {'name': 'Gemini', 'image': 'profile-sign4.png'},
    722: {'name': 'Cancer', 'image': 'profile-sign1.png'},
    822: {'name': 'Leo', 'image': 'profile-sign5.png'},
    921: {'name': 'Virgo', 'image': 'profile-sign6.png'},
    1022: {'name': 'Libra', 'image': 'profile-sign7.png'},
    1122: {'name': 'Scorpio', 'image': 'profile-sign8.png'},
    1221: {'name': 'Sagittarius', 'image': 'profile-sign9.png'},
}