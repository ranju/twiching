# Django settings for twiching project.
import os
import sys

# Sets the required PYTHONPATH to the python path from the script itself
sys.path.append(os.path.join(os.path.dirname(__file__), 'apps/').replace('\\', '/'))
sys.path.append(os.path.join(os.path.dirname(__file__), 'thirdparty/').replace('\\', '/'))
sys.path.append(os.path.join(os.path.dirname(__file__), 'config/').replace('\\', '/'))

DIRNAME = os.path.abspath(os.path.dirname(__file__))
LOCAL_DEV = False

DEBUG = False
TEMPLATE_DEBUG = False
MAINTENANCE_MODE = False
THUMBNAIL_DEBUG = False

ADMINS = (
    ('Akhil   ', 'akhil.sayone@gmail.com'),
)

MANAGERS = ADMINS

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',  # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'twiching_new',  # os.path.join(os.path.dirname(__file__), 'twiching.db').replace('\\', '/'),  # Or path to database file if using sqlite3.
        'USER': 'twiching_new',  # Not used with sqlite3.
        'PASSWORD': '4126c959',  # Not used with sqlite3.
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}


#DATABASES = {
#  'default': {
#    'ENGINE': 'django.db.backends.postgresql_psycopg2',
#    'NAME': 'd8p1s1e6onpqvs',
#    'HOST': 'ec2-54-243-181-33.compute-1.amazonaws.com',
#    'PORT': 5432,
#    'USER': 'sddodsjhgvorpi',
#    'PASSWORD': 'k-ulNTrWsiVJtaGV3ZClZkoZzm'
#  }
#}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Asia/Kolkata'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'media/').replace('\\', '/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

#STATIC_ROOT = os.path.join(os.path.dirname(__file__), 'static/').replace('\\', '/')
STATIC_ROOT = '/home/twiching/webapps/twiching_new_static/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"

#STATIC_URL = '/static/'

STATIC_URL = 'http://twiching.com/static/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'  # STATIC_URL + "grappelli/" #

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    
    #os.path.join(os.path.dirname(__file__), 'static_files').replace('\\', '/'),
    #'/home/twiching/webapps/twiching_new_static/',
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '&yf^m#gio3!yyjtx9zt2oowb5ekeiufpy$rmrh!8o@qmb@u+m1'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)


TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    #    'django.core.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
#    'satchmo_store.shop.context_processors.settings',
    'social_auth.context_processors.social_auth_by_name_backends',
    'social_auth.context_processors.social_auth_backends',
    'adzone.context_processors.get_source_ip',

    # Oscar Specific
    "django.core.context_processors.debug",
    'oscar.apps.search.context_processors.search_form',
    'oscar.apps.promotions.context_processors.promotions',
    'oscar.apps.checkout.context_processors.checkout',
    'oscar.apps.customer.notifications.context_processors.notifications',
    'oscar.core.context_processors.metadata',

)

MIDDLEWARE_CLASSES = (
    #'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.doc.XViewMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    #'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'threaded_multihost.middleware.ThreadLocalMiddleware',
#    'satchmo_store.shop.SSLMiddleware.SSLRedirect',
    #'debug_toolbar.middleware.DebugToolbarMiddleware',
    'maintenancemode.middleware.MaintenanceModeMiddleware',

    'oscar.apps.basket.middleware.BasketMiddleware'


)

SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'

ROOT_URLCONF = 'twiching.urls'

from oscar import OSCAR_MAIN_TEMPLATE_DIR
TEMPLATE_DIRS = [
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(os.path.dirname(__file__), 'templates').replace('\\', '/'),
    OSCAR_MAIN_TEMPLATE_DIR,
]



from oscar import get_core_apps
INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.admin',
    'django.contrib.sitemaps',
    #'django.contrib.admindocs',
    'django.contrib.markup',
    'keyedcache',
    'livesettings',
#    'l10n',
    #'django.contrib.flatpages',
    'twiching.thirdparty.countries',
    'django_extensions',
    'registration',
    'widget_tweaks',
    'twiching.apps.accounts',
    'twiching.apps.general',
    'twiching.apps.menu',
    'mptt',
    'tagging',
    'twiching.apps.photography',
    'endless_pagination',
    'tagging_autocomplete',
    'twiching.apps.comment',
    'twiching.apps.events',
    'twiching.apps.classifieds',
    'sorl.thumbnail',
    #    'satchmo_store.shop',
    #    'satchmo_store.contact',
    #    'satchmo_ext.wishlist',
    #    'tax',
    #    'tax.modules.no',
    #    'tax.modules.area',
    #    'tax.modules.percent',
    #    'shipping',
    #    'product',
    #    'product.modules.configurable',
    #    'payment',
    #    'payment.modules.cod',
    #    'payment.modules.giftcertificate',
    #    'satchmo_utils.thumbnail',
    #    'satchmo_utils',
#    'app_plugins',
#    'debug_toolbar',
    #    'twiching.apps.ecommerce.store',
    #    'twiching.apps.ecommerce.wishlist',
    #    'twiching.apps.ecommerce.product',
#    'south',
    'djangoratings',
    'social_auth',
    'captcha',
    'twiching.apps.atoz',
    'twiching.apps.assistance',
    'twiching.apps.wall',
    'ajaxutils',
    'mailsnake',
    'ckeditor',
    'adzone',
    'twiching.apps.cms',
    'twiching.apps.feeds',
    'maintenancemode',
    'twiching.apps.seo',
    'googletools',
    #'haystack',
    #'search',
    
    'twiching.apps.classifieds',

]
oscar_apps = [
    'oscar',
    'oscar.apps.analytics',
    'oscar.apps.order',
    'oscar.apps.checkout',
    'oscar.apps.shipping',
    'oscar.apps.catalogue',
    'oscar.apps.catalogue.reviews',
    'oscar.apps.basket',
    'oscar.apps.payment',
    'oscar.apps.offer',
    'oscar.apps.address',
    'oscar.apps.partner',
    'oscar.apps.customer',
#    'oscar.apps.promotions',
    'twiching.apps.myecommerce.promotions',
    'twiching.apps.myecommerce.basket',
    'twiching.apps.myecommerce.checkout',
    'twiching.apps.myecommerce.catelogue',
    'oscar.apps.search',
    'oscar.apps.voucher',
    'twiching.mailer',
    'oscar.apps.dashboard',
    'oscar.apps.dashboard.reports',
    'oscar.apps.dashboard.users',
    'oscar.apps.dashboard.orders',
    'oscar.apps.dashboard.promotions',
    'oscar.apps.dashboard.catalogue',
    'oscar.apps.dashboard.offers',
    'oscar.apps.dashboard.ranges',
    'oscar.apps.dashboard.vouchers',
    'oscar.apps.dashboard.communications',
    'sorl.thumbnail'
]

INSTALLED_APPS = INSTALLED_APPS + oscar_apps


AUTHENTICATION_BACKENDS = (
    'social_auth.backends.twitter.TwitterBackend',
    'social_auth.backends.facebook.FacebookBackend',
    'social_auth.backends.OpenIDBackend',
#    'satchmo_store.accounts.email-auth.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',
    'twiching.apps.accounts.backends.CaseInsensitiveModelBackend',

    # Oscar specific
    'oscar.apps.customer.auth_backends.Emailbackend',


)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': os.path.join(os.path.dirname(__file__), 'tmp/cache').replace('\\', '/'),
        }
}

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
            },
        }
}

#Country application settings
COUNTRIES_FLAG_PATH = '/media/flags/%s.png'

#Django Profile Application
AUTH_PROFILE_MODULE = 'accounts.UserProfile'
REGISTRATION_MIN_AGE = 13
REGISTRATION_MAX_AGE = 75

#TinyMCE settings
#TINYMCE_JS_URL = MEDIA_URL + '/jscripts/tiny_mce/tiny_mce.js'
#TINYMCE_JS_ROOT = MEDIA_ROOT + 'jscripts/tiny_mce'
#TINYMCE_DEFAULT_CONFIG = {
#    'plugins': "table,spellchecker,paste,searchreplace",
#    'theme': "advanced",
#    }
#TINYMCE_SPELLCHECKER = True
#TINYMCE_COMPRESSOR = True


#IMAGE Upload Settings
PROFILE_IMAGE_UPLOAD_PATH = 'profiles'
BANNER_IMAGE_UPLOAD_PATH = 'banners'
PHOTOGRAPH_UPLOAD_PATH = 'photographs'
PHOTOGRAPH_ORIGINAL_UPLOAD_PATH = 'photographs/nagsghjq'
PRODUCT_UPLOAD_PATH = 'products'
AtoZ_IMAGE_UPLOAD_PATH = 'flatpage'
EVENTS_IMAGE_UPLOAD_PATH = 'events'

#pagination settings
ENDLESS_PAGINATION_PER_PAGE = 12
ENDLESS_PAGINATION_PREVIOUS_LABEL = '&lt;&lt; Previous'
ENDLESS_PAGINATION_NEXT_LABEL = 'Next &gt;&gt;'

# Load the local settings
from local_settings import *

#Message Storage Engine
MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'


LOGIN_URL = '/accounts/login/'
LOGIN_REDIRECT_URL = '/'
LOGIN_ERROR_URL = '/accounts/login/'
SOCIAL_AUTH_BACKEND_ERROR_URL = '/'

SOCIAL_AUTH_COMPLETE_URL_NAME = 'socialauth_complete'
SOCIAL_AUTH_ASSOCIATE_URL_NAME = 'socialauth_associate_complete'

#Registration module settings
ACCOUNT_ACTIVATION_DAYS = 7
#EMAIL_BACKEND = "mailer.backend.DbBackend"
DEFAULT_FROM_EMAIL = "Twiching.com <do-not-reply@twiching.com>"
SERVER_EMAIL = "Twiching.com <do-not-reply@twiching.com>"
EMAIL_HOST = 'smtp.webfaction.com'
#EMAIL_USE_TLS = 1
#EMAIL_PORT = 465
EMAIL_HOST_USER = 'twich_app_email'
EMAIL_HOST_PASSWORD = 'uW b.6vsyv2X)18'
DEFAULT_STORE_EMAIL = 'Twiching Stores <store@twiching.com>'
DEFAULT_ADMIN_EMAIL = 'store@twiching.com'

#### Satchmo unique variables ####
from django.conf.urls.defaults import patterns, include
#SATCHMO_SETTINGS = {
#    'SHOP_BASE': '',
#    'MULTISHOP': False,
#    'SSL': False,
#    #'SHOP_URLS' : patterns('satchmo_store.shop.views',)
#}

#OAuth settings
TWITTER_CONSUMER_KEY = 'sO7IDfK05m2xy9tlqgVeQ'
TWITTER_CONSUMER_SECRET = 'e6W4TgWlFDRNRzsnCxQjGNa07PMTmUhAgHplZJsyN8'

#facebook settings
#FACEBOOK_APP_ID = '223063791125302'
#FACEBOOK_API_SECRET = '624b22617fb47d9d425e25a4c5856762'
FACEBOOK_APP_ID = '358848787498185'
FACEBOOK_API_SECRET = '65d721a42fedef525767e4a72fa77ae1'
FACEBOOK_SHOW_SEND = "true"   # or "false, default is "true"
FACEBOOK_LIKE_WIDTH = "240"   # "numeric value for width", default is 450
FACEBOOK_SHOW_FACES = "true"  # or "false, default is "true"
FACEBOOK_FONT = "arial"        # default is "arial"
FACEBOOK_EXTENDED_PERMISSIONS = ['email', 'user_about_me', 'user_birthday']

#Extra Settings
DATE_FORMAT = "d M Y"
#INTERNAL_IPS = ('127.0.0.1','192.168.2.5')
#DEBUG_TOOLBAR_PANELS = (
#    'debug_toolbar.panels.version.VersionDebugPanel',
#    'debug_toolbar.panels.timer.TimerDebugPanel',
#    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
#    'debug_toolbar.panels.headers.HeaderDebugPanel',
#    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
#    'debug_toolbar.panels.template.TemplateDebugPanel',
#    'debug_toolbar.panels.sql.SQLDebugPanel',
#    'debug_toolbar.panels.signals.SignalDebugPanel',
#    'debug_toolbar.panels.logger.LoggingPanel',
#)
#DEBUG_TOOLBAR_CONFIG = {
#    'INTERCEPT_REDIRECTS': False,
#}

L10N_SETTINGS = {
    'currency_formats': {
        'AED': {'symbol': u'AED', 'positive': u"aed%(val)0.2f", 'negative': u"aed(%(val)0.2f)", 'decimal': ','},
        'Rupee': {'symbol': u'Rs.', 'positive': u"Rs%(val)0.2f", 'negative': u"rs(%(val)0.2f)", 'decimal': '.'},
        },
    'default_currency': 'AED',
    'show_admin_translations': False,
    'allow_translation_choice': False,
    }

SOCIAL_AUTH_UUID_LENGTH = 5
SOCIAL_AUTH_EXTRA_DATA = False
SOCIAL_AUTH_DEFAULT_GROUP = 1

#ReCaptcha
RECAPTCHA_PUBLIC_KEY = '6LcVDc8SAAAAAMFAL0ROTwPnByYUKq5YzSu_AvtU'
RECAPTCHA_PRIVATE_KEY = '6LcVDc8SAAAAAOJaRA3_jy8wukdICbFVeCJzlUNG'

#MailChimp Configuration
MAILCHIMP_API_KEY = '8f7429d153e361d1f95570248c61ae12-us4'

#ckeditor configuration
CKEDITOR_MEDIA_PREFIX = "/static/ckeditor/"
CKEDITOR_UPLOAD_PATH = os.path.join(os.path.dirname(__file__), 'media/editor/').replace('\\', '/')
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Full',
        },
    }

#SUPPORT
SUPPORT_TOPIC_CHOICES = (
    ('Feedback', 'Feedback'),
    ('General Support', 'General Support'),
    ('Shopping Support', 'Shopping Support'),
    ('Website Bug Reporting', 'Website Bug Reporting'),
    )
SUPPORT_EMAIL_LIST = ['ani.sayone@gmail.com']

#ID CONFIGURATION
#map the group_id in database table here
GROUP_ID_MAPPING = {
    'PHOTOGRAPHER': 1,
    'MODEL': 2,
    'PHOTO_ENTHUSIAST': 3,
    'MODELING_AGENCY': 11,
    'FASHION_DESIGNER': 9,
    'MAKE_UP_ARTIST': 5,
    }

ENABLED_GROUP_ID_MAPPING = {
    'PHOTOGRAPHER': 1,
    'MODEL': 2,
    'FASHION_DESIGNER': 9,
    'MAKE_UP_ARTIST': 5,
    }

LEARNING_CENTER_ID = {
    'NEWS': 1,
    'ARTICLES': 2,
    'TUTORIALS': 3,
    }

#solr-thumbnail settings
THUMBNAIL_ENGINE = 'sorl.thumbnail.engines.convert_engine.Engine'
#THUMBNAIL_CONVERT = 'gm convert'
#THUMBNAIL_IDENTIFY = 'gm identify'
#THUMBNAIL_QUALITY = 85

#HAYSTACK_SITECONF = 'twiching.search_sites'
##If you choose whoosh as search backend uncomment following 2 lines and comment last 2 lines
#HAYSTACK_SEARCH_ENGINE = 'whoosh'
#HAYSTACK_WHOOSH_PATH = os.path.join(os.path.dirname(__file__), 'whoosh_index')

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(os.path.dirname(__file__), 'whoosh_index'),
        },
    }



#settings for multiple file upload
DEFAULT_CATEGORY = '28'
FILEUPLOAD_SETTINGS = {
    'MAX_FILESIZE':10485760,  #size in kb
    'MIN_FILESIZE':1024, #size in kb
    'ACCEPTED_TYPES':(
        "image/jpeg",
        "image/png",)
}


# Oscar settings
from oscar.defaults import *

OSCAR_INITIAL_ORDER_STATUS = 'Pending'
OSCAR_INITIAL_LINE_STATUS = 'Pending'
OSCAR_ORDER_STATUS_PIPELINE = {
    'Order Placed': ('Order Placed', 'Order Placed',),
    'Pending': ('Being processed', 'Cancelled',),
    'Being processed': ('Processed', 'Cancelled',),
    'Cancelled': (),
    }
OSCAR_LINE_STATUS_PIPELINE = {
    'Order Placed': ('Order Placed', 'Order Placed',),
    'Pending': ('Pending', 'Pending',),
    'Being processed': ('Being processed', 'Being processed',),
    'Cancelled': ('Cancelled','Cancelled'),
    }
