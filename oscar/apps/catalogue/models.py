"""
Vanilla product models
"""
from twiching.oscar.apps.catalogue.abstract_models import *
from django.utils.translation import ugettext_lazy as _


class ProductClass(AbstractProductClass):
    pass


class Category(AbstractCategory):
    pass


class ProductCategory(AbstractProductCategory):
    pass


class Product(AbstractProduct):
    pass


class ContributorRole(AbstractContributorRole):
    pass


class Contributor(AbstractContributor):
    pass


class ProductContributor(AbstractProductContributor):
    pass


class ProductAttribute(AbstractProductAttribute):
    pass


class ProductAttributeValue(AbstractProductAttributeValue):
    pass


class AttributeOptionGroup(AbstractAttributeOptionGroup):
    pass


class AttributeOption(AbstractAttributeOption):
    pass


class AttributeEntity(AbstractAttributeEntity):
    pass


class AttributeEntityType(AbstractAttributeEntityType):
    pass


class Option(AbstractOption):
    pass


class ProductImage(AbstractProductImage):
    pass

class Comments(models.Model):
    product = models.ForeignKey(Product,related_name='product_comments')
    name = models.CharField(max_length=50)
    text = models.TextField()
    is_approved = models.BooleanField(_("Approve"), default=False,help_text="This feature is not implemented yet.")
    email = models.EmailField(_("Email Address"))

    class Meta:
        verbose_name = "Comment"
        verbose_name_plural="Comments"
