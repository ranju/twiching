"""
A file to import the haystack apps and to set auto-discovery of indexes. File created based on haystack documentation

@author: Aswathy Rajesh
@organization: Opentechnics
@contact: aswathi.r@opentechnics.com
@copyright: (c) 2012, Twiching
"""
import haystack
haystack.autodiscover()
